# Dropfort Build Tools

Drupal development tools, CI/CD checks and validation scripts.

Generates development and CI/CD configuration files within your Drupal project
to allow it to work with Dropfort release processes.

All configurations and settings defined in this project are based on the Drupal best practices
and Composer / PHP recommended development practices.

See https://dropfort.gitlab.io/dropfort_build for configuration and usage details.
