# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [6.22.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.21.2...v6.22.0) (2025-02-21)


### Features

* bump default minor versions of php ([21ba30e](https://gitlab.com/dropfort/dropfort_build/commit/21ba30e41227a02c8cf8a8cec8d7d7453b505f0a))
* **scaffold:** add new scripts to build themes and modules ([c7025cf](https://gitlab.com/dropfort/dropfort_build/commit/c7025cfecb4c722cf61fcf8b5f5cc2fee0fc008a))


### Bug Fixes

* **docs:** typo in env var table ([38ee5e5](https://gitlab.com/dropfort/dropfort_build/commit/38ee5e5225357deacaebafb6b1d59e7bf1202842))
* **env:** add rules for feature env ([87c0e35](https://gitlab.com/dropfort/dropfort_build/commit/87c0e3510b1ccdae530dec5147f3f992385302a3))
* remove stevencl extension ([2276e92](https://gitlab.com/dropfort/dropfort_build/commit/2276e925153ecb6027ccec9852d72d17227bc7eb))
* **solr:** allow solr 9.8+ ([5114d8b](https://gitlab.com/dropfort/dropfort_build/commit/5114d8b591192479ae3cae1353a02a6df080239e)), closes [/solr.apache.org/guide/solr/latest/upgrade-notes/major-changes-in-solr-9.html#configuration-2](https://gitlab.com/dropfort//solr.apache.org/guide/solr/latest/upgrade-notes/major-changes-in-solr-9.html/issues/configuration-2)

### [6.21.2](https://gitlab.com/dropfort/dropfort_build/compare/v6.21.1...v6.21.2) (2024-12-06)

### [6.21.1](https://gitlab.com/dropfort/dropfort_build/compare/v6.21.0...v6.21.1) (2024-12-06)


### Bug Fixes

* **ci:** add exclude check when feat is disabled ([2f40c13](https://gitlab.com/dropfort/dropfort_build/commit/2f40c138a9cadf8ce11b430573d8d314dffcdc52))
* **ci:** update k8s tasks ([ce655e4](https://gitlab.com/dropfort/dropfort_build/commit/ce655e4643b683d07f3b0c73845bd9e28f9ffef7))

## [6.21.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.20.0...v6.21.0) (2024-12-03)


### Features

* **vscode:** add flashpost ([d454aac](https://gitlab.com/dropfort/dropfort_build/commit/d454aacac445217d886b5f143d50240c134117f3))
* **vscode:** add flashpost and containerssh client ([e5f6f9e](https://gitlab.com/dropfort/dropfort_build/commit/e5f6f9eb9be5bdbba19b83b510248f4090eb22af))


### Bug Fixes

* **ci:** force yargs newer version ([2a2c596](https://gitlab.com/dropfort/dropfort_build/commit/2a2c5968d0faf8ddad99d7c7f21e7148eedfd638))

## [6.20.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.19.0...v6.20.0) (2024-11-22)


### Features

* **ci:** add prod database dump ([0407bab](https://gitlab.com/dropfort/dropfort_build/commit/0407bab17f33ba805ce09cbf0adeab2d47312a25))
* **k8s:** allow feature envs ([01fba49](https://gitlab.com/dropfort/dropfort_build/commit/01fba49ac56c3cccac73ccc833f3c1d14f34913b))


### Bug Fixes

* **ci:** move db dump outside webroot ([dff00a1](https://gitlab.com/dropfort/dropfort_build/commit/dff00a14965c721a69001dd474801415c7094e4a))
* **ci:** update get db task ([a81f441](https://gitlab.com/dropfort/dropfort_build/commit/a81f44197da76e5c1a8efeaf818fabdf901c21d7))
* **docs:** add k8s feat env info ([380f9ce](https://gitlab.com/dropfort/dropfort_build/commit/380f9ceb3cc7127ccef60d2c9128771196346b61))
* **docs:** adjust markdown ([4bf6cc7](https://gitlab.com/dropfort/dropfort_build/commit/4bf6cc75e92e140b4e3e5d08b3c66c11eba2fca2))
* **mariadb:** ensure the mysql alias exists ([c5f988f](https://gitlab.com/dropfort/dropfort_build/commit/c5f988f6170deaf12a46efa343eb182badf218f7))


### Code Refactoring

* **dev:** use fewer layers ([cc16919](https://gitlab.com/dropfort/dropfort_build/commit/cc16919d92b7f16175db74ce4b7744465d3e5bb0))

## [6.19.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.18.0...v6.19.0) (2024-11-16)


### Features

* **drupal:** add support for dynamic config splits ([71f99ad](https://gitlab.com/dropfort/dropfort_build/commit/71f99ad58b6176a0303d8a277072a4180d49c321))

## [6.18.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.17.1...v6.18.0) (2024-11-16)


### Features

* **solr:** automatically manage connection info for search_api_solr backends ([b1fa69d](https://gitlab.com/dropfort/dropfort_build/commit/b1fa69d0594f1f805c82f5ae2c5e2691753b0048))
* **vscode:** add css support checker ([0b749d6](https://gitlab.com/dropfort/dropfort_build/commit/0b749d604ad85213556700bf09da45eca3ff033b))

### [6.17.1](https://gitlab.com/dropfort/dropfort_build/compare/v6.17.0...v6.17.1) (2024-11-08)


### Bug Fixes

* **docs:** add docs about skip sanitize ([a619641](https://gitlab.com/dropfort/dropfort_build/commit/a619641687698821bc18bf5902623be512fda924))

## [6.17.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.16.0...v6.17.0) (2024-11-08)


### Features

* **ci:** allow sql drop on target db during sync ([1904bbb](https://gitlab.com/dropfort/dropfort_build/commit/1904bbb7e75934cff9fdb74ea9844d565b116545))
* **ci:** only show backcopy on prod branches ([f8112e8](https://gitlab.com/dropfort/dropfort_build/commit/f8112e8fcbb09fc4b93984d6709b63d2b4899844))


### Bug Fixes

* **ci:** default to no sql drop on backcopy ([521ee73](https://gitlab.com/dropfort/dropfort_build/commit/521ee73194cd04ace7dfe6d91c0fb8b49e2f698e))
* **ci:** default to no sql drop on backcopy ([ba82753](https://gitlab.com/dropfort/dropfort_build/commit/ba82753b5f8fe12863ccdbc8859339b5a8f9e6c3))
* **docs:** update domains ([a650a2d](https://gitlab.com/dropfort/dropfort_build/commit/a650a2d631dfe73c12db5c69a7e74e666ec3e106))

## [6.16.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.13.0...v6.16.0) (2024-11-01)


### Features

* **apache:** add another local IP range ([5de3905](https://gitlab.com/dropfort/dropfort_build/commit/5de3905c4118d205927492d4876cb0f376536c0f))
* **ci:** allow skip sanitize on backcopy job ([fc2cdcb](https://gitlab.com/dropfort/dropfort_build/commit/fc2cdcb855e6622289d9b2ba841c556a2b39e123))
* **jmeter:** Add jmeter support ([c2c213b](https://gitlab.com/dropfort/dropfort_build/commit/c2c213b9aecf55737132ca60afa6132ea4feae4e))


### Bug Fixes

* **ci:** only allow backcopy from prod release pipelines ([d1965ef](https://gitlab.com/dropfort/dropfort_build/commit/d1965efc699f547ee0263538e1b79f63d9609e26))
* **ci:** only allow backcopy from prod release pipelines ([f9721b2](https://gitlab.com/dropfort/dropfort_build/commit/f9721b259630791aaa18c2f4fbea5b93198c97a0))
* **docs:** add missing link ([3518109](https://gitlab.com/dropfort/dropfort_build/commit/351810930fab0729ee29790497c83e98d2110a24))
* **scaffold:** ignore cache dirs ([2452370](https://gitlab.com/dropfort/dropfort_build/commit/2452370fb891644e3550d57ec20d22d96a77e9be))
* **scaffold:** ignore files from phpstorm ([d4a8f8f](https://gitlab.com/dropfort/dropfort_build/commit/d4a8f8f75ae1d8086c337ac207534a8d82d01282))
* **test:** specify Drupal 10 ([8617a81](https://gitlab.com/dropfort/dropfort_build/commit/8617a8169e52cc0a72c62734f79afde72a580d97))

## [6.15.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.13.0...v6.15.0) (2024-11-01)


### Features

* **apache:** add another local IP range ([5de3905](https://gitlab.com/dropfort/dropfort_build/commit/5de3905c4118d205927492d4876cb0f376536c0f))
* **ci:** allow skip sanitize on backcopy job ([fc2cdcb](https://gitlab.com/dropfort/dropfort_build/commit/fc2cdcb855e6622289d9b2ba841c556a2b39e123))
* **jmeter:** Add jmeter support ([c2c213b](https://gitlab.com/dropfort/dropfort_build/commit/c2c213b9aecf55737132ca60afa6132ea4feae4e))


### Bug Fixes

* **ci:** only allow backcopy from prod release pipelines ([d1965ef](https://gitlab.com/dropfort/dropfort_build/commit/d1965efc699f547ee0263538e1b79f63d9609e26))
* **ci:** only allow backcopy from prod release pipelines ([f9721b2](https://gitlab.com/dropfort/dropfort_build/commit/f9721b259630791aaa18c2f4fbea5b93198c97a0))
* **docs:** add missing link ([3518109](https://gitlab.com/dropfort/dropfort_build/commit/351810930fab0729ee29790497c83e98d2110a24))
* **scaffold:** ignore cache dirs ([2452370](https://gitlab.com/dropfort/dropfort_build/commit/2452370fb891644e3550d57ec20d22d96a77e9be))
* **scaffold:** ignore files from phpstorm ([d4a8f8f](https://gitlab.com/dropfort/dropfort_build/commit/d4a8f8f75ae1d8086c337ac207534a8d82d01282))
* **test:** specify Drupal 10 ([8617a81](https://gitlab.com/dropfort/dropfort_build/commit/8617a8169e52cc0a72c62734f79afde72a580d97))

## [6.14.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.13.0...v6.14.0) (2024-11-01)


### Features

* **apache:** add another local IP range ([5de3905](https://gitlab.com/dropfort/dropfort_build/commit/5de3905c4118d205927492d4876cb0f376536c0f))
* **ci:** allow skip sanitize on backcopy job ([fc2cdcb](https://gitlab.com/dropfort/dropfort_build/commit/fc2cdcb855e6622289d9b2ba841c556a2b39e123))
* **jmeter:** Add jmeter support ([c2c213b](https://gitlab.com/dropfort/dropfort_build/commit/c2c213b9aecf55737132ca60afa6132ea4feae4e))


### Bug Fixes

* **ci:** only allow backcopy from prod release pipelines ([d1965ef](https://gitlab.com/dropfort/dropfort_build/commit/d1965efc699f547ee0263538e1b79f63d9609e26))
* **ci:** only allow backcopy from prod release pipelines ([f9721b2](https://gitlab.com/dropfort/dropfort_build/commit/f9721b259630791aaa18c2f4fbea5b93198c97a0))
* **docs:** add missing link ([3518109](https://gitlab.com/dropfort/dropfort_build/commit/351810930fab0729ee29790497c83e98d2110a24))
* **scaffold:** ignore cache dirs ([2452370](https://gitlab.com/dropfort/dropfort_build/commit/2452370fb891644e3550d57ec20d22d96a77e9be))
* **scaffold:** ignore files from phpstorm ([d4a8f8f](https://gitlab.com/dropfort/dropfort_build/commit/d4a8f8f75ae1d8086c337ac207534a8d82d01282))
* **test:** specify Drupal 10 ([8617a81](https://gitlab.com/dropfort/dropfort_build/commit/8617a8169e52cc0a72c62734f79afde72a580d97))

## [6.13.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.12.0...v6.13.0) (2024-10-11)


### Features

* **k8s:** add k8s tast to update varnish ([83be2ed](https://gitlab.com/dropfort/dropfort_build/commit/83be2ed42a81bd60f487ba761e48a1fbe6aa7c09))
* **vscode:** allow html live preview ([a9f95ae](https://gitlab.com/dropfort/dropfort_build/commit/a9f95aee0d657f6045f0e420be19a6426f14cf3d))


### Bug Fixes

* **docs:** add notes about new env var ([131f1a4](https://gitlab.com/dropfort/dropfort_build/commit/131f1a4496a4092dcbeff942b06ce99354f68857))
* **kubernetes:** allow specify cluster id ([16c10da](https://gitlab.com/dropfort/dropfort_build/commit/16c10da35c24df47f90f1ea4006ecc2568b3484c))
* **scaffold:** add new ci file ([9113300](https://gitlab.com/dropfort/dropfort_build/commit/911330070abc1f6b8ee80e9660f27259036b4576))


### Performance Improvements

* **ci:** allow reference to run during build ([0ac6f24](https://gitlab.com/dropfort/dropfort_build/commit/0ac6f24ff733c12f491dd269956e38f4605b48f1))

## [6.12.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.11.0...v6.12.0) (2024-09-20)


### Features

* **k8s:** add versionlabel with the commit sha to the k8s deploy ([7183a21](https://gitlab.com/dropfort/dropfort_build/commit/7183a219665df9c91644d19162cfe2e3054e0b0a))


### Bug Fixes

* **container:** required for symfony isSecure ([121e35f](https://gitlab.com/dropfort/dropfort_build/commit/121e35f0f2d6d89b1425d9d8867bb729ac805307))
* **vscode:** ignore hooks in spellchecker ([10f9230](https://gitlab.com/dropfort/dropfort_build/commit/10f9230d4f56f7dc28bebe61cb6dff860ee6bd1e))
* **vscode:** update devcontainer ([37dc4e6](https://gitlab.com/dropfort/dropfort_build/commit/37dc4e6fa766336701301bd79ddc7102cfe5704c))


### Performance Improvements

* **ci:** allow code spec parrallel exec ([da860a6](https://gitlab.com/dropfort/dropfort_build/commit/da860a6d246b183508583951c751efe102c0bcf6))

## [6.11.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.10.0...v6.11.0) (2024-09-12)


### Features

* **ci:** allow skip of maintenance mode ([0f2b447](https://gitlab.com/dropfort/dropfort_build/commit/0f2b447f5838cfac924b3f33e160746f3a6e1fd1))

## [6.10.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.9.0...v6.10.0) (2024-09-12)


### Features

* **ci:** allow skip of local backup during deploy ([473e04e](https://gitlab.com/dropfort/dropfort_build/commit/473e04e3fc442da96b6f1e4fdc5e5ac60ad2459f))
* **ci:** download ingress config from api site ([c150c59](https://gitlab.com/dropfort/dropfort_build/commit/c150c592c1ecec1bd73fda55c033d9acc6add8cc))
* **drupal:** add file asset path ([59785f8](https://gitlab.com/dropfort/dropfort_build/commit/59785f8cf1ed853e70a1935f5b95e204b94e51b9))
* **k8s:** add additional install tasks ([1d662b3](https://gitlab.com/dropfort/dropfort_build/commit/1d662b3e2d38499db5142b22b7eb29d5261a0190))
* **k8s:** pass image as artifact ([aadd9f6](https://gitlab.com/dropfort/dropfort_build/commit/aadd9f6e09ce4e47c30946c3bb16004f6a05dddf))
* **k8s:** use repo from pipeline ([d0ea2ec](https://gitlab.com/dropfort/dropfort_build/commit/d0ea2ec857d8ae24dcd9f5caeea0105edee5474b))
* **performance:** skip git clone in more scenarios ([7f591e6](https://gitlab.com/dropfort/dropfort_build/commit/7f591e6a3bbbd6c36bb4142ad8a2a502a76365cf))


### Bug Fixes

* **build:** add ignore exception ([0f44728](https://gitlab.com/dropfort/dropfort_build/commit/0f44728bcedd7b2a7910d5ee6c6cbd5c71a81536))
* **build:** scaffold ([3c1603c](https://gitlab.com/dropfort/dropfort_build/commit/3c1603c1a307d84d87b813c0e80e2cc8866a0b7e))
* **ci:** helm typo ([c364f0d](https://gitlab.com/dropfort/dropfort_build/commit/c364f0d3a91a55ee09b127d214cb7b4bdb6961da))
* **ci:** move to task ([90d54de](https://gitlab.com/dropfort/dropfort_build/commit/90d54de56783eb1e510bbb62ec478360afa6e2a5))
* **ci:** update yq command ([da73aa4](https://gitlab.com/dropfort/dropfort_build/commit/da73aa462d29dea9fa94dc6af2983f31e663e4a6))
* **ci:** use release rules ([4bfc217](https://gitlab.com/dropfort/dropfort_build/commit/4bfc217edb6e82bc0599f41f1adfae60d2675c58))
* **helm:** use defaults when new values added ([6638d0a](https://gitlab.com/dropfort/dropfort_build/commit/6638d0a598fe0a4c9b6ffdb93e685fa57f5f98a6))
* **php:** enable igbinary ([9f57878](https://gitlab.com/dropfort/dropfort_build/commit/9f578783bd39a2c1e73e6a6189e9c9d525914484))
* **typo:** missing arrow ([59f423b](https://gitlab.com/dropfort/dropfort_build/commit/59f423ba1177a2be6a2e8663083b9fbaaae946f3))
* **updater:** remove check for error higher that 0 ([486eea7](https://gitlab.com/dropfort/dropfort_build/commit/486eea72817ea2228fe441dcd074abe0b5cf4234))


### Code Refactoring

* **ci:** download all values before updating k8s ([8cbda58](https://gitlab.com/dropfort/dropfort_build/commit/8cbda58dbc3aa9848ac09c82b8f8d0709ee25aaa))

## [6.9.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.8.0...v6.9.0) (2024-08-29)


### Features

* **ci:** allow auto updater script options to be overwritten ([1a12ddc](https://gitlab.com/dropfort/dropfort_build/commit/1a12ddcb90d2e113e0f1c9a6de8e2acdeab93f21))


### Performance Improvements

* adjust qhat arguments can be customized ([2f368a4](https://gitlab.com/dropfort/dropfort_build/commit/2f368a46f6e3910e2844b42116aaa22e6e02d6a3))

## [6.8.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.7.0...v6.8.0) (2024-08-29)


### Features

* **caching:** autodetect memcache ([e2880ef](https://gitlab.com/dropfort/dropfort_build/commit/e2880ef990084a43b87761e6f9a8b4f3ab73e484))
* **container:** enable remoteip module ([c81ff34](https://gitlab.com/dropfort/dropfort_build/commit/c81ff347eecd84bbdbbad6d9dd458678a931f7be))
* **helm:** make the k8s deploys use the coldfront drupal chart ([3ea484d](https://gitlab.com/dropfort/dropfort_build/commit/3ea484d447b623d8c9df05a88d561e7d8eaf72fa))


### Bug Fixes

* **build:** add k8s script ([8d5c1c4](https://gitlab.com/dropfort/dropfort_build/commit/8d5c1c45ec56bb4cc73e72b34ecc94c3236d81f2))
* **kubernetes:** allow any hostname ([13fb2e0](https://gitlab.com/dropfort/dropfort_build/commit/13fb2e0ee2c0129877dc276f9e1553b818121b8d))
* **updates:** remove initial ssh config setup ([e860d15](https://gitlab.com/dropfort/dropfort_build/commit/e860d15582ef777db3af0df3c08594d5ea8bc1e1))


### Code Refactoring

* **docker:** sort compose file ([f03e0aa](https://gitlab.com/dropfort/dropfort_build/commit/f03e0aa3c36fbb08daa154f4facd3889416e5bb4))

## [6.7.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.6.2...v6.7.0) (2024-07-23)


### Features

* **dev:** add additional settings for local dev ([efa9902](https://gitlab.com/dropfort/dropfort_build/commit/efa990236bb437aedf83cef4ab3a43edd3fa1d1c))
* **drupal:** add DF_DRUPAL_DOMAINS handling to dev ([cb9d206](https://gitlab.com/dropfort/dropfort_build/commit/cb9d206a806f9ac2e9374259203139c487e25bd0))
* **drupal:** restore trusted host patterns for prod containers ([a81de3e](https://gitlab.com/dropfort/dropfort_build/commit/a81de3ed85c4de819cdc41a2ef3309289da7d3de))
* **drupal:** restore trusted host patterns for prod containers ([93d9fde](https://gitlab.com/dropfort/dropfort_build/commit/93d9fde630b236564e4fc951780b61757a408213))
* **drupal:** upgrade settings.php to 10.3 ([7982a8b](https://gitlab.com/dropfort/dropfort_build/commit/7982a8bea1719f68ce029905ec902e4e875fc6e2))


### Bug Fixes

* **rsync:** do not rsync js config files ([d416179](https://gitlab.com/dropfort/dropfort_build/commit/d4161794d80db4da48e576a35cc00d6705bfc660))


### Code Refactoring

* **container:** update build scripts ([cf0baa0](https://gitlab.com/dropfort/dropfort_build/commit/cf0baa09541ca25b41184fe13ce43f102d8aadb5))

### [6.6.2](https://gitlab.com/dropfort/dropfort_build/compare/v6.6.1...v6.6.2) (2024-07-18)


### Bug Fixes

* **git:** remove deprecated husky elements ([8d86592](https://gitlab.com/dropfort/dropfort_build/commit/8d86592d2997b90101a9ab7d11a9a38ebfa71086))

### [6.6.1](https://gitlab.com/dropfort/dropfort_build/compare/v6.6.0...v6.6.1) (2024-07-16)


### Bug Fixes

* **dependencies:** allow codeception 10.x ([d2017a1](https://gitlab.com/dropfort/dropfort_build/commit/d2017a1b3c58a4aa53ac50d2deac7892df3f805a))
* **docker:** remove skip server ([39c81c3](https://gitlab.com/dropfort/dropfort_build/commit/39c81c38787c8f5b64c49cb5328232cebe781883))
* **docker:** remove version arg ([4e916a6](https://gitlab.com/dropfort/dropfort_build/commit/4e916a6a653c89547617f3381b3a970b61797189))

## [6.6.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.4.1...v6.6.0) (2024-06-10)


### Features

* **docker:** create the translations folder by default ([ab0badd](https://gitlab.com/dropfort/dropfort_build/commit/ab0badda73c565b59c13037c053824ce94470118))


### Bug Fixes

* **docker:** add skip server ([0739e6a](https://gitlab.com/dropfort/dropfort_build/commit/0739e6a260bf34640eccbd2bc0873889c8b6f676))
* **docs:** update env var table ([2e54325](https://gitlab.com/dropfort/dropfort_build/commit/2e54325f18966eef542d04e931e4a2da08dfad1c))

## [6.5.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.4.1...v6.5.0) (2024-06-10)


### Features

* **docker:** create the translations folder by default ([ab0badd](https://gitlab.com/dropfort/dropfort_build/commit/ab0badda73c565b59c13037c053824ce94470118))


### Bug Fixes

* **docker:** add skip server ([0739e6a](https://gitlab.com/dropfort/dropfort_build/commit/0739e6a260bf34640eccbd2bc0873889c8b6f676))
* **docs:** update env var table ([2e54325](https://gitlab.com/dropfort/dropfort_build/commit/2e54325f18966eef542d04e931e4a2da08dfad1c))

### [6.4.1](https://gitlab.com/dropfort/dropfort_build/compare/v6.4.0...v6.4.1) (2024-05-24)


### Bug Fixes

* **ci:** more remove envs ([ff38a5b](https://gitlab.com/dropfort/dropfort_build/commit/ff38a5b83ef0f5f411bfcd013ae19f30ad40d232))
* **ci:** remove env on non-env jobs ([84ae7c6](https://gitlab.com/dropfort/dropfort_build/commit/84ae7c6c70be0d2cfa40a32789d5919f2a8f1687))

## [6.4.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.3.1...v6.4.0) (2024-05-24)


### Features

* **ci:** mark deployment actions ([cd54ea1](https://gitlab.com/dropfort/dropfort_build/commit/cd54ea1f1473cea253dd4c358fe0f777d6a6fec2))
* **scripts:** commit scaffolded changes during auto updates ([a53e6ac](https://gitlab.com/dropfort/dropfort_build/commit/a53e6ac6865738e4ee475e6e057d4c106c6e5560))


### Bug Fixes

* **mariadb:** fix mariadb imports of dumps that now have the sandbox mode command in them ([db4bd27](https://gitlab.com/dropfort/dropfort_build/commit/db4bd279dbe8cf674cf341e4e4d8b7929ba3a41e))
* **update:** add maintenance mode env var ([6905c0d](https://gitlab.com/dropfort/dropfort_build/commit/6905c0dadb32dedac4f86c9d97ea1cb5f1a911f5))

### [6.3.1](https://gitlab.com/dropfort/dropfort_build/compare/v6.3.0...v6.3.1) (2024-05-22)


### Bug Fixes

* **php:** disable iocube by default ([c682fff](https://gitlab.com/dropfort/dropfort_build/commit/c682fff38971795b78aa142eebbe8124d951b71e))

## [6.3.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.2.0...v6.3.0) (2024-05-21)


### Features

* **traefik:** add support for 3.x ([c1fbbc9](https://gitlab.com/dropfort/dropfort_build/commit/c1fbbc95e4f7ec097674f3169e40279cc0678b53))

## [6.2.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.1.3...v6.2.0) (2024-05-16)


### Features

* update default terminus version ([72bc997](https://gitlab.com/dropfort/dropfort_build/commit/72bc997f8486b367d3017c29b3b09475b356ef64))

### [6.1.3](https://gitlab.com/dropfort/dropfort_build/compare/v6.1.2...v6.1.3) (2024-05-15)


### Bug Fixes

* **build:** update version number ([0b675ea](https://gitlab.com/dropfort/dropfort_build/commit/0b675ea290f419de782fe16d108c1af89a5f2be3))

### [6.1.2](https://gitlab.com/dropfort/dropfort_build/compare/v6.1.1...v6.1.2) (2024-05-15)


### Bug Fixes

* **traefik:** default to 2 ([23fe8fd](https://gitlab.com/dropfort/dropfort_build/commit/23fe8fd7ecccbbb1fe195ee155461c28c0dfb696))

### [6.1.1](https://gitlab.com/dropfort/dropfort_build/compare/v6.1.0...v6.1.1) (2024-05-06)


### Bug Fixes

* **drupal:** properly set config dir ([dd2c175](https://gitlab.com/dropfort/dropfort_build/commit/dd2c1754801726af70a3387e6aab7c2a0e222a1b))
* **lint:** remove warning ([b9d1a9b](https://gitlab.com/dropfort/dropfort_build/commit/b9d1a9bd62f5d13845b8697aaa4a49405d20baa6))

## [6.1.0](https://gitlab.com/dropfort/dropfort_build/compare/v6.0.1...v6.1.0) (2024-05-03)


### Features

* **multisite:** dynamically define config dir ([357bcb4](https://gitlab.com/dropfort/dropfort_build/commit/357bcb4357a290d0a0b53f5c153e7373aeab70f7))


### Bug Fixes

* **rsync:** remove npm and composer cache ([52ab0f0](https://gitlab.com/dropfort/dropfort_build/commit/52ab0f0f2dd3140c3ec390370f88e804d05fffdc))
* set eslint to ignore files in docroot ([fa08efe](https://gitlab.com/dropfort/dropfort_build/commit/fa08efe9e0e531f1b96b6760f1043c072b37e2ea))


### Continuous Integration

* correct path to docs ([91bd60a](https://gitlab.com/dropfort/dropfort_build/commit/91bd60a29ac9bc8895a332afc13cafa0e78f2808))


### Documentation

* add outDir to be compatible with gitlab ([b6cf4f3](https://gitlab.com/dropfort/dropfort_build/commit/b6cf4f3f36cb69072547bc278bf403316ce23035))


### Code Refactoring

* move single line comments into file comment ([fc485c3](https://gitlab.com/dropfort/dropfort_build/commit/fc485c3eb927c84b8f0ab1cab50c491ef962af27))

### [6.0.1](https://gitlab.com/dropfort/dropfort_build/compare/v6.0.0...v6.0.1) (2024-04-15)


### Build System

* **docker:** upgrade dev container to latest setup ([5cecbbe](https://gitlab.com/dropfort/dropfort_build/commit/5cecbbe3e596db7c952e93ae3e8f92b698cfe5c4))


### Documentation

* swap to vitepress ([bf5126e](https://gitlab.com/dropfort/dropfort_build/commit/bf5126e9a75a016ea984e285ad024aac337fb6b7))

## [6.0.0](https://gitlab.com/dropfort/dropfort_build/compare/v5.16.2...v6.0.0) (2024-04-12)


### Features

* **apache:** allow remote ip passthrough ([b85675b](https://gitlab.com/dropfort/dropfort_build/commit/b85675bbfc6a5dff5c29c4e788e8cfb4fc53197a))
* **ci:** add scaffold validation ([37eee44](https://gitlab.com/dropfort/dropfort_build/commit/37eee44418388497a80d0d9364c768112ced8daa))
* **config:** add stock performance config ([016cf18](https://gitlab.com/dropfort/dropfort_build/commit/016cf182aea4028ed7e4de3f7d229bfb2cc222c8))
* **docker:** upgrade base containers ([262e8ab](https://gitlab.com/dropfort/dropfort_build/commit/262e8ab2877486405344f4493f717410ba972800))
* **docs:** add notes about migrations ([dbfcf97](https://gitlab.com/dropfort/dropfort_build/commit/dbfcf9700a6461b028bc8936680323c13ea690bd))
* **docs:** add notes on scaffolding config dirs ([d8081fb](https://gitlab.com/dropfort/dropfort_build/commit/d8081fb2856dafd4805990c5b97517a5e2dbfc95))
* **env:** add feature as a valid df_env_type ([12e326d](https://gitlab.com/dropfort/dropfort_build/commit/12e326d5eb38ea1403af2f7d9448a28774eebd07))
* implement new config system for all js dependencies ([fbf6b4c](https://gitlab.com/dropfort/dropfort_build/commit/fbf6b4c152f42dc608848057f4dcff5a079d467c))
* **scaffold:** add more stock webroots to ignore ([e568385](https://gitlab.com/dropfort/dropfort_build/commit/e568385e2549e012983d0df71c52860c1681b8a9))
* **scaffold:** remove gulp ([7b075c0](https://gitlab.com/dropfort/dropfort_build/commit/7b075c0a78b481e6621f4119559dc98aa4898fc9))
* **services:** allow versioning of services config ([0810f17](https://gitlab.com/dropfort/dropfort_build/commit/0810f1782e364ea8e722233a7ab0f681cec19b24))
* **tests:** add basic auth config ([fbf5e9d](https://gitlab.com/dropfort/dropfort_build/commit/fbf5e9d0eb1497de96ce78814e690def3a33eeb8))
* **tests:** add basic auth config ([6599e46](https://gitlab.com/dropfort/dropfort_build/commit/6599e46373771a99a2cb8a64a3919d70c9a1096a))
* update auto update and security jobs ([28aa9b4](https://gitlab.com/dropfort/dropfort_build/commit/28aa9b482c26f1ff268f5cf05324680b3146873b))
* **zookeeper:** enable admin ui ([c11838a](https://gitlab.com/dropfort/dropfort_build/commit/c11838ad7f3875592e05585494833e6a29f6f248))


### Bug Fixes

* **archive:** only remove trailing /web to find source_folder ([ace7344](https://gitlab.com/dropfort/dropfort_build/commit/ace73449e946928bf87b8262b592f960fac77faa))
* **build:** add to allowed extensions ([7b48dca](https://gitlab.com/dropfort/dropfort_build/commit/7b48dca0afaa8c87b928d6d1e896be9d63e11a53))
* **build:** restore browsersync ([835c7bd](https://gitlab.com/dropfort/dropfort_build/commit/835c7bd1c5ff87ff8e34efbed3ca6b8130ef6ed2))
* **ci:** add known hosts ([e216285](https://gitlab.com/dropfort/dropfort_build/commit/e216285156cdf37c619e2ffe1ed5dccefc57045a))
* **ci:** add shield conf ([ecf8055](https://gitlab.com/dropfort/dropfort_build/commit/ecf80550282a4f5b1e616e17bd88127d3f06a4d7))
* **ci:** update path to dev container ([3661ef4](https://gitlab.com/dropfort/dropfort_build/commit/3661ef49c82669b5e616f11ad7266b86ebe2057c))
* **deploy:** allow drush cc drush to fail ([7f01560](https://gitlab.com/dropfort/dropfort_build/commit/7f015602e5b792c572e393ab468a80045579023b))
* **deps:** allow drush 12 ([4553e07](https://gitlab.com/dropfort/dropfort_build/commit/4553e070b6c2a01679d162ec96573684044d752d))
* **devcontainer:** only run npm build if project is not installed ([014f137](https://gitlab.com/dropfort/dropfort_build/commit/014f1374b507ea006fca2b59cc4ec69cf677c156))
* **dev:** disable extension signature check ([3a698e0](https://gitlab.com/dropfort/dropfort_build/commit/3a698e03c756ea92883e4235933729f3269938e3))
* disable StrictHostKeyChecking for pantheon deployments ([ba71b64](https://gitlab.com/dropfort/dropfort_build/commit/ba71b64a5e58ddcceed28d37d5cd3fa0baef1b49))
* **docker:** update before node install ([b9baecc](https://gitlab.com/dropfort/dropfort_build/commit/b9baecce5bed5b46125ca8d021b3d9e87d896914))
* **docker:** update before node install ([73951f6](https://gitlab.com/dropfort/dropfort_build/commit/73951f6c9022e005a81b016582c532ccd88c852d))
* **docs:** change script order ([7d9cdfc](https://gitlab.com/dropfort/dropfort_build/commit/7d9cdfc3685f9fc14a84d9075778d522d24e3561))
* **docs:** typo in ssh config ([8c182a1](https://gitlab.com/dropfort/dropfort_build/commit/8c182a186fd89ca7136577e98a188f84dc4386f7))
* **docs:** update config path ([f986055](https://gitlab.com/dropfort/dropfort_build/commit/f986055534f9c07e902a291df24df2588471b501))
* **git:** add trust to project dir ([cc58078](https://gitlab.com/dropfort/dropfort_build/commit/cc580788a8576b7a6e6efa74b347be7acfd5d0bb))
* **k8s:** update container build ([dea0246](https://gitlab.com/dropfort/dropfort_build/commit/dea0246fca75180a0135dc0223c9a3224455b53f))
* **macos:** remove mount point for ssh auth sock ([e8aa2c9](https://gitlab.com/dropfort/dropfort_build/commit/e8aa2c9cb9195cf6368e38f8c81d2d3466851e22))
* **node:** use newer install method ([f294eb3](https://gitlab.com/dropfort/dropfort_build/commit/f294eb318a6a3aac90ce59f0c150442c64fa08b5))
* **performance:** remove slow extension ([e6e452f](https://gitlab.com/dropfort/dropfort_build/commit/e6e452f0f7f62c4fd6e66126fa5c2211edd1617e))
* **rsync:** include core ([4ac86c0](https://gitlab.com/dropfort/dropfort_build/commit/4ac86c0062bb69605ddbdda4596816d77da7ff95))
* **rsync:** include core ([5ad9c50](https://gitlab.com/dropfort/dropfort_build/commit/5ad9c500d72e94dd9896669c3ea77b592265d0ca))
* **rsync:** use include core ([f74f2d2](https://gitlab.com/dropfort/dropfort_build/commit/f74f2d272414a9b4bf3d0ac281c7fe398db267a7))
* **scaffold:** add production container apache conf ([b175e94](https://gitlab.com/dropfort/dropfort_build/commit/b175e94ca3db3e0bd8d21a87612c2ea8a782380c))
* **tests:** prettier standards ([d18f20e](https://gitlab.com/dropfort/dropfort_build/commit/d18f20e888966e6acfd3cd0814e50dabfdf18ab2))
* **updater:** correct npm audit and security updates ([879776c](https://gitlab.com/dropfort/dropfort_build/commit/879776cff47e3e5eafc8cbf1e1848c3450f79ede))
* **vscode:** update eslint settings to new version ([bd21e0c](https://gitlab.com/dropfort/dropfort_build/commit/bd21e0cfa08be76cb5ef635572d39f5509c908e7))


### Build System

* **composer:** allow php-http plugin ([df9ffdf](https://gitlab.com/dropfort/dropfort_build/commit/df9ffdfb359a8ba848c6df38164ddb2d4ab0a20a))
* **npm:** move all dependencies into a single package ([24f220a](https://gitlab.com/dropfort/dropfort_build/commit/24f220a9886725e6de4b992b9d71dc4b8d6dd76d))
* **scaffold:** do not overwrite local settings file ([8e3b3ea](https://gitlab.com/dropfort/dropfort_build/commit/8e3b3ea139e88815caaa8d10622abc3971ab3082))


### Continuous Integration

* **containers:** add rules to publish_container job ([a7497bd](https://gitlab.com/dropfort/dropfort_build/commit/a7497bd3d3d252a110d80317c7c1934b5fa55ae7))
* **deploy:** add and remove deploy.lock file during and after deployment ([634de05](https://gitlab.com/dropfort/dropfort_build/commit/634de05ffde6c7080236e64cff8eeb5de1272ba1))


### Code Refactoring

* **ci:** add debug ([32e4bcf](https://gitlab.com/dropfort/dropfort_build/commit/32e4bcf81d020046bbd54c78d20a54fc506ee87d))
* **ci:** add debug ([386bc15](https://gitlab.com/dropfort/dropfort_build/commit/386bc15e394abdf00f95c948ba5e53e460b94fe3))
* **ci:** skip k8s jobs by default ([514958c](https://gitlab.com/dropfort/dropfort_build/commit/514958cdab8f596087dad7bee4d5fcb30370f053))
* **docker:** optmize build file ([3014c6a](https://gitlab.com/dropfort/dropfort_build/commit/3014c6a8ad4af4e9a396dfa7c81872f747c5fb60))
* **docs:** sort tasks ([7e58f7f](https://gitlab.com/dropfort/dropfort_build/commit/7e58f7f099af8f26604985d71b68685d5af7dcc4))
* swap default config to patches and minor changes ([dee126c](https://gitlab.com/dropfort/dropfort_build/commit/dee126c59dea556dfbd3ca88ea3cd1adbc816970))
* **tests:** add smarter visual regression ([85499dc](https://gitlab.com/dropfort/dropfort_build/commit/85499dc8763b00ca23ac72bb61df841edc632abb))


### Documentation

* **docker:** properly match php version in env ([31842d0](https://gitlab.com/dropfort/dropfort_build/commit/31842d082a2d998ea0f854d52458a0a11963d01c))
* **install:** add composer command ([966a149](https://gitlab.com/dropfort/dropfort_build/commit/966a149ad181036096b60d2298ec6747745e6472))
* update comments for all files ([e3d4cd5](https://gitlab.com/dropfort/dropfort_build/commit/e3d4cd5a5e43bab1c6adc88b538fa90b9b020217))

### [5.16.2](https://gitlab.com/dropfort/dropfort_build/compare/v5.16.1...v5.16.2) (2023-10-19)


### Bug Fixes

* **deps:** update requirements ([993809c](https://gitlab.com/dropfort/dropfort_build/commit/993809c954aa5a5e52c8078cd09c97c2681bb4b4))

### [5.16.1](https://gitlab.com/dropfort/dropfort_build/compare/v5.16.0...v5.16.1) (2023-10-19)


### Bug Fixes

* **performance:** remove slow extension ([c750966](https://gitlab.com/dropfort/dropfort_build/commit/c750966876c1cfd3570df9584444b52c6881b0e3))

## [5.16.0](https://gitlab.com/dropfort/dropfort_build/compare/v5.15.0...v5.16.0) (2023-10-02)


### Features

* **npm:** allow setting version ([e0e79a3](https://gitlab.com/dropfort/dropfort_build/commit/e0e79a3be1f12d36eca239da4811b46955222892))


### Bug Fixes

* **ci:** add shield conf ([243e19e](https://gitlab.com/dropfort/dropfort_build/commit/243e19e1d1f13cbb1ae4b8cdf9782e177e17e9df))


### Code Refactoring

* **ci:** add config status output ([71ab323](https://gitlab.com/dropfort/dropfort_build/commit/71ab3231cef433ae88b76cb348d48a22c9e3e9cc))

## [5.15.0](https://gitlab.com/dropfort/dropfort_build/compare/v5.14.1...v5.15.0) (2023-08-17)


### Features

* **tests:** add basic auth config ([f47b53a](https://gitlab.com/dropfort/dropfort_build/commit/f47b53ac64f66d607386a4b066187790d824e6da))
* **tests:** add basic auth config ([c905d18](https://gitlab.com/dropfort/dropfort_build/commit/c905d18eb6bea7d51d815654a8eb2e645cbb564a))


### Bug Fixes

* **build:** add to allowed extensions ([4d2a547](https://gitlab.com/dropfort/dropfort_build/commit/4d2a547ac17440f972a14d7c8799ac74a1a7e868))
* **ci:** add known hosts ([15bf34e](https://gitlab.com/dropfort/dropfort_build/commit/15bf34ee8b7fc8326c0637d5a3c1de4ff64861a7))
* **dependencies:** restrict drush sub 12 ([8b248cf](https://gitlab.com/dropfort/dropfort_build/commit/8b248cfc805f1cc676b2dce3288c04d3d5e1c051))
* **deps:** remove drupal console ([a8e562d](https://gitlab.com/dropfort/dropfort_build/commit/a8e562d58742c44cff6162781476f15d4414ccbb))
* **tests:** add tag ([81dca25](https://gitlab.com/dropfort/dropfort_build/commit/81dca250cacb9c01e97a0149e60ba11e3de3fbbf))
* **tests:** prettier standards ([18f37a6](https://gitlab.com/dropfort/dropfort_build/commit/18f37a619e47cba7a42f7363b2514a614f03bf54))


### Documentation

* **install:** add composer command ([62089af](https://gitlab.com/dropfort/dropfort_build/commit/62089afcf5f11a92cf7de08c3b15431f9ef4b604))


### Code Refactoring

* **docs:** sort tasks ([812e57e](https://gitlab.com/dropfort/dropfort_build/commit/812e57e3d977fc445cf14e8ab7cfb090ed69769b))

### [5.14.1](https://gitlab.com/dropfort/dropfort_build/compare/v5.14.0...v5.14.1) (2023-08-03)


### Bug Fixes

* **dev:** disable extension signature check ([6fca3b9](https://gitlab.com/dropfort/dropfort_build/commit/6fca3b96b8ad35aeb345618b06c31f6fa521018d))

## [5.14.0](https://gitlab.com/dropfort/dropfort_build/compare/v5.13.0...v5.14.0) (2023-07-25)


### Features

* **docs:** add notes about migrations ([de21805](https://gitlab.com/dropfort/dropfort_build/commit/de2180544ae7777ba8eb1348c1a63a0d6aa85926))

## [5.13.0](https://gitlab.com/dropfort/dropfort_build/compare/v5.12.2...v5.13.0) (2023-07-14)


### Features

* **npm:** add a dev build script ([4101136](https://gitlab.com/dropfort/dropfort_build/commit/4101136b70726bc2b6f8f00bf50773906d558cf1))


### Bug Fixes

* **npm:** remove duplicate build script ([a595ae6](https://gitlab.com/dropfort/dropfort_build/commit/a595ae6cfb61287704955cf8c78f49049ebae5b2))

### [5.12.2](https://gitlab.com/dropfort/dropfort_build/compare/v5.12.1...v5.12.2) (2023-06-28)


### Bug Fixes

* **docker:** add profile for plantuml ([e9475f2](https://gitlab.com/dropfort/dropfort_build/commit/e9475f236d9e2e3e7fc70a4ffb30e627ab6368df))

### [5.12.1](https://gitlab.com/dropfort/dropfort_build/compare/v5.12.0...v5.12.1) (2023-06-23)

## [5.12.0](https://gitlab.com/dropfort/dropfort_build/compare/v5.11.0...v5.12.0) (2023-06-19)


### Features

* **build:** add build wrapper ([dff59ae](https://gitlab.com/dropfort/dropfort_build/commit/dff59ae17e3972a66ce138462edd054d2b7bdae1))


### Bug Fixes

* ignore files generated by auto update jobs ([45574d4](https://gitlab.com/dropfort/dropfort_build/commit/45574d469c742c2e5518bce141081881cd8667de))

## [5.11.0](https://gitlab.com/dropfort/dropfort_build/compare/v5.10.0...v5.11.0) (2023-06-13)


### Features

* **ci:** add env vars override ([1a07d85](https://gitlab.com/dropfort/dropfort_build/commit/1a07d8518cf6a473f8627840d78ff96c4dbb71b8))
* **ci:** add node env override ([31ad05f](https://gitlab.com/dropfort/dropfort_build/commit/31ad05fdfa07bdf58224f5c076fc7d1b5ced8034))
* **scaffold:** add backup script to scaffold ([4c1d1bf](https://gitlab.com/dropfort/dropfort_build/commit/4c1d1bf1ccae9e2be78f692c00015a38397f8f4d))


### Bug Fixes

* **build:** use updated npx name ([5899c2b](https://gitlab.com/dropfort/dropfort_build/commit/5899c2bfa0995fd55220829a15ce924d1cedfa0f))
* **scripts:** add grep js back to acceptance tests ([a0984ed](https://gitlab.com/dropfort/dropfort_build/commit/a0984edd99200b75e3ee76015152b17d2fa449df))


### Build System

* **npm:** add node-env-run ([7eb3501](https://gitlab.com/dropfort/dropfort_build/commit/7eb3501145b496337f834c0ebefb5108f508dc3e))


### Code Refactoring

* **ssh:** use base64 encoded keys ([f1b6e2a](https://gitlab.com/dropfort/dropfort_build/commit/f1b6e2a9bbb428440ebd809c3c85c8ca9cd4e73e))

## [5.10.0](https://gitlab.com/dropfort/dropfort_build/compare/v5.9.3...v5.10.0) (2023-05-26)


### Features

* **deploy:** add deploy freeze support ([0fd3af5](https://gitlab.com/dropfort/dropfort_build/commit/0fd3af56ca4dfb0f140bd047efe876dc323c92f5))

### [5.9.3](https://gitlab.com/dropfort/dropfort_build/compare/v5.9.2...v5.9.3) (2023-05-25)


### Bug Fixes

* **deploy:** update rsync exclude ([ae2a770](https://gitlab.com/dropfort/dropfort_build/commit/ae2a770f36772d96e2bd13eab2b616210e3b9e57))


### Code Refactoring

* **dependencies:** allow drupal 10 ([495b5d6](https://gitlab.com/dropfort/dropfort_build/commit/495b5d618bd7e8e4f4d773c7744996b301175e18))

### [5.9.2](https://gitlab.com/dropfort/dropfort_build/compare/v5.9.1...v5.9.2) (2023-05-25)


### Bug Fixes

* **scaffold:** update default messages ([08a46ed](https://gitlab.com/dropfort/dropfort_build/commit/08a46ed37f1f7418321026cdb25ad91beb19da99))

### [5.9.1](https://gitlab.com/dropfort/dropfort_build/compare/v5.9.0...v5.9.1) (2023-05-25)

## [5.9.0](https://gitlab.com/dropfort/dropfort_build/compare/v5.8.1...v5.9.0) (2023-05-25)


### Features

* **autoupdates:** add scaffold changes automatically ([f00f12c](https://gitlab.com/dropfort/dropfort_build/commit/f00f12c66bb356a4d3609908e3eb7c8bed10d0a1))
* **ci:** clear dropfort update on pantheon ([e423f47](https://gitlab.com/dropfort/dropfort_build/commit/e423f47e532c4b86b33c9db9dc32808c2985ffc2))
* **solr:** allow version selection ([b26a0a5](https://gitlab.com/dropfort/dropfort_build/commit/b26a0a5f046d520239fa39f624d24c52a7b054a0))


### Bug Fixes

* **attributes:** update to match scaffold ([051fd0b](https://gitlab.com/dropfort/dropfort_build/commit/051fd0b2ce7b1e70d15430f97e7d0e0b200f6ffb))
* **ci:** pantheon deploy ([13c3878](https://gitlab.com/dropfort/dropfort_build/commit/13c387845c92e40bfeeb472e113a7c02574b7561))
* **ci:** remote control repos ([2758f38](https://gitlab.com/dropfort/dropfort_build/commit/2758f3868f1430d3f8f39f4120adbcdd2d4f2c58))
* **docs:** spelling mistakes ([7032c4a](https://gitlab.com/dropfort/dropfort_build/commit/7032c4a82e39c8e20fe3d984c8060671bed83974))
* **git:** update gitattributes ([33238cf](https://gitlab.com/dropfort/dropfort_build/commit/33238cf8f3fdbe1df5a9a0b3cac80f6cfbb737fe))
* **php81:** add mariadb repos ([b68524c](https://gitlab.com/dropfort/dropfort_build/commit/b68524cd49a40423c1575742edad0470bb9a013e))

### [5.8.1](https://gitlab.com/dropfort/dropfort_build/compare/v5.8.0...v5.8.1) (2023-04-20)


### Bug Fixes

* **ci:** skip smudge on external control repos ([09b9348](https://gitlab.com/dropfort/dropfort_build/commit/09b9348d5f983d7563c5d83b76c7bd8f4262d834))

## [5.8.0](https://gitlab.com/dropfort/dropfort_build/compare/v5.7.4...v5.8.0) (2023-04-20)


### Features

* **ci:** clear the dropfort update status ([4e067d5](https://gitlab.com/dropfort/dropfort_build/commit/4e067d5ed9ab6ff86c8805135b20a3697e2502d2))

### [5.7.4](https://gitlab.com/dropfort/dropfort_build/compare/v5.7.3...v5.7.4) (2023-03-30)


### Reverts

* Revert "fix(acquia): disable hooks" ([0d89fd5](https://gitlab.com/dropfort/dropfort_build/commit/0d89fd5497012301ce4caed7e961a6c182863297))

### [5.7.3](https://gitlab.com/dropfort/dropfort_build/compare/v5.7.2...v5.7.3) (2023-03-30)


### Bug Fixes

* **acquia:** disable hooks ([a8772e3](https://gitlab.com/dropfort/dropfort_build/commit/a8772e3730a387cceb10c030eff59f05c48bcdaa))

### [5.7.2](https://gitlab.com/dropfort/dropfort_build/compare/v5.7.1...v5.7.2) (2023-03-25)


### Bug Fixes

* wait for install ([027a9e6](https://gitlab.com/dropfort/dropfort_build/commit/027a9e6104faffc69db5f673ef9f5ea3533d161f))

### [5.7.1](https://gitlab.com/dropfort/dropfort_build/compare/v5.7.0...v5.7.1) (2023-03-23)


### Bug Fixes

* **ci:** make composer second ([8816587](https://gitlab.com/dropfort/dropfort_build/commit/88165872912700db3113d62302a46d5559920f3a))


### Performance Improvements

* **ci:** :zap: run composer and npm install at the same time ([0be845b](https://gitlab.com/dropfort/dropfort_build/commit/0be845bfbcb3b58379c5c618cee1a6ea40386fbd))

## [5.7.0](https://gitlab.com/dropfort/dropfort_build/compare/v5.6.2...v5.7.0) (2023-03-22)


### Features

* **devcontainer:** poststart faster ([228d6fb](https://gitlab.com/dropfort/dropfort_build/commit/228d6fb024a94c062224ef00c5dacb04a573fe14))
* **search:** add ability to handle search api solr updates through admin module ([0ee7f29](https://gitlab.com/dropfort/dropfort_build/commit/0ee7f29e1c5c3a954b6365f91100d5ac941d5fb1))


### Bug Fixes

* **ci:** disable hooks ([548c2c5](https://gitlab.com/dropfort/dropfort_build/commit/548c2c5b7a0b30d3980e67c906f68732cb168eda))


### Performance Improvements

* **ci:** :zap: run composer and npm install at the same time ([7355e6f](https://gitlab.com/dropfort/dropfort_build/commit/7355e6fce1a45231a51b344bebe87a7a84ded063))


### Documentation

* **index:** rename default page title ([16eb1b4](https://gitlab.com/dropfort/dropfort_build/commit/16eb1b4dd68bf18203f6555656ddd4c20801ed63))
* **local-env:** set useful text on links ([c0890f4](https://gitlab.com/dropfort/dropfort_build/commit/c0890f4c7ea5727a775c438dc8b338a5101c1c73))

### [5.6.2](https://gitlab.com/dropfort/dropfort_build/compare/v5.6.1...v5.6.2) (2023-03-15)


### Documentation

* add troubleshooting section ([2ba02f5](https://gitlab.com/dropfort/dropfort_build/commit/2ba02f588d83835b51fef14e3dfaf5c3c094009b))

### [5.6.1](https://gitlab.com/dropfort/dropfort_build/compare/v5.6.0...v5.6.1) (2023-03-14)


### Bug Fixes

* **dependencies:** swap drupal-codeception ([449e256](https://gitlab.com/dropfort/dropfort_build/commit/449e256a6edba51699e682dd9c04e587a3ccfde8))
* **ssh:** mount known_hosts ([dc1ff46](https://gitlab.com/dropfort/dropfort_build/commit/dc1ff460f45a9d0acf34ba5fe6b187430fbff7a6))

## [5.6.0](https://gitlab.com/dropfort/dropfort_build/compare/v5.5.2...v5.6.0) (2023-03-09)


### Features

* **ci:** add pantheon-specific update job ([d52f803](https://gitlab.com/dropfort/dropfort_build/commit/d52f8037d6885a1a60e002ade1495e7d22c88b2b))
* **vscode:** add markdown lint ([89b3467](https://gitlab.com/dropfort/dropfort_build/commit/89b3467f05c7cb714b89fe7cf20ff17fb41d187d))


### Bug Fixes

* **ci:** ensure known host addition commands run individually ([530281d](https://gitlab.com/dropfort/dropfort_build/commit/530281d3b009d43511ec4bb34d39242232e59792))

### [5.5.2](https://gitlab.com/dropfort/dropfort_build/compare/v5.5.1...v5.5.2) (2023-03-07)


### Bug Fixes

* **docs:** move tip wrapper ([165be19](https://gitlab.com/dropfort/dropfort_build/commit/165be19b3b6f8b2b65a13ccae14f47a26636c424))

### [5.5.1](https://gitlab.com/dropfort/dropfort_build/compare/v5.5.0...v5.5.1) (2023-03-07)


### Bug Fixes

* **docs:** add escape for twig example ([9fe83d2](https://gitlab.com/dropfort/dropfort_build/commit/9fe83d2260c1f9351f783673023a53a6ef7def98))

## [5.5.0](https://gitlab.com/dropfort/dropfort_build/compare/v5.4.2...v5.5.0) (2023-03-07)


### Features

* **ssh:** add known hosts ([44b87db](https://gitlab.com/dropfort/dropfort_build/commit/44b87db9dd216f47f91f114d505b7f4506be2b0a))


### Bug Fixes

* **ci:** run config import before auto update ([5a76286](https://gitlab.com/dropfort/dropfort_build/commit/5a762866b39db62d1160f8de53bcf4ee62d675b0))
* **docs:** update known hosts description ([008cf03](https://gitlab.com/dropfort/dropfort_build/commit/008cf03b02a38172ccd62e8f9bec74b3e366d705))
* **docs:** use tip instead of info ([f866642](https://gitlab.com/dropfort/dropfort_build/commit/f866642cf3dcf2a847c24cf2d93cba948123b075))

### [5.4.2](https://gitlab.com/dropfort/dropfort_build/compare/v5.4.1...v5.4.2) (2023-02-24)


### Bug Fixes

* **docker:** allow multisite on TLS port ([fd4bd3e](https://gitlab.com/dropfort/dropfort_build/commit/fd4bd3e28c5fcc49f8b8dea366422c18e5e9da93))
* **updates:** add auto updates ([fc852b2](https://gitlab.com/dropfort/dropfort_build/commit/fc852b2e5fbcc1e0e28d4dc9140eeae608d87a4a))
* **updates:** no verify ([38e2317](https://gitlab.com/dropfort/dropfort_build/commit/38e2317d5a2a4e5e04ca4f65889bd3da3f45d3b1))

### [5.4.1](https://gitlab.com/dropfort/dropfort_build/compare/v5.4.0...v5.4.1) (2023-02-22)


### Bug Fixes

* **multisite:** directory detection ([c08b9c0](https://gitlab.com/dropfort/dropfort_build/commit/c08b9c0d0c7b6a3d14c09e06ad4c97bd6a9d965d))

## [5.4.0](https://gitlab.com/dropfort/dropfort_build/compare/v5.3.0...v5.4.0) (2023-02-22)


### Features

* **drupal:** start adding multisite support ([84e6d50](https://gitlab.com/dropfort/dropfort_build/commit/84e6d50d6b555fac5ab57f5d93754c541860c7a4))

## [5.3.0](https://gitlab.com/dropfort/dropfort_build/compare/v5.2.1...v5.3.0) (2023-02-21)


### Features

* **ci:** add automated config updates ([1be4cee](https://gitlab.com/dropfort/dropfort_build/commit/1be4ceeaadcaae85377ff7cc39846d6b873ceb70))


### Bug Fixes

* **config:** add whitespace ([3193ba3](https://gitlab.com/dropfort/dropfort_build/commit/3193ba36400904d7956314d0b9699b11b8f18267))

### [5.2.1](https://gitlab.com/dropfort/dropfort_build/compare/v5.2.0...v5.2.1) (2023-02-17)


### Bug Fixes

* **docker:** add php.ini ([08abe08](https://gitlab.com/dropfort/dropfort_build/commit/08abe083511f3952771728f0974aae2ba4295cd5))

## [5.2.0](https://gitlab.com/dropfort/dropfort_build/compare/v5.1.6...v5.2.0) (2023-02-17)


### Features

* **docker:** allow https connections ([82c1294](https://gitlab.com/dropfort/dropfort_build/commit/82c1294f937824be0bc23a2881c21e40569afbdf))
* **tests:** deprecate backstop ([6d79b0b](https://gitlab.com/dropfort/dropfort_build/commit/6d79b0b4ee068dea94a3bb94000df107572cb8db))


### Bug Fixes

* **warnings:** hide xdebug warning messages ([230e11d](https://gitlab.com/dropfort/dropfort_build/commit/230e11d5fca3171101557460b08cb7d28e13f59a))

### [5.1.6](https://gitlab.com/dropfort/dropfort_build/compare/v5.1.5...v5.1.6) (2023-02-03)

### [5.1.5](https://gitlab.com/dropfort/dropfort_build/compare/v5.1.4...v5.1.5) (2023-02-01)


### Bug Fixes

* **env:** update to override ([ef295ec](https://gitlab.com/dropfort/dropfort_build/commit/ef295ecb9f3cf53d2bde3cd4ab1a7cf5910d3ef0))


### Documentation

* **composer:** add note on scaffolding without local php ([f866f72](https://gitlab.com/dropfort/dropfort_build/commit/f866f72d7a92b2c17dbe0933836ed9839409ea46))

### [5.1.4](https://gitlab.com/dropfort/dropfort_build/compare/v5.1.3...v5.1.4) (2023-02-01)


### Bug Fixes

* **docker:** scaffold new file ([d3bc165](https://gitlab.com/dropfort/dropfort_build/commit/d3bc1652e996e6da4da7b2438570dfb819dfa0af))

### [5.1.3](https://gitlab.com/dropfort/dropfort_build/compare/v5.1.2...v5.1.3) (2023-02-01)


### Bug Fixes

* **drupal:** update settings.php with latest ([8aff431](https://gitlab.com/dropfort/dropfort_build/commit/8aff4316581e98772dbc0a2910d70539bb1b782d))


### Code Refactoring

* **versions:** use node lts ([4e4889a](https://gitlab.com/dropfort/dropfort_build/commit/4e4889a9ebce4246e935115c5a3e71f28320936a))

### [5.1.2](https://gitlab.com/dropfort/dropfort_build/compare/v5.1.1...v5.1.2) (2023-01-31)


### Bug Fixes

* **env:** build errors when dotenv is missing ([9c1a947](https://gitlab.com/dropfort/dropfort_build/commit/9c1a94745daad390ce0b22a059509b34d3c18d86))
* **eslint:** disable in acquia docroot ([c2abee9](https://gitlab.com/dropfort/dropfort_build/commit/c2abee977d00fc22f6f3adc04bf4a07ab4553612))
* **scaffold:** add acquia env var in split check ([0bd2f8e](https://gitlab.com/dropfort/dropfort_build/commit/0bd2f8e491e550b9f5014ae7e869f0a6f02bf156))

### [5.1.1](https://gitlab.com/dropfort/dropfort_build/compare/v5.1.0...v5.1.1) (2023-01-31)


### Bug Fixes

* **docs:** update sample commands ([ef1d7ad](https://gitlab.com/dropfort/dropfort_build/commit/ef1d7ad1a3650eba4559063e3bee779b22ddd0a3))
* **scaffold:** update load.environment ([bfb1331](https://gitlab.com/dropfort/dropfort_build/commit/bfb13313e33128deac59da548d1d57a3e9052478))

## [5.1.0](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0...v5.1.0) (2023-01-31)


### Features

* update docs for solr configuration ([d44e00a](https://gitlab.com/dropfort/dropfort_build/commit/d44e00a2612d327d899e5c0657b4a4dc2d1852fe))


### Code Refactoring

* **docs:** add composer cli commands ([4794aa5](https://gitlab.com/dropfort/dropfort_build/commit/4794aa5e111b643bf1e42a1b34ebfcc02ddd4c1b))
* **scaffold:** scaffold more files ([d234aec](https://gitlab.com/dropfort/dropfort_build/commit/d234aecce78cac674f14fbcb43bc0c26f57fdb53))

## [5.0.0](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.25...v5.0.0) (2023-01-27)


### Features

* **dev:** simplify the onboarding of a project ([4e376fb](https://gitlab.com/dropfort/dropfort_build/commit/4e376fbd5f564abde00343d15de551d3a69ce729))


### Bug Fixes

* **scaffold:** add mariadb cnf ([8472878](https://gitlab.com/dropfort/dropfort_build/commit/8472878365545c1e62788886f970c7f9a4381d6a))

## [5.0.0-beta.25](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.24...v5.0.0-beta.25) (2023-01-26)


### Bug Fixes

* **env:** add file assoc for env files ([4eedd2f](https://gitlab.com/dropfort/dropfort_build/commit/4eedd2f452e3e1094f90bb262e3a42fe62a264e4))
* **mariadb:** use existing tag ([d768898](https://gitlab.com/dropfort/dropfort_build/commit/d7688988126906fda49cdb7640751c9893643cd1))

## [5.0.0-beta.24](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.23...v5.0.0-beta.24) (2023-01-25)


### Features

* **docker:** add ability to control php memory limit ([1b09613](https://gitlab.com/dropfort/dropfort_build/commit/1b096133a5706c0c208ec26b0f5f9e6cea4eceb0))


### Code Refactoring

* **deploy:** update acquia ([6919aa0](https://gitlab.com/dropfort/dropfort_build/commit/6919aa0020c74e75dd832bef289b097d48107554))

## [5.0.0-beta.23](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.22...v5.0.0-beta.23) (2023-01-20)


### Features

* **mariadb:** set tx isolation based on drupal recommendations ([b62e48a](https://gitlab.com/dropfort/dropfort_build/commit/b62e48aee4da54721d29326ff1a20059ad73bbdd))
* **phpcs:** enable auto detect ([d4a986b](https://gitlab.com/dropfort/dropfort_build/commit/d4a986b74e833cacad58b9f19cce048c80913972))


### Bug Fixes

* **drupal:** add trusted host pattern ([69eb4ae](https://gitlab.com/dropfort/dropfort_build/commit/69eb4aeda31c8f3a498a980380722f475830fae2))
* **rsync:** exclude additional files ([4940222](https://gitlab.com/dropfort/dropfort_build/commit/4940222365c4bd008e11ede27c864ea7266d2985))
* **selenium:** add export ([94bf502](https://gitlab.com/dropfort/dropfort_build/commit/94bf5021fb05d0a7c9ecfa95acd98ee0539f18e2))

## [5.0.0-beta.22](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.21...v5.0.0-beta.22) (2023-01-13)


### Bug Fixes

* **log:** update deploy status log location ([f17d284](https://gitlab.com/dropfort/dropfort_build/commit/f17d284b444dc35667fc260e95d0cf4e116d88ca))


### Code Refactoring

* **deploy:** update acquia/pantheon deploy ([87b2874](https://gitlab.com/dropfort/dropfort_build/commit/87b2874944cb50b08206c2222832344296d84573))

## [5.0.0-beta.21](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.20...v5.0.0-beta.21) (2022-12-23)


### Bug Fixes

* **pantheon:** restore folder path ([a72359b](https://gitlab.com/dropfort/dropfort_build/commit/a72359b8ebf6728fd5211f6c487182af0a44b325))

## [5.0.0-beta.20](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.19...v5.0.0-beta.20) (2022-12-22)


### Features

* **script:** add qa release script ([4f8645a](https://gitlab.com/dropfort/dropfort_build/commit/4f8645a9a854be7a0b3f0a5deff69c692d8a476a))


### Bug Fixes

* **docker:** correct env variable format for composer version ([774d666](https://gitlab.com/dropfort/dropfort_build/commit/774d66650b5b3873ad3eee009acd71793e43e5b2))
* **pantheon:** correct rsync target for deploys ([6ab7ba9](https://gitlab.com/dropfort/dropfort_build/commit/6ab7ba9b78f167c1730026d8681c4abab18afb58))

## [5.0.0-beta.19](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.18...v5.0.0-beta.19) (2022-12-13)


### Bug Fixes

* **ci:** disable acceptance on other hosts ([0ee9acf](https://gitlab.com/dropfort/dropfort_build/commit/0ee9acfa7da3bc1f72f84af77707f2719d1d5be5))
* **ci:** disable acceptance on other hosts ([e6b7a91](https://gitlab.com/dropfort/dropfort_build/commit/e6b7a91690d974cf877aefff60466bff5e95de55))
* **ci:** include core folder explicitly ([9feee89](https://gitlab.com/dropfort/dropfort_build/commit/9feee89b88dac5a758329b7b1795cec17a33cd51))

## [5.0.0-beta.18](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.17...v5.0.0-beta.18) (2022-12-13)


### Bug Fixes

* **acquia:** set branch ([ab6906b](https://gitlab.com/dropfort/dropfort_build/commit/ab6906b8fd91d69330de5dbad93c2d1e9313551f))
* **acquia:** update git sync commands ([404aa37](https://gitlab.com/dropfort/dropfort_build/commit/404aa372b1faa11726d47b88bdc87416b6203a57))
* **ci:** change artifact location ([d235adc](https://gitlab.com/dropfort/dropfort_build/commit/d235adc623e454cb98ab0401888c6972d9b0a112))
* **ci:** use webroot var ([63a7861](https://gitlab.com/dropfort/dropfort_build/commit/63a786137f2e52c99eb41a0508f2187e8c9c4760))


### Code Refactoring

* **acquia:** allow hooks to sync ([2630948](https://gitlab.com/dropfort/dropfort_build/commit/2630948029f26c9258d4a1faabd374908bd95393))
* **gitdeploy:** add cvs exclude ([6ede555](https://gitlab.com/dropfort/dropfort_build/commit/6ede55519c52fe9fb8b3dc9ed1e0d32dc01f501e))

## [5.0.0-beta.17](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.16...v5.0.0-beta.17) (2022-12-05)


### Features

* **npm:** allow version selection ([e618a55](https://gitlab.com/dropfort/dropfort_build/commit/e618a55d1a4d30c161175f36394fb2d404a09d17))


### Bug Fixes

* **bin:** use bin and vendor bin ([3155f52](https://gitlab.com/dropfort/dropfort_build/commit/3155f52dcc0e597c3e16bb91133dd2533d6c516b))
* **ci:** add include for acquia ([f322a38](https://gitlab.com/dropfort/dropfort_build/commit/f322a384a1cddb6b64ddd7929e12afde97eabdee))


### Code Refactoring

* **composer:** check for existing ([e09a851](https://gitlab.com/dropfort/dropfort_build/commit/e09a8516cc11ea12ca10a173c2c1cb6bdec99dbe))

## [5.0.0-beta.16](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.15...v5.0.0-beta.16) (2022-12-01)


### Features

* **acquia:** support vendor bin ([b2f6064](https://gitlab.com/dropfort/dropfort_build/commit/b2f606428985881a5f2c1a26105075435f3298df))

## [5.0.0-beta.15](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.14...v5.0.0-beta.15) (2022-12-01)


### Features

* **acquia:** support vendor bin ([a8bf784](https://gitlab.com/dropfort/dropfort_build/commit/a8bf784d02e87565202cce5f4d6476717cf6cff8))


### Code Refactoring

* **rsync:** add comments ([fc6883b](https://gitlab.com/dropfort/dropfort_build/commit/fc6883be188aef0cddce82b564023b99017bd6aa))

## [5.0.0-beta.14](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.13...v5.0.0-beta.14) (2022-11-30)


### Bug Fixes

* **drupal:** use php elvis operator ([31ab853](https://gitlab.com/dropfort/dropfort_build/commit/31ab853f9bcc327109f79e3f9f01aed9c60aeb34))

## [5.0.0-beta.13](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.12...v5.0.0-beta.13) (2022-11-30)


### Bug Fixes

* **dependencies:** allow psych 0.11 ([2a6a826](https://gitlab.com/dropfort/dropfort_build/commit/2a6a826792245d5501d40f4296e6ff2dc4e96a31))

## [5.0.0-beta.12](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.11...v5.0.0-beta.12) (2022-11-24)


### Features

* **ci:** use drush deploy ([b5a3212](https://gitlab.com/dropfort/dropfort_build/commit/b5a3212facedbdfaa6173ea1d3f81b686368ea3c))
* **vscode:** add auto rename tag ([cc57fa6](https://gitlab.com/dropfort/dropfort_build/commit/cc57fa6a7dcd3121d76f99fe6ef876db25e06af1))

## [5.0.0-beta.11](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.10...v5.0.0-beta.11) (2022-10-28)


### Bug Fixes

* **ci:** do not run update migrations for pantheon ([f47998e](https://gitlab.com/dropfort/dropfort_build/commit/f47998e6d0cea5e62a9f9ed2fdec1f5e4f6b558c))

## [5.0.0-beta.10](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.9...v5.0.0-beta.10) (2022-10-17)


### Bug Fixes

* **ci:** include all required templates ([371954c](https://gitlab.com/dropfort/dropfort_build/commit/371954c8bfa8ff91b64bef3e1aaaf09e6ba33cd8))

## [5.0.0-beta.9](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.8...v5.0.0-beta.9) (2022-10-17)


### Bug Fixes

* **ci:** correct target pipeline logic for security checks ([568890d](https://gitlab.com/dropfort/dropfort_build/commit/568890d9c66ff05db4ab6b5b7a411afd4e0dd765))
* **ci:** update target pipeline rules for build ([3df1c48](https://gitlab.com/dropfort/dropfort_build/commit/3df1c480ed8d49c44f7f3465efbfa826e13bc730))

## [5.0.0-beta.8](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.7...v5.0.0-beta.8) (2022-10-07)


### Features

* **ci:** refactor all ci templates to use rules instead of only and except ([cf676a1](https://gitlab.com/dropfort/dropfort_build/commit/cf676a121b48c82e4213cee4dd3ccc41528a2ecb))


### Bug Fixes

* add paths to ignore lint ([b970069](https://gitlab.com/dropfort/dropfort_build/commit/b970069a09ae9894fd9c7086b601f335a276a192))
* add paths to ignore lint ([e23fed0](https://gitlab.com/dropfort/dropfort_build/commit/e23fed0a224a4b23396d24010739fd09e8f665c1))
* **config:** call php lint and ignore specified folders ([c1c5714](https://gitlab.com/dropfort/dropfort_build/commit/c1c5714a8b7d014e4d225505e6f34d4b12361d42))
* remove node fetch and all core scaffold in allowed plugins ([80cc5e0](https://gitlab.com/dropfort/dropfort_build/commit/80cc5e0b77625cc6d66db0f897071ef65303dd32))


### Build System

* **composer:** do not allow core-composer-scaffold to run ([2058c50](https://gitlab.com/dropfort/dropfort_build/commit/2058c50e4fb5490df9cf94ef6775f2f7df362301))

## [5.0.0-beta.7](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.6...v5.0.0-beta.7) (2022-09-23)


### Bug Fixes

* **dependencies:** add npm selenium ([2c3e2a8](https://gitlab.com/dropfort/dropfort_build/commit/2c3e2a8ec797bebd26f6bf736418a61303de202e))

## [5.0.0-beta.6](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.5...v5.0.0-beta.6) (2022-09-20)

## [5.0.0-beta.5](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.4...v5.0.0-beta.5) (2022-09-16)


### Bug Fixes

* **debian:** don't ask questions during updates ([84729f3](https://gitlab.com/dropfort/dropfort_build/commit/84729f3bc519e328f8ef1f4a1c74a54da8ee636a))

## [5.0.0-beta.4](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.3...v5.0.0-beta.4) (2022-09-16)


### Features

* **docker:** allow PHP version to change ([a801b0a](https://gitlab.com/dropfort/dropfort_build/commit/a801b0a3cf76937dd4d32aa6e9978cfeafb4a8a3))


### Bug Fixes

* **codeceptjs:** cast port and correct url handling for selenium tests ([b4a9274](https://gitlab.com/dropfort/dropfort_build/commit/b4a9274d8a1f50d7f6a2b2089e3eb671994d6397))
* **message:** typo in echo command ([93e3b46](https://gitlab.com/dropfort/dropfort_build/commit/93e3b460ebbe2b5158fd6e1849be5dcf331280e1))


### Build System

* **docker:** install jq by default ([5d8a422](https://gitlab.com/dropfort/dropfort_build/commit/5d8a422a03eb479ed619c03ba454d356e93fc40c))


### Code Refactoring

* **editor:** remove custom spacing for json files ([0fcccb6](https://gitlab.com/dropfort/dropfort_build/commit/0fcccb6434d469643885db60d217bb1574673066))
* **npm:** use npm install in all situations ([c19d003](https://gitlab.com/dropfort/dropfort_build/commit/c19d003b7a5a13f1f3617e041e69c0536b9d7016))
* **tests:** remove references to backstop ([e08e099](https://gitlab.com/dropfort/dropfort_build/commit/e08e0992e0a866e6dd241e17190e9f12662de336))

## [5.0.0-beta.3](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.2...v5.0.0-beta.3) (2022-08-29)


### Bug Fixes

* **scaffold:** add comments to require git keeps ([94bd294](https://gitlab.com/dropfort/dropfort_build/commit/94bd29420b7d55f0435c813a3b8ea4a44ca20ca7))


### Code Refactoring

* **ci:** use npm to trigger php scripts ([0f6a3f0](https://gitlab.com/dropfort/dropfort_build/commit/0f6a3f072eb42958cd9b730080855578d23ece8a))
* **tests:** use npm as a wrapper for all tests ([db378e6](https://gitlab.com/dropfort/dropfort_build/commit/db378e6af4452a74843f684e9cbf739f46adef28))

## [5.0.0-beta.2](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.1...v5.0.0-beta.2) (2022-08-26)


### Features

* **arm:** add support for Apple Silicon ([17ee9d4](https://gitlab.com/dropfort/dropfort_build/commit/17ee9d41b40c6f062deb7f1ddea6493aed2eae51))
* **npm:** add args option ([fae89ed](https://gitlab.com/dropfort/dropfort_build/commit/fae89ed605de1fe13f70f78e29477779808621f5))


### Bug Fixes

* **ci:** remove deprecated migration option ([6ca9c4f](https://gitlab.com/dropfort/dropfort_build/commit/6ca9c4fdf32bde07c3707737f580dbccecec07fe))
* **migration:** remove deprecated options ([bbb86e3](https://gitlab.com/dropfort/dropfort_build/commit/bbb86e3bb1754c1c58aa31081062044f418527e5))
* **scaffold:** add gitkeep to screenshots folder ([a7a8a17](https://gitlab.com/dropfort/dropfort_build/commit/a7a8a1719b695def29fc97aee9422d93ba859e4d))
* **testing:** update scaffold file name ([62cbf68](https://gitlab.com/dropfort/dropfort_build/commit/62cbf689bf02bda0996577d02c62a63123cc7d75))
* **testing:** update scaffold package.json ([cd5776f](https://gitlab.com/dropfort/dropfort_build/commit/cd5776f243bad76f88d15d192d4d8f1c2ff61b62))
* **tests:** update scaffolding ([a84d2ff](https://gitlab.com/dropfort/dropfort_build/commit/a84d2ffc08e227b10594e97aeb077c98cf50f71f))
* **xhgui:** update nginx config ([0588f5e](https://gitlab.com/dropfort/dropfort_build/commit/0588f5ea05532b58b3d0704595f78ff24327c7d9))


### Documentation

* :construction: add environment variables to doc ([da4d36e](https://gitlab.com/dropfort/dropfort_build/commit/da4d36e630ab6088a5991a2db4923cdccead45e6))

## [5.0.0-beta.1](https://gitlab.com/dropfort/dropfort_build/compare/v5.0.0-beta.0...v5.0.0-beta.1) (2022-08-04)


### Bug Fixes

* **docker:** don't copy drupal ([0437977](https://gitlab.com/dropfort/dropfort_build/commit/0437977302af56b567c0457807079e84dd05bf68))

## [5.0.0-beta.0](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.6...v5.0.0-beta.0) (2022-08-03)


### ⚠ BREAKING CHANGES

* **docker:** database is now contained inside a volume, not bound on the host machine
* **docker:** database is now contained inside a volume, not bound on the host machine

### Features

* **docker:** add scaffold entries ([1d0f909](https://gitlab.com/dropfort/dropfort_build/commit/1d0f90947ed4871d728fb8580f1fbbfa5b5a9901))
* **docker:** build container locally ([fb9a0e3](https://gitlab.com/dropfort/dropfort_build/commit/fb9a0e306e4a989c921a4077dd9e311a864f6e6a))
* **drupal:** use env vars for config split ([3092f07](https://gitlab.com/dropfort/dropfort_build/commit/3092f074c35cab5b167f2c702def76240dfdb865))
* **memcache:** add support for memcache container ([3ba6414](https://gitlab.com/dropfort/dropfort_build/commit/3ba6414e0472dd9674d77462ae0874de199dd863))
* **migration:** add migration source db ([1e500fc](https://gitlab.com/dropfort/dropfort_build/commit/1e500fc0a1afdff117b9479cdc1d0b494a355367))
* **migration:** add stock migrate db connection ([533ac94](https://gitlab.com/dropfort/dropfort_build/commit/533ac948989fd8d7f4fe704c88b290b46d6bd01f))
* **variables:** allow different browsers ([1d64215](https://gitlab.com/dropfort/dropfort_build/commit/1d64215898704b5ad4480d69ce3725f2dfda991e))
* **vscode:** add extension ([8d189c5](https://gitlab.com/dropfort/dropfort_build/commit/8d189c52faa1667c99bbfd3cef45b190e5e33ffe))
* **vscode:** add todo tree ([b150555](https://gitlab.com/dropfort/dropfort_build/commit/b150555e3634231811612415a9a53971556edd5f))


### Bug Fixes

* **composer:** allow php8.0+ ([470ea46](https://gitlab.com/dropfort/dropfort_build/commit/470ea46d6f956b6ccd6c16ef3e12267b5dc9899d))
* **composer:** scaffold zookeeper dockerfile ([43fb77c](https://gitlab.com/dropfort/dropfort_build/commit/43fb77c71bbb3252aacacf3def77781cb9f94b07))
* **dev:** update proxy for browsersync ([832b947](https://gitlab.com/dropfort/dropfort_build/commit/832b94749a03932b6724e3cd34b8a374594c2fe8))
* **docker:** add migration volume ([f70f6d8](https://gitlab.com/dropfort/dropfort_build/commit/f70f6d82ca8b512f0965a325890030b4d7511b48))
* **docker:** add missing scripts entrypoint ([e4420cc](https://gitlab.com/dropfort/dropfort_build/commit/e4420cc61d0e189f8bfd4611cbed3d13e4a700b4))
* **docker:** remove bind mount for database ([20782b1](https://gitlab.com/dropfort/dropfort_build/commit/20782b104312276ac198130371d0be44e523a396))
* **docker:** remove bind mount for database ([184bbdb](https://gitlab.com/dropfort/dropfort_build/commit/184bbdba642b5e4e1b47b80109d33532a032c138))
* **env:** allow config_split variable to be read and set correctly ([085a725](https://gitlab.com/dropfort/dropfort_build/commit/085a725b52f486ea31b13aa1cea127352707fc09))
* **env:** use getenv ([348d2c5](https://gitlab.com/dropfort/dropfort_build/commit/348d2c5cc715c0714db8ebc8254f4a90dc968ab9))
* **npm:** skip downloading assets ([ae9c14f](https://gitlab.com/dropfort/dropfort_build/commit/ae9c14f2f57772dd3b461d88bf7bc998489266c0))
* **scaffold:** add gitlab-ci ([9ffa2ae](https://gitlab.com/dropfort/dropfort_build/commit/9ffa2aecead945dceb3bf3e5bf1580c82c939880))
* **solr:** permissions on linux rootless ([55b10d0](https://gitlab.com/dropfort/dropfort_build/commit/55b10d0775262886b1f34ed48ba64fe64497ff39))


### Documentation

* update documentation for environment variables ([8c4418b](https://gitlab.com/dropfort/dropfort_build/commit/8c4418b87250ac4b95fc3c5423b380d4f34561ca))


### Build System

* **docker:** swap root docker to named volumes ([7413955](https://gitlab.com/dropfort/dropfort_build/commit/74139556117911792923eff345c8cd0f029041c0))
* **docker:** use named volumes for all bound data directories ([404d0df](https://gitlab.com/dropfort/dropfort_build/commit/404d0dfe1b358af73efad716f871048b853a4824))
* implement local docker environment ([cb068d0](https://gitlab.com/dropfort/dropfort_build/commit/cb068d0cfd152e6ba876d658ca2af6f49beb83e0))
* **npm:** add resolution for is-fullwidth-code-point 3.0.0 ([14c1366](https://gitlab.com/dropfort/dropfort_build/commit/14c13669fbcbf8fde49e0512884840bda70ed9f3))


### Code Refactoring

* **settings:** leave  config enabled ([428d803](https://gitlab.com/dropfort/dropfort_build/commit/428d803dfab37c3c851ff0f7871fad4ad57db9ac))
* **task:** get database as template ([6eaf62f](https://gitlab.com/dropfort/dropfort_build/commit/6eaf62fa5cdaef366269218b39e8ef1ea37eb831))
* **templates:** use config dir ([7278ebf](https://gitlab.com/dropfort/dropfort_build/commit/7278ebf11edefe550c82c25b64d3d065cb011bc5))

### [4.0.6](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.5...v4.0.6) (2022-04-07)


### Bug Fixes

* **ci:** ensure automatic updates respect requested composer version ([f91f7e0](https://gitlab.com/dropfort/dropfort_build/commit/f91f7e05cfd063980a463b7f68752a4509532300))
* **scaffold:** add gitignore for exported content from migrate_default_content ([013d623](https://gitlab.com/dropfort/dropfort_build/commit/013d6236f803208d7148ae73f459df40a82449a1))


### Documentation

* add commit and code standard documentation ([47509cc](https://gitlab.com/dropfort/dropfort_build/commit/47509cc292d01cd12019972bf62e97e22e4e92ee))

### [4.0.5](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.4...v4.0.5) (2022-03-25)


### Bug Fixes

* **updates:** do not attempt to update composer/composer ([9e5592a](https://gitlab.com/dropfort/dropfort_build/commit/9e5592a838ce1e4fdad96119c81e254818125894))


### Build System

* **composer:** require core-composer-scaffold ([2bd4061](https://gitlab.com/dropfort/dropfort_build/commit/2bd406147d58645c43a28b431a668ec24ba56a51))

### [4.0.4](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.3...v4.0.4) (2022-03-17)


### Bug Fixes

* **updates:** check for composer updates before general updates ([84ed882](https://gitlab.com/dropfort/dropfort_build/commit/84ed882857537c6bd2a6fcc17c8350b073c3f76e))
* **updates:** only run other security updates if they exist ([b04eaab](https://gitlab.com/dropfort/dropfort_build/commit/b04eaab4adf37a0ef78db5f37776a0548a121bae))


### Performance Improvements

* **updates:** add quiet to all updates ([01a1c76](https://gitlab.com/dropfort/dropfort_build/commit/01a1c764a672598786f859c879833da87adfdc01))

### [4.0.3](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.2...v4.0.3) (2022-03-10)


### Bug Fixes

* **ci:** correct composer general update dependecy matching for commits ([246692c](https://gitlab.com/dropfort/dropfort_build/commit/246692ca84c4747edbed24c9dd4514b69714cb18))
* **prettier:** update deprecated option to bracketSameLine ([f1dc871](https://gitlab.com/dropfort/dropfort_build/commit/f1dc871caec7c9c54cbcb756bf546d58a3edd8ed))
* **vscode:** remove PATH changes from linux ([e69d14b](https://gitlab.com/dropfort/dropfort_build/commit/e69d14bfceb81a2e548e2776f6d9890aad617601))

### [4.0.2](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.1...v4.0.2) (2022-02-22)


### Performance Improvements

* **updates:** remove un-updated dependencies from MR and commit messages ([436cd2f](https://gitlab.com/dropfort/dropfort_build/commit/436cd2feb4c68b8f034e55e76b83606e10e31af0))

### [4.0.1](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0...v4.0.1) (2022-02-09)


### Bug Fixes

* **lint:** remove bin ([739b2ac](https://gitlab.com/dropfort/dropfort_build/commit/739b2acefb4e73aaa574d2e943507ea8640a075a))
* **paths:** remove bin ([f963c3a](https://gitlab.com/dropfort/dropfort_build/commit/f963c3a38f10c88c2ccf879d95cb9c07079f43f8))


### Continuous Integration

* allow pages to run for 4.x ([c032392](https://gitlab.com/dropfort/dropfort_build/commit/c0323927877b7a2e70f9b3a046239140125ad337))
* **build:** include build jobs ([b66c04a](https://gitlab.com/dropfort/dropfort_build/commit/b66c04ac569d877a86256b397ceb8654dfbe46ba))


### Build System

* **docs:** set base to /dropfort_build/ ([a0ca99f](https://gitlab.com/dropfort/dropfort_build/commit/a0ca99fe89bd51649d77e10439a83b5762033501))


### Documentation

* add all pages to sidebar and correct links ([e4f0669](https://gitlab.com/dropfort/dropfort_build/commit/e4f06697ae1a4c841ea157765927ab52fd199c25))
* fix plantuml ([2bbbfe2](https://gitlab.com/dropfort/dropfort_build/commit/2bbbfe2c754052e281af6cf3d9353eb8e515395f))
* implement vuepress for gitlab pages ([1ba0169](https://gitlab.com/dropfort/dropfort_build/commit/1ba0169d7c8485d259bee6e5fa6851544f38b255))
* update documentation for installation and scaffolding ([7df2bc9](https://gitlab.com/dropfort/dropfort_build/commit/7df2bc98464cdaaa8c2e5e8e260d25f4c23f6a0c))

## [4.0.0](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-rc.16...v4.0.0) (2022-01-30)


### Features

* **docker:** allow multiple compose running at the same time ([c632c13](https://gitlab.com/dropfort/dropfort_build/commit/c632c137c622d5fb10e71a8eea987abd46c30484))


### Bug Fixes

* **deploy:** update pantheon deploy ([33bf3dc](https://gitlab.com/dropfort/dropfort_build/commit/33bf3dc46ca98005170cdcc08bb14467a320dfb9))


### Performance Improvements

* **docker:** remove override file from scaffold ([d758d00](https://gitlab.com/dropfort/dropfort_build/commit/d758d00d22ebd56a9c479d4050fd7cbf598199b5))


### Documentation

* **drupal:** correct typo in default settings file ([3ab334e](https://gitlab.com/dropfort/dropfort_build/commit/3ab334ee05d8e71ac2166f1dfd8ef10ec19d4adc))


### Build System

* correct bin path in vscode for php linting ([b83d086](https://gitlab.com/dropfort/dropfort_build/commit/b83d0869e85c47cd4e11f4b97a46126211b5173c))


### Code Refactoring

* **path:** avoid changing PATH in osx ([f53c1d4](https://gitlab.com/dropfort/dropfort_build/commit/f53c1d46d3b5ab8aa870c3307b48dbe7a9712782))

## [4.0.0-rc.16](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-rc.15...v4.0.0-rc.16) (2022-01-26)


### Bug Fixes

* **bash:** bash history actually works ([8b55745](https://gitlab.com/dropfort/dropfort_build/commit/8b5574568f680c5ba54e3ad64348f46325c1cd2b))

## [4.0.0-rc.15](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-rc.14...v4.0.0-rc.15) (2022-01-26)


### Features

* **bash:** persist history ([445ee40](https://gitlab.com/dropfort/dropfort_build/commit/445ee402370baf05b7f2166aa7e25e7b35fe35b9))
* **traefik:** set volume path with env var ([699ca6c](https://gitlab.com/dropfort/dropfort_build/commit/699ca6cbd97c0b1f0bb8f47306d41dec93483c65))

## [4.0.0-rc.14](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-rc.13...v4.0.0-rc.14) (2022-01-25)


### Bug Fixes

* **selenium:** update grid config ([7705670](https://gitlab.com/dropfort/dropfort_build/commit/77056703cf9d66835e587e8b7bcf91405731f92d))
* **vscode:** install renamed xdebug extension ([102bbad](https://gitlab.com/dropfort/dropfort_build/commit/102bbad4c0aaf4ebb9fce4c0914ea670d741e217))


### Code Refactoring

* **devcontainer:** remove override ([ca2088f](https://gitlab.com/dropfort/dropfort_build/commit/ca2088fcdafec5744518c37ba6a8ba596c6617a2))

## [4.0.0-rc.13](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-rc.12...v4.0.0-rc.13) (2022-01-21)


### Features

* **hostname:** use valid host name ([af00048](https://gitlab.com/dropfort/dropfort_build/commit/af00048a9ee776c1673df3919329503a281408a0))
* **vscode:** add file association ([47b86ec](https://gitlab.com/dropfort/dropfort_build/commit/47b86ece0f209e43df47976be2448188dc79b1f6))


### Code Refactoring

* **docker:** set defaults in compose ([c15e027](https://gitlab.com/dropfort/dropfort_build/commit/c15e027a9b3821f9767ff72f4fea6f41e9d79ba9))
* **json:** sort alpha ([70aa7db](https://gitlab.com/dropfort/dropfort_build/commit/70aa7db29c26b348cf4fddd2a66662dcaaca8e68))

## [4.0.0-rc.12](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-rc.11...v4.0.0-rc.12) (2022-01-17)


### Bug Fixes

* **pantheon:** deploy log ([7e2d779](https://gitlab.com/dropfort/dropfort_build/commit/7e2d779e9b504a18cf2fd6cfa19be73d2a0ba545))

## [4.0.0-rc.11](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-rc.10...v4.0.0-rc.11) (2022-01-17)


### Bug Fixes

* **patheon:** add sleep to code commit ([ac4ee15](https://gitlab.com/dropfort/dropfort_build/commit/ac4ee15d8f55b623e1e7bf10c92b1e54fb843096))

## [4.0.0-rc.10](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-rc.9...v4.0.0-rc.10) (2022-01-13)


### Bug Fixes

* **pantheon:** always upload artifacts on deploy ([2bfd195](https://gitlab.com/dropfort/dropfort_build/commit/2bfd19520947cbf143a597d2f9e6d9b25c0c73f6))


### Code Refactoring

* **deploy:** update pantheon ([f3ebe9f](https://gitlab.com/dropfort/dropfort_build/commit/f3ebe9f3eb66bb129345d2b40a71a2d47559b047))

## [4.0.0-rc.9](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-rc.8...v4.0.0-rc.9) (2022-01-13)


### Bug Fixes

* **docker:** remove prefix ([539a0e5](https://gitlab.com/dropfort/dropfort_build/commit/539a0e5c6740959abc0ac38518b0abb8c3581bb1))

## [4.0.0-rc.8](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-rc.7...v4.0.0-rc.8) (2022-01-13)


### Bug Fixes

* **docker:** add container name ([02349dd](https://gitlab.com/dropfort/dropfort_build/commit/02349dda626a1b07aba884ff413fc9ebddbb53ac))
* **drupal:** update default.settings.php file ([d4e6306](https://gitlab.com/dropfort/dropfort_build/commit/d4e63069e630d3b0078bffef72650e36e5a5b77b))
* **env:** mark both locations for docker compose settings ([3c7ec46](https://gitlab.com/dropfort/dropfort_build/commit/3c7ec46a1b8dca5921fc0c27c28fb1327f5de03a))
* **patheon:** use latest terminus ([3284960](https://gitlab.com/dropfort/dropfort_build/commit/328496091f379b405c15e6c38252a7c3bc6d503b))
* **typo:** extra space ([519ea0c](https://gitlab.com/dropfort/dropfort_build/commit/519ea0cac6089dc8ebe1aea0a61d9cbc6ec62b0e))
* **vscode:** disable auto accept suggest on return key ([89cfd9a](https://gitlab.com/dropfort/dropfort_build/commit/89cfd9a77dd67189dbf810fb01ada42bd1a3ec5b))

## [4.0.0-rc.7](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-rc.6...v4.0.0-rc.7) (2022-01-11)


### Bug Fixes

* **docker:** avoid volume name collisions ([11a4e26](https://gitlab.com/dropfort/dropfort_build/commit/11a4e26c7f858a7a5ec72545e0465ef847a73666))
* **docker:** update version requirement ([9d607ae](https://gitlab.com/dropfort/dropfort_build/commit/9d607aefbf76fa1aa79ab5ec43f28e226286f74b))

## [4.0.0-rc.6](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-rc.5...v4.0.0-rc.6) (2022-01-07)


### Features

* **docker:** add override by default ([b225513](https://gitlab.com/dropfort/dropfort_build/commit/b2255132b9205e294c86d4f9738c16b70611b9b3))

## [4.0.0-rc.5](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-rc.4...v4.0.0-rc.5) (2022-01-07)


### Features

* **docker:** provision docker config ([6a04827](https://gitlab.com/dropfort/dropfort_build/commit/6a04827cd947ec2697b911659f1bd99ced7e8f50))
* **docs:** expose example env file ([25e4865](https://gitlab.com/dropfort/dropfort_build/commit/25e4865562426a2a37a19e54a2f1a2d9117e01ea))
* **solr:** add dockerfile ([9d06052](https://gitlab.com/dropfort/dropfort_build/commit/9d06052279b03ab48304e0be03f89e03a8f8e8de))


### Bug Fixes

* **ignore:** add new folders ([3504d9c](https://gitlab.com/dropfort/dropfort_build/commit/3504d9c2bb72362e2e75cd96182a74f708c11b39))
* **scaffold:** path to file for xhgui ([efe520f](https://gitlab.com/dropfort/dropfort_build/commit/efe520f37e384169fa865c23356b26de92eddfc4))


### Code Refactoring

* **scaffold:** remove gitkeep ([6370629](https://gitlab.com/dropfort/dropfort_build/commit/6370629027e670821d393fdf1e9f0c583e030f54))
* **scaffold:** remove gitkeep ([d8cfc52](https://gitlab.com/dropfort/dropfort_build/commit/d8cfc5282f0878f9c1412f753e5820502efc3abb))

## [4.0.0-rc.4](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-rc.3...v4.0.0-rc.4) (2022-01-07)


### Features

* **vscode:** add thunderclient ([f532e51](https://gitlab.com/dropfort/dropfort_build/commit/f532e51e1803a71bda83b7e7e2bebd43ad48868c))


### Bug Fixes

* **build:** add skip dev to npm prod builds ([971e378](https://gitlab.com/dropfort/dropfort_build/commit/971e3780cf306f828ba106b26c21c74104b3b7e3))
* **ci:** allow npm audit to fail if some packages can't be updated ([1a2a038](https://gitlab.com/dropfort/dropfort_build/commit/1a2a038bd771edb9f5b5e001241e7140a38d755c))

## [4.0.0-rc.3](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-rc.2...v4.0.0-rc.3) (2021-12-22)


### Code Refactoring

* **composer:** change path order ([7d49241](https://gitlab.com/dropfort/dropfort_build/commit/7d492411da86a10c5519113fba51ac8ac07eaa98))

## [4.0.0-rc.2](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-rc.1...v4.0.0-rc.2) (2021-12-22)


### Bug Fixes

* **composer:** always install ([5b28c06](https://gitlab.com/dropfort/dropfort_build/commit/5b28c0695e8c58d2b46b20bd9f7db8e1ccceb4df))
* **pantheon:** checkout the web/sites ([4f4ba9e](https://gitlab.com/dropfort/dropfort_build/commit/4f4ba9e1083446a099b8479d3f1a26a5bf835644))

## [4.0.0-rc.1](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-rc.0...v4.0.0-rc.1) (2021-12-20)


### Bug Fixes

* **ci:** clean up automatic security and general update jobs ([6fd850b](https://gitlab.com/dropfort/dropfort_build/commit/6fd850b900bd74ca2e318b781c2a69bc8ac33584))

## [4.0.0-rc.0](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-beta.3...v4.0.0-rc.0) (2021-12-20)


### Bug Fixes

* **before_script:** correct PATH export ([0107b59](https://gitlab.com/dropfort/dropfort_build/commit/0107b5901f2832ae35978b9d9bf071ad30297046))
* **cli:** limit version of psych for drush ([b6f204b](https://gitlab.com/dropfort/dropfort_build/commit/b6f204bb5980862ec632b5401145744ae27f2d22))
* **paths:** remove bin/ ([f80fe1e](https://gitlab.com/dropfort/dropfort_build/commit/f80fe1e73a53b179bb1f48641a0226c362c11576))


### Build System

* **npm:** allow prepare script to fail to fix release builds ([07c9cbd](https://gitlab.com/dropfort/dropfort_build/commit/07c9cbd410c20b351c1c3072eea185cd9628c90d))

## [4.0.0-beta.3](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-beta.2...v4.0.0-beta.3) (2021-12-14)


### Features

* **vscode:** add extensions ([e3992a6](https://gitlab.com/dropfort/dropfort_build/commit/e3992a6dc5d28ba6370b40914e4a081255344a88))
* **vscode:** move sidebar to the right ([a4c943a](https://gitlab.com/dropfort/dropfort_build/commit/a4c943af8dc734e1399cfdffd386812a5af2ad25))
* **vscode:** move sidebar to the right ([3ca54e5](https://gitlab.com/dropfort/dropfort_build/commit/3ca54e5dd457c315b319565116eb0825136fcb85))
* **vuejs:** add language handling ([77f27bb](https://gitlab.com/dropfort/dropfort_build/commit/77f27bbd631d64fe3eb6c1ee6894680ba06bdf63))

## [4.0.0-beta.2](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-beta.1...v4.0.0-beta.2) (2021-12-10)


### Features

* **extensions:** add uuid generator ([e58d702](https://gitlab.com/dropfort/dropfort_build/commit/e58d7028332e859099e3db779fa6d339ffa24f82))
* **shell:** add Drupal CLI ([4202a02](https://gitlab.com/dropfort/dropfort_build/commit/4202a0266c019acb7c3e725ea2bd2bcf85b52963))
* **shell:** add drush sql-cli shell ([d3add99](https://gitlab.com/dropfort/dropfort_build/commit/d3add99cb535c8a54fb8b09b68c5aa11936f3369))

## [4.0.0-beta.1](https://gitlab.com/dropfort/dropfort_build/compare/v4.0.0-beta.0...v4.0.0-beta.1) (2021-12-09)


### Bug Fixes

* **scaffold:** update lintstaged path ([3f39b1c](https://gitlab.com/dropfort/dropfort_build/commit/3f39b1c78c4fb737ba2f4dd890b4c5b5c39e20b4))

## [4.0.0-beta.0](https://gitlab.com/dropfort/dropfort_build/compare/v3.6.0...v4.0.0-beta.0) (2021-12-09)


### Features

* **devcontainer:** add config for vscode devcontainers ([0c79d62](https://gitlab.com/dropfort/dropfort_build/commit/0c79d62584f46a1e13dd2b99fdc122fd350db02d))


### Bug Fixes

* **scaffold:** add lint-staged back into the scaffolded package.json file ([cf687b2](https://gitlab.com/dropfort/dropfort_build/commit/cf687b2418542f0d45e68fa53d698b97b284b37f))
* **scaffold:** correct hidden file ([529b83d](https://gitlab.com/dropfort/dropfort_build/commit/529b83d99c486b91cc22d3902fa4187845d6a2ef))


### Continuous Integration

* update template paths to fix invalid yaml ([93b3121](https://gitlab.com/dropfort/dropfort_build/commit/93b312150f3f352d5eea3f06210aa33a5ce28ce4))


### Build System

* **npm:** add lighthouse dependencies ([603f0bc](https://gitlab.com/dropfort/dropfort_build/commit/603f0bcd27d70517697237f51f30bed1c4beb75b))
* update root dependencies and config ([578154a](https://gitlab.com/dropfort/dropfort_build/commit/578154ab63ba09dd616c5559b1185496e6f4d82d))
* update scaffolding dependencies and config ([b186f2b](https://gitlab.com/dropfort/dropfort_build/commit/b186f2b7cae9876308a21520b2c2feade5887951))

## [3.6.0](https://gitlab.com/dropfort/dropfort_build/compare/v3.5.0...v3.6.0) (2021-10-20)


### Features

* **vscode:** :sparkles: add conventional commits extension ([b54d5cc](https://gitlab.com/dropfort/dropfort_build/commit/b54d5cc84ef317ddeaba0ef27e18b3ab898b6dbd))


### Bug Fixes

* update environment detection for d9 ([0503ec8](https://gitlab.com/dropfort/dropfort_build/commit/0503ec80e12fe4211a9d3640ed94afa3405ad927))
* **deploy:** fix pantheon deploy to qa and prod ([29436f1](https://gitlab.com/dropfort/dropfort_build/commit/29436f1754281634255a8c6a2ad217c0a9b03e23))

## [3.5.0](https://gitlab.com/dropfort/dropfort_build/compare/v3.5.0-alpha.2...v3.5.0) (2021-09-22)


### Bug Fixes

* include list of updated packages in MR description ([1229fd7](https://gitlab.com/dropfort/dropfort_build/commit/1229fd732722472be84c183fc05eb1db5db51c61))

## [3.5.0-alpha.2](https://gitlab.com/dropfort/dropfort_build/compare/v3.5.0-alpha.1...v3.5.0-alpha.2) (2021-09-22)


### Bug Fixes

* remove composer version from composer install ([50acfd7](https://gitlab.com/dropfort/dropfort_build/commit/50acfd7fcfc1774a2f90efbf88bf2559e424df93))

## [3.5.0-alpha.1](https://gitlab.com/dropfort/dropfort_build/compare/v3.5.0-alpha.0...v3.5.0-alpha.1) (2021-09-22)


### Bug Fixes

* install prior to update so that reporting shows only updated modules ([d37096c](https://gitlab.com/dropfort/dropfort_build/commit/d37096c44eecd319d5a56a6f7dc7ce41d61d01a0))

## [3.5.0-alpha.0](https://gitlab.com/dropfort/dropfort_build/compare/v3.4.5...v3.5.0-alpha.0) (2021-09-21)


### Features

* add job to create MRs for security and general updates ([24a2f92](https://gitlab.com/dropfort/dropfort_build/commit/24a2f926c6eff924a5eed6334e836a6352e86010))


### Bug Fixes

* **apply-updates:** remove hirak/prestissimo requirement ([f393077](https://gitlab.com/dropfort/dropfort_build/commit/f393077bcc31b42b011ca2cabd98767a81861d4d))
* **pantheon:** install terminus when needed ([092d8f0](https://gitlab.com/dropfort/dropfort_build/commit/092d8f05bae39c5b20f36e8c3aae338f8e7d0d4a))

## [3.5.0-alpha.0](https://gitlab.com/dropfort/dropfort_build/compare/v3.4.5...v3.5.0-alpha.0) (2021-09-03)


### Features

* add job to create MRs for security and general updates ([24a2f92](https://gitlab.com/dropfort/dropfort_build/commit/24a2f926c6eff924a5eed6334e836a6352e86010))

### [3.4.5](https://gitlab.com/dropfort/dropfort_build/compare/v3.4.4...v3.4.5) (2021-09-02)


### Bug Fixes

* **ci:** remove prestissimo ([c5acd27](https://gitlab.com/dropfort/dropfort_build/commit/c5acd27fc013b8511844609db5de4379cb242145))

### [3.4.4](https://gitlab.com/dropfort/dropfort_build/compare/v3.4.3...v3.4.4) (2021-07-08)


### Bug Fixes

* **ci:** fix only for pantheon uri test ([4524273](https://gitlab.com/dropfort/dropfort_build/commit/4524273fd343a26ee5e604b8d5af428774aa49dc))

### [3.4.3](https://gitlab.com/dropfort/dropfort_build/compare/v3.4.2...v3.4.3) (2021-07-01)


### Bug Fixes

* **ci:** typo ([69a14d5](https://gitlab.com/dropfort/dropfort_build/commit/69a14d5abc5e8b72c5f562c9f364607f30b302ea))
* **tests:** add symfony bridge ([e28984a](https://gitlab.com/dropfort/dropfort_build/commit/e28984a75eb4f5d932e175a75020bb588a7f9073))

### [3.4.2](https://gitlab.com/dropfort/dropfort_build/compare/v3.4.1...v3.4.2) (2021-06-30)


### Bug Fixes

* **scaffold:** add pantheon uri check to scaffold ([0833144](https://gitlab.com/dropfort/dropfort_build/commit/0833144c74972db5b584c81a1a7ee133b913ab7d))
* **scaffold:** add pantheon uri template to profile ([202dea7](https://gitlab.com/dropfort/dropfort_build/commit/202dea77c2d32bc9e8dbfee8acf3209664cd2275))
* **test:** add core dev ([f43feeb](https://gitlab.com/dropfort/dropfort_build/commit/f43feebb54f37df6616fad6b18e2eab802fa0389))

### [3.4.1](https://gitlab.com/dropfort/dropfort_build/compare/v3.4.0...v3.4.1) (2021-06-25)


### Bug Fixes

* **ci:** fix jobs that failed due to missing dependencies and incorrect drush ([3b1f9b4](https://gitlab.com/dropfort/dropfort_build/commit/3b1f9b412bbf09b3f12499b74aa2eecbf8d5253e))
* **ci:** run composer install during code test ([f5cc6f1](https://gitlab.com/dropfort/dropfort_build/commit/f5cc6f1b280e743910e56db5524ae5781ddf645b))
* **codetest:** run composer install during code test ([70c1c17](https://gitlab.com/dropfort/dropfort_build/commit/70c1c17fdc7b97ca2058cab732afbbe461c11921))

## [3.4.0](https://gitlab.com/dropfort/dropfort_build/compare/v3.3.3...v3.4.0) (2021-06-23)


### Features

* **ci:** build before code test ([96e8bce](https://gitlab.com/dropfort/dropfort_build/commit/96e8bce92b003eb2b08acd297eb82791592ca2d2))
* **task:** get config task ([ab71db4](https://gitlab.com/dropfort/dropfort_build/commit/ab71db448b1ac02c77214579aaade254ac17cc4b))


### Bug Fixes

* **vscode:** update docker config ([4099465](https://gitlab.com/dropfort/dropfort_build/commit/4099465df585ddd54b5e8a2654f22e30513cb7ec))

### [2.12.3](https://gitlab.com/dropfort/dropfort_build/compare/v2.12.2...v2.12.3) (2021-06-03)


### Bug Fixes

* **hooks:** add a check for CI before running any git hook ([bd47ef8](https://gitlab.com/dropfort/dropfort_build/commit/bd47ef8378b0c9d3671bfdb143d569ae1edff863))

### [2.12.2](https://gitlab.com/dropfort/dropfort_build/compare/v3.3.2...v2.12.2) (2021-05-07)


### Bug Fixes

* **hooks:** add execute to husky hooks ([2fd5601](https://gitlab.com/dropfort/dropfort_build/commit/2fd560151e81396194d381d89ca952051941bcd2))
* **npm:** update dependencies ([0466df0](https://gitlab.com/dropfort/dropfort_build/commit/0466df0d9b58c78c2f3800e97c6a7da4a1aab8bc))

### [2.12.1](https://gitlab.com/dropfort/dropfort_build/compare/v3.3.1...v2.12.1) (2021-05-06)


### Bug Fixes

* **hooks:** remove hook files from scaffold ([d8a02d6](https://gitlab.com/dropfort/dropfort_build/commit/d8a02d6527d487643ce284890aef2fceb06ce871))

## [2.12.0](https://gitlab.com/dropfort/dropfort_build/compare/v3.3.0...v2.12.0) (2021-05-06)


### Features

* **lint:** use lint staged and husky 6.0.0 ([72c4c51](https://gitlab.com/dropfort/dropfort_build/commit/72c4c516cafd976b590429544dc34308e6abb166))


### Bug Fixes

* **composer:** update composer version ([2c1bdf1](https://gitlab.com/dropfort/dropfort_build/commit/2c1bdf13afeead3233e60bdd5cbd46d66eb04462))

## [2.11.0](https://gitlab.com/dropfort/dropfort_build/compare/v3.2.1...v2.11.0) (2021-04-15)


### Features

* **task:** get config task ([eabb3d2](https://gitlab.com/dropfort/dropfort_build/commit/eabb3d292b22097424d8b9caab9c33b23d6d7489))


### Bug Fixes

* **linkcheck:** change timeout ([f786d6f](https://gitlab.com/dropfort/dropfort_build/commit/f786d6fa2a0b311914a21de39226f5a9647f5391))

## [2.10.0](https://gitlab.com/dropfort/dropfort_build/compare/v3.2.0-alpha.0...v2.10.0) (2021-03-29)


### Bug Fixes

* **ci:** remove unnecessary copy of accessibility report to dev server ([d969672](https://gitlab.com/dropfort/dropfort_build/commit/d969672ac8b5688db88605d309c39f5062f42d4e))
* **composer:** set unlimited memory ([e9720d1](https://gitlab.com/dropfort/dropfort_build/commit/e9720d148c15e96048dfd63f5b4d9853c5b2f93e))
* **linkchecker:** fix format for artifacts ([8a8af8a](https://gitlab.com/dropfort/dropfort_build/commit/8a8af8ab8482f1f41259c89e4e5cd2902694bf2b))
* **path:** use variable resolver ([005c1b9](https://gitlab.com/dropfort/dropfort_build/commit/005c1b9fc3a8151bb279575edd47e1dd259b4a78))
* **vscode:** set bracket styles ([c07b567](https://gitlab.com/dropfort/dropfort_build/commit/c07b567ba726a97516f3cd06f65695c2b96ca756))

## [2.10.0-alpha.0](https://gitlab.com/dropfort/dropfort_build/compare/v2.9.2...v2.10.0-alpha.0) (2021-03-05)


### Features

* **ci:** reconfigure jobs to run concurrently ([7424613](https://gitlab.com/dropfort/dropfort_build/commit/7424613b85f52dfde0419109ccb6afe6e4c075c1))
* **task:** add task to get dev db as build artifact ([83fe77e](https://gitlab.com/dropfort/dropfort_build/commit/83fe77edb4f8f5a1c97ecb01e33fc626d12aa62c))


### Bug Fixes

* **2.x:** merge latest changes from master ([71bee5e](https://gitlab.com/dropfort/dropfort_build/commit/71bee5e2ebdd0cd05b1db7926429988231b20088))

### [2.9.2](https://gitlab.com/dropfort/dropfort_build/compare/v3.1.1...v2.9.2) (2021-03-02)


### Bug Fixes

* **bug:** update 2.x branch with v2.8.2 changes ([9f7a191](https://gitlab.com/dropfort/dropfort_build/commit/9f7a19131cbc043f27ea5570b20053c541d1abbc))
* **vscode:** set bin for phpcs ([eb6ac85](https://gitlab.com/dropfort/dropfort_build/commit/eb6ac859d1bbb82c6f560499741cf3b761a6a81f))

### [2.9.1](https://gitlab.com/dropfort/dropfort_build/compare/v2.9.0...v2.9.1) (2021-02-24)


### Bug Fixes

* **vscode:** update paths to bin ([b443522](https://gitlab.com/dropfort/dropfort_build/commit/b44352295e3e9c68937e08486aa94c516598ccd0))

## [2.9.0](https://gitlab.com/dropfort/dropfort_build/compare/v3.1.0...v2.9.0) (2021-02-23)


### Features

* **xdebug:** allow v2 or v3 ([55c06e2](https://gitlab.com/dropfort/dropfort_build/commit/55c06e240f041f99a8f3a65a90e84db10d43e375))


### Bug Fixes

* **ci:** add 'main' branch detection ([f2205e3](https://gitlab.com/dropfort/dropfort_build/commit/f2205e3fd5ac768bfb3fd7ff1918660ec9a4e3b1))

### [2.8.1](https://gitlab.com/dropfort/dropfort_build/compare/v3.0.0...v2.8.1) (2021-01-11)


### Bug Fixes

* **ci:** add pantheon file to scaffold ([8e322b2](https://gitlab.com/dropfort/dropfort_build/commit/8e322b2762e8339b29306a116936c5485dc5652a))

### [3.3.3](https://gitlab.com/dropfort/dropfort_build/compare/v3.3.2...v3.3.3) (2021-06-03)


### Bug Fixes

* **hooks:** add a check for CI before running any git hook ([4b8f53e](https://gitlab.com/dropfort/dropfort_build/commit/4b8f53e6ddcab4e2a7f82b0cbe6df023124d1974))

### [3.3.2](https://gitlab.com/dropfort/dropfort_build/compare/v3.3.1...v3.3.2) (2021-05-07)


### Bug Fixes

* **hooks:** add execute to husky hooks ([437bd20](https://gitlab.com/dropfort/dropfort_build/commit/437bd202043b41c2c5393a7a40107c2f010e6189))
* **npm:** update dependencies ([1371565](https://gitlab.com/dropfort/dropfort_build/commit/1371565c7d05fe9fbdddb35db249a6288ce3ca32))

### [3.3.1](https://gitlab.com/dropfort/dropfort_build/compare/v3.3.0...v3.3.1) (2021-05-06)


### Bug Fixes

* **scaffold:** remove hooks files ([cb562c6](https://gitlab.com/dropfort/dropfort_build/commit/cb562c69a670df5e1a4d9688759c816295a761d7))

## [3.3.0](https://gitlab.com/dropfort/dropfort_build/compare/v3.2.1...v3.3.0) (2021-05-06)


### Features

* **lint:** use lint staged and husky 6.0.0 ([517e256](https://gitlab.com/dropfort/dropfort_build/commit/517e2565f07e3f7c21421c8c2ca228eb30657b3b))

### [3.2.1](https://gitlab.com/dropfort/dropfort_build/compare/v3.2.0...v3.2.1) (2021-04-15)


### Bug Fixes

* **cleanup:** require build artifact for cleanup ([aba7c72](https://gitlab.com/dropfort/dropfort_build/commit/aba7c7234d7ff8284d5e45c788265430f154b29b))
* **linkcheck:** extend timeout ([38cb707](https://gitlab.com/dropfort/dropfort_build/commit/38cb7079997e551185cadbfc70b0a9e3a1d5abb4))


### Code Refactoring

* **composer:** change method of version install ([39cb78f](https://gitlab.com/dropfort/dropfort_build/commit/39cb78f5e500e759e063ee55be8ca788f5fe059a))

## [3.2.0](https://gitlab.com/dropfort/dropfort_build/compare/v3.2.0-alpha.0...v3.2.0) (2021-03-29)


### Bug Fixes

* **ci:** remove unnecessary copy of artifacts to dev server ([4c85216](https://gitlab.com/dropfort/dropfort_build/commit/4c8521617fe673d8a0f531c34522d69d78b50bec))
* **composer:** set unlimited memory ([5192f27](https://gitlab.com/dropfort/dropfort_build/commit/5192f2763878ee776f8b60bd3a2378cc7acae863))
* **eslint:** ignore backstop data folder ([181d7e7](https://gitlab.com/dropfort/dropfort_build/commit/181d7e7750a81bdbc07c8d4c04bbfe47cbd281bd))
* **linkchecker:** fix format for artifacts ([e20c279](https://gitlab.com/dropfort/dropfort_build/commit/e20c2795df9bfeb73246bed0aaf0162140d45cea))
* **path:** use variable resolver ([02ec02d](https://gitlab.com/dropfort/dropfort_build/commit/02ec02d9f4ede500c82ce2687e9f4044c8206167))
* **vscode:** set bracket styles ([f4f8f92](https://gitlab.com/dropfort/dropfort_build/commit/f4f8f9282068ed1861e8d7a217ad9f27b577c749))
* add recent updates from master branch ([ae4570c](https://gitlab.com/dropfort/dropfort_build/commit/ae4570c4869aeb655acf05cbfe645b095f083a6f))

## [3.2.0-alpha.0](https://gitlab.com/dropfort/dropfort_build/compare/v3.1.1...v3.2.0-alpha.0) (2021-03-05)


### Features

* **ci:** add task to get dev database ([b56c308](https://gitlab.com/dropfort/dropfort_build/commit/b56c308b4b13fcf1216cb4806087b9d694095dd8))


### Bug Fixes

* **vscode:** set bin for phpcs ([5be15c3](https://gitlab.com/dropfort/dropfort_build/commit/5be15c313ab526fadda2673cd9113ae0bc342ab2))

### [3.1.1](https://gitlab.com/dropfort/dropfort_build/compare/v3.1.0...v3.1.1) (2021-02-24)


### Bug Fixes

* **vscode:** update paths to bin ([d205112](https://gitlab.com/dropfort/dropfort_build/commit/d2051129359de0513a8c20eb7acfbe8955a4e836))

## [3.1.0](https://gitlab.com/dropfort/dropfort_build/compare/v3.0.3...v3.1.0) (2021-02-23)


### Features

* **xdebug:** allow v2 or v3 ([9379fd7](https://gitlab.com/dropfort/dropfort_build/commit/9379fd731614e70e19125376b256a56705130200))

### [3.0.3](https://gitlab.com/dropfort/dropfort_build/compare/v3.0.2...v3.0.3) (2021-02-12)


### Bug Fixes

* **ci:** add 'main' branch detection ([9903842](https://gitlab.com/dropfort/dropfort_build/commit/990384274cb04eb48079bfecf8628a93915eb6f3))


### Code Refactoring

* **vue:** update templates ([e8cb3e4](https://gitlab.com/dropfort/dropfort_build/commit/e8cb3e4247f4f2aedf883d08896015cce96eb4d6))

### [3.0.2](https://gitlab.com/dropfort/dropfort_build/compare/v3.0.1...v3.0.2) (2021-01-13)


### Bug Fixes

* **composer:** update minimum dependency version ([dbb0a35](https://gitlab.com/dropfort/dropfort_build/commit/dbb0a35922750ebb5c640f237161caf453fb40d2))
* **composer:** update minimum dependency version ([63be8f2](https://gitlab.com/dropfort/dropfort_build/commit/63be8f261a28704f53da80a5297c90cc3ad64fdb))

### [3.0.1](https://gitlab.com/dropfort/dropfort_build/compare/v3.0.0...v3.0.1) (2021-01-11)


### Bug Fixes

* **ci:** add pantheon file to scaffold ([d5ccdde](https://gitlab.com/dropfort/dropfort_build/commit/d5ccdde9d315e17250a745718a711db2d1be1d25))

## [3.0.0](https://gitlab.com/dropfort/dropfort_build/compare/v2.8.0...v3.0.0) (2021-01-11)


### ⚠ BREAKING CHANGES

* **dependencies:** Updating composer requirement to v2

### Code Refactoring

* **ci:** remove global require ([0d06951](https://gitlab.com/dropfort/dropfort_build/commit/0d06951599284e24221c64c37ea5023b0aaa58a3))
* **dependencies:** require composer 2 ([f517054](https://gitlab.com/dropfort/dropfort_build/commit/f5170546468df46eab8763644426d78224ef3231))

## [2.8.0](https://gitlab.com/dropfort/dropfort_build/compare/v2.7.2...v2.8.0) (2021-01-08)


### Features

* **deploy:** add pantheon deploy template ([38c299f](https://gitlab.com/dropfort/dropfort_build/commit/38c299f3177c73c86d7eea48bdf217d2ef313336))


### Bug Fixes

* **ci:** fix initial deploy for feat envs ([6acd0a2](https://gitlab.com/dropfort/dropfort_build/commit/6acd0a2d2f0407eb3d8afae11d218a806934f087))

### [2.7.2](https://gitlab.com/dropfort/dropfort_build/compare/v2.7.1...v2.7.2) (2020-12-31)


### Bug Fixes

* **package:** skip before / after ([0188d13](https://gitlab.com/dropfort/dropfort_build/commit/0188d13bbca1bc349767f1e07b513fc5e593a899))

### [2.7.1](https://gitlab.com/dropfort/dropfort_build/compare/v2.7.0...v2.7.1) (2020-12-23)


### Bug Fixes

* **ci:** update stage for package ([b2c2d56](https://gitlab.com/dropfort/dropfort_build/commit/b2c2d5664c08dcba85b15bc081445579333faf8d))

## [2.7.0](https://gitlab.com/dropfort/dropfort_build/compare/v2.6.4...v2.7.0) (2020-12-23)


### Features

* **backcopy:** sanitize after sql-sync ([41a6c48](https://gitlab.com/dropfort/dropfort_build/commit/41a6c483fe568c787d1524852b17c8dd6fca420b))
* **ci:** publish composer packages ([1ced67b](https://gitlab.com/dropfort/dropfort_build/commit/1ced67be24dbc2ffacf46d9ab1698c7022775d35))

### [2.6.4](https://gitlab.com/dropfort/dropfort_build/compare/v2.6.3...v2.6.4) (2020-12-14)


### Bug Fixes

* **ci:** add --ssh-options to URI validation jobs ([536170c](https://gitlab.com/dropfort/dropfort_build/commit/536170ccd1872d7e603abf56ff780e2c6f7391fa)), closes [dropfort/dropfort_build#8](https://gitlab.com/dropfort/dropfort_build/issues/8)
* **ci:** add except cases from template to validate css dev jobs ([ac7578d](https://gitlab.com/dropfort/dropfort_build/commit/ac7578d2f8edf54176f87f0c0ec205caeed29f2e)), closes [dropfort/dropfort_build#7](https://gitlab.com/dropfort/dropfort_build/issues/7)
* **composer:** update base version ([28706c4](https://gitlab.com/dropfort/dropfort_build/commit/28706c4a3527d025ba7bc5ebba934cca570062a3))
* **debug:** allow breakpoints in any file ([2e2c581](https://gitlab.com/dropfort/dropfort_build/commit/2e2c58179734e357a2e1ecb9d2ca50edc20d7983))


### Code Refactoring

* **ci:** remove upload to /var/www/html/dev ([b374ae8](https://gitlab.com/dropfort/dropfort_build/commit/b374ae860255aab5e89e1e95c72d693691998fd8))

### [2.6.3](https://gitlab.com/dropfort/dropfort_build/compare/v2.6.2...v2.6.3) (2020-11-02)


### Bug Fixes

* **deploy:** run status after cr ([1a278f7](https://gitlab.com/dropfort/dropfort_build/commit/1a278f7f95ee522d7b80be0601e6d5e1818e24c1))
* **env:** append instead of crush ([4132ebe](https://gitlab.com/dropfort/dropfort_build/commit/4132ebe59b6d43848cd0822a557896976d915576))
* **rsync:** don't copy .env file ([a07c283](https://gitlab.com/dropfort/dropfort_build/commit/a07c2835d6cc1ece1d11de7c0574adc5ee59319e))
* **xdebug:** remove data limit ([a340f5e](https://gitlab.com/dropfort/dropfort_build/commit/a340f5e89f16f81ab6ec46cf0e5ffdedcc1518e9))

### [2.6.2](https://gitlab.com/dropfort/dropfort_build/compare/v2.6.1...v2.6.2) (2020-10-26)


### Bug Fixes

* **ci:** add run scripts ([d6157e1](https://gitlab.com/dropfort/dropfort_build/commit/d6157e1ff82550068833a3bb96b520e59632abbd))


### Code Refactoring

* **templates:** remove duplicate file ([2ddc749](https://gitlab.com/dropfort/dropfort_build/commit/2ddc749f1abe8df85e54f89f7fb9f25131ecd17b))

### [2.6.1](https://gitlab.com/dropfort/dropfort_build/compare/v2.6.0...v2.6.1) (2020-10-26)

## [2.6.0](https://gitlab.com/dropfort/dropfort_build/compare/v2.5.0...v2.6.0) (2020-10-26)


### Features

* **ci:** allow control over composer version ([2c203e6](https://gitlab.com/dropfort/dropfort_build/commit/2c203e66c73a4ffdbeb59cfa1e5ce32122bd6bad))
* **docker:** add scaffold for vagrant box ([4b4637d](https://gitlab.com/dropfort/dropfort_build/commit/4b4637d424649ca59c04217640bfa49b0cfb108e))
* **docker:** add vagrant scaffold ([6ae7880](https://gitlab.com/dropfort/dropfort_build/commit/6ae788018a4473d043c5fde914f595faec17795b))
* **extensions:** add ms remote extension pack ([0d991b3](https://gitlab.com/dropfort/dropfort_build/commit/0d991b3fb89afd8980ce0ea1b3abb1af2a6ddb34))


### Bug Fixes

* **assets:** update for vagrant ([8cb367e](https://gitlab.com/dropfort/dropfort_build/commit/8cb367e0820a21d9a8d052d7587ffe7da59d35ae))
* **composer:** allow setting desired version ([3b8bb7c](https://gitlab.com/dropfort/dropfort_build/commit/3b8bb7c122eddeaa205cb6ca7e5a94d1a7c6be34))
* **context:** remove port ([5f368f8](https://gitlab.com/dropfort/dropfort_build/commit/5f368f847d2f4c188554e2b98a9ba89e6087c459))


### Code Refactoring

* **docker:** change files scaffolded ([aefaeb6](https://gitlab.com/dropfort/dropfort_build/commit/aefaeb6eac8a8019194a4b85bf9a56c812950e5f))
* **docker:** remove dockercontext ([e91c087](https://gitlab.com/dropfort/dropfort_build/commit/e91c0870c00c951dc1b3fcf4eb23ba4284d84340))

## [2.5.0](https://gitlab.com/dropfort/dropfort_build/compare/v2.4.0...v2.5.0) (2020-09-21)


### Features

* **release:** use gitlab release tools ([e4bd056](https://gitlab.com/dropfort/dropfort_build/commit/e4bd056380e2a286bbeb22b3f0658e95e874d3b5))


### Bug Fixes

* **scaffold:** add entry for release job ([6419b70](https://gitlab.com/dropfort/dropfort_build/commit/6419b70fd0954d624377ee3fd849fb50f2d337ca))
* **validate:** download puppeteer when needed ([14859d6](https://gitlab.com/dropfort/dropfort_build/commit/14859d6903e6468834a539e2de5ee0fc91546774))

## [2.4.0](https://gitlab.com/dropfort/dropfort_build/compare/v2.3.2...v2.4.0) (2020-09-21)


### Features

* **ci:** add default values ([b6a71d9](https://gitlab.com/dropfort/dropfort_build/commit/b6a71d9442398e82004edadf02860433b8f2bfd1))
* **ci:** allow config import to retry once ([79be196](https://gitlab.com/dropfort/dropfort_build/commit/79be196df1a69780e0650db61f4b38bd75c0320d))
* **ci:** update gitlab 13.2 ci changes ([e81329c](https://gitlab.com/dropfort/dropfort_build/commit/e81329cf630e05bf69bf8af2bea42d97f3f4f351))
* **environments:** mark start and prepare jobs ([78a701d](https://gitlab.com/dropfort/dropfort_build/commit/78a701d598b3970b4ea25d2778542e25de23ec97))
* **releases:** exclude .git files from release artifacts ([0fc92eb](https://gitlab.com/dropfort/dropfort_build/commit/0fc92eb79127d2dcb40a2cb8cb38e2161406a7d2))


### Bug Fixes

* **ci:** add envs ([db5e60e](https://gitlab.com/dropfort/dropfort_build/commit/db5e60e8e0e8ef80e48db72c2e815a8b7c2a000d))
* **ci:** fix ci lint ([e56d5c1](https://gitlab.com/dropfort/dropfort_build/commit/e56d5c1814d1ff9b312bb0483e0aaeddfb274bcf))
* **ci:** use standard env names ([4da3d3f](https://gitlab.com/dropfort/dropfort_build/commit/4da3d3f71dcf20719fe062865bc4604e28c9ccf7))
* **docs:** add support section ([e3010ad](https://gitlab.com/dropfort/dropfort_build/commit/e3010adfe4f1f7b3cee8bfb0848472ce91ae464d))
* **env:** add unique name ([827548e](https://gitlab.com/dropfort/dropfort_build/commit/827548eba7895b74241072dc8d16588e0fdfe4bc))
* **env:** feature env names ([02db91c](https://gitlab.com/dropfort/dropfort_build/commit/02db91c301ef37025677ea9227a96a034ba1a602))
* **gitattributes:** fix png ([2786548](https://gitlab.com/dropfort/dropfort_build/commit/2786548e55ffa5e7543a4ea36b23bf7d81f24aa7))
* **vscode:** set url properly ([f46ae7f](https://gitlab.com/dropfort/dropfort_build/commit/f46ae7f59523b02722812a2801bc1ddb0a53ffdb))


### Code Refactoring

* **build:** remove exclude ([2903bf2](https://gitlab.com/dropfort/dropfort_build/commit/2903bf2bbd457cdb01a40fe502c38ecb3c739fa1))

### [2.3.2](https://git.dropfort.com/dropfort/dropfort_drupal_template/compare/v2.3.1...v2.3.2) (2020-09-09)


### Build System

* **eslint:** replace drupal contrib config with plugin ([1251750](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/12517506aba0a91c1d99c2669749775092476a02))
* **npm:** apply security updates ([8649e48](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/8649e48a243cc688f6cb8bae87d898e7b5e00800))

### [2.3.1](https://git.dropfort.com/dropfort/dropfort_drupal_template/compare/v2.3.0...v2.3.1) (2020-08-29)


### Bug Fixes

* **ignore:** add root specificity ([a132665](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/a1326656bfd54d1e561dad7403efea026aaf4509))

## [2.3.0](https://git.dropfort.com/dropfort/dropfort_drupal_template/compare/v2.2.4...v2.3.0) (2020-08-29)


### Features

* **ignore:** add export ignore ([4b60497](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/4b60497e7c02ccba2f14bc1f0577c5edbfef4bc3))
* **initial:** set existing config flag by default ([5e55d9a](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/5e55d9a93a2649e9bce2486d8725d2a3262cfcdf))


### Bug Fixes

* **ci:** add missing job templates ([9bc4952](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/9bc49522252956bb45d73b5835ceef41fc48ab21))
* **ci:** change directory variable ([440b89a](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/440b89a223767ea68790067ed4748df01abb5d11))

### [2.2.4](https://git.dropfort.com/dropfort/dropfort_drupal_template/compare/v2.2.3...v2.2.4) (2020-07-31)


### Bug Fixes

* **ci:** validate uri ([21bff9e](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/21bff9efa8f3f5fbe699062fb9f3b59ffbbbeadd))
* **security:** update package lock ([3606687](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/3606687d596fb59beb0df72104aa0775cc2bb6e0))

### [2.2.3](https://git.dropfort.com/dropfort/dropfort_drupal_template/compare/v2.2.2...v2.2.3) (2020-07-24)


### Bug Fixes

* **gulpfile:** use proxy variable instead of string ([8dc7bb7](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/8dc7bb78d513ffc486ef5f310c7d780e27913712))


### Code Refactoring

* **standard-version:** move postbump script into config file ([1dbf562](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/1dbf5621a9ecb3614a8483e426adaf594dd0c0c3))


### Build System

* **dev-deps:** clean up dependencies and config for root project ([8ba110e](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/8ba110e14d9a1a67be8f1f16fcde5a1af039afce))
* **prettier:** update prettier config to be more specific ([9f055db](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/9f055db54c732679f2ced673cc0ba6809e125a5b))

### [2.2.2](https://git.dropfort.com/dropfort/dropfort_drupal_template/compare/v2.2.1...v2.2.2) (2020-07-24)


### Bug Fixes

* **solr:** update default values ([69f9558](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/69f95588949565c55011a7c47c9142a117420fe1))

### [2.2.1](https://git.dropfort.com/dropfort/dropfort_drupal_template/compare/v2.2.0...v2.2.1) (2020-07-24)


### Bug Fixes

* **links:** add ssh options ([eb02ed4](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/eb02ed4b41f4d20534264f7e11f45cb59ff19c38))

## [2.2.0](https://git.dropfort.com/dropfort/dropfort_drupal_template/compare/v2.1.2...v2.2.0) (2020-07-24)


### Features

* **comments:** enable suggestions ([a5cca5f](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/a5cca5fb02a9af7ea8eac23239adb93356e9b9b0))
* **release:** add postbump script ([14c9c12](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/14c9c12f2fa8a5dbbf0682bc501a79de335eff49))


### Bug Fixes

* **package:** add postbump script ([d41ee61](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/d41ee61022fdf3004f56f28201ad7ab91cda9685))
* **package:** disable overwrite ([9141fff](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/9141fff725930bf778549bd64bc2b1b879808877))

### [2.1.2](https://git.dropfort.com/dropfort/dropfort_drupal_template/compare/v2.1.1...v2.1.2) (2020-07-24)


### Bug Fixes

* **release:** remove composer version markers ([a5790a4](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/a5790a40f9292b02c3325243329fc3c503348c8b))

### [2.1.1](https://git.dropfort.com/dropfort/dropfort_drupal_template/compare/v2.1.0...v2.1.1) (2020-07-24)

## [2.1.0](https://git.dropfort.com/dropfort/dropfort_drupal_template/compare/v2.0.0...v2.1.0) (2020-07-24)


### Features

* **vscode:** add vue, json and yaml defaults ([040ad79](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/040ad79d9e89916bfe5d42c798ff5d8ce183d595))


### Bug Fixes

* **vscode:** disable paste formatting ([1ada1cf](https://git.dropfort.com/dropfort/dropfort_drupal_template/commit/1ada1cfdad3b5a5375c60042fa947d139bfad116))

## [2.0.0](https://git.dropfort.com/dropfort/dropfort_drupal_template/compare/v1.3.0...v2.0.0) (2020-07-22)


### ⚠ BREAKING CHANGES

* **ci:** Sold cloud template no longer the default search index template

### Features

* **ci:** add basic search index template ([384c295](https://gitlab.com:dropfort/dropfort_build/commit/384c2959992150452e5c0ee814e084aadc1447a4))
* **rsync:** exclude patches directory ([bf360b3](https://gitlab.com:dropfort/dropfort_build/commit/bf360b3f831cd2e60035b6b2ad08e77a6849af04))

## [1.3.0](https://gitlab.com:dropfort/dropfort_build/compare/v1.2.0...v1.3.0) (2020-07-22)


### Features

* **env:** add exampel file ([ceb4ebc](https://gitlab.com:dropfort/dropfort_build/commit/ceb4ebce8a94555b5d1bd0ef78b6f208ed664210))
* **tasks:** add task to clean up old feat envs ([594c775](https://gitlab.com:dropfort/dropfort_build/commit/594c7756fd6707cc958300799dbd802235c05705))


### Bug Fixes

* **deploy:** type in drush command ([ab0fb6e](https://gitlab.com:dropfort/dropfort_build/commit/ab0fb6ee3ae657f4e60a2d48c2df322eb155772e))
* **scaffold:** disable hook overwrite ([04048a6](https://gitlab.com:dropfort/dropfort_build/commit/04048a6fd9cbd5af5c78b68755b98f1b142fff9f))
* **vscode:** disable format on paste for PHP ([4a84d33](https://gitlab.com:dropfort/dropfort_build/commit/4a84d33e78d935da7e1336fe4f6aafa5af7a401d))
* **vscode:** update php validation settings ([da7f10a](https://gitlab.com:dropfort/dropfort_build/commit/da7f10a51cfae050714ae369c944a4a22b22d687))


### Code Refactoring

* **docs:** update documentation on all env vars ([69d821d](https://gitlab.com:dropfort/dropfort_build/commit/69d821d8a592f66f6288e8e5959eac8b7c1129dd))

## [1.2.0](https://gitlab.com:dropfort/dropfort_build/compare/v1.1.6...v1.2.0) (2020-07-15)


### Features

* **validate:** add broken link checker ([6120f42](https://gitlab.com:dropfort/dropfort_build/commit/6120f42ea57173568c372354ff17bb634daf1726))


### Bug Fixes

* **ci:** add missing jobs to build_and_deploy pipeline ([1e387b4](https://gitlab.com:dropfort/dropfort_build/commit/1e387b425e0c0965afd9b3d833d1c67a27fb0c19))

### [1.1.6](https://gitlab.com:dropfort/dropfort_build/compare/v1.1.5...v1.1.6) (2020-07-14)


### Bug Fixes

* **browsersync:** update paths ([50056e3](https://gitlab.com:dropfort/dropfort_build/commit/50056e3f790a7870fb4b0eb5170c9c3e31b682e4))

### [1.1.5](https://gitlab.com:dropfort/dropfort_build/compare/v1.1.4...v1.1.5) (2020-07-14)


### Bug Fixes

* **ci:** use proper env vars ([766df3b](https://gitlab.com:dropfort/dropfort_build/commit/766df3b0d06434fec39217a3c140361c19e7461c))

### [1.1.4](https://gitlab.com:dropfort/dropfort_build/compare/v1.1.3...v1.1.4) (2020-07-13)


### Bug Fixes

* **env:** add env name ([f9f5294](https://gitlab.com:dropfort/dropfort_build/commit/f9f5294a00455b8a12915aaa4052042d41d61847))

### [1.1.3](https://gitlab.com:dropfort/dropfort_build/compare/v1.1.2...v1.1.3) (2020-07-13)


### Bug Fixes

* **standard-version:** avoid 8.0.1 release ([3c9ea2e](https://gitlab.com:dropfort/dropfort_build/commit/3c9ea2ed2bf41d185744276542b4ecc695a10d7b))

### [1.1.2](https://gitlab.com:dropfort/dropfort_build/compare/v1.1.1...v1.1.2) (2020-07-13)


### Bug Fixes

* **dependencies:** allow any version of devel ([b557b31](https://gitlab.com:dropfort/dropfort_build/commit/b557b31e4573f4624f2518a91c8d4d7c035f8413))

### [1.1.1](https://gitlab.com:dropfort/dropfort_build/compare/v1.1.0...v1.1.1) (2020-07-10)


### Bug Fixes

* **build:** add build release config ([c9a924b](https://gitlab.com:dropfort/dropfort_build/commit/c9a924b3337b42de1004e5c50ffc3ead7a403433))

## 1.1.0 (2020-07-10)


### Features

* **ci:** merge projects ([64e5572](https://gitlab.com:dropfort/dropfort_build/commit/64e557287b8eb476695d40edf64e85b940b0d821))
* **codeception:** upgrade to 4.x ([935f494](https://gitlab.com:dropfort/dropfort_build/commit/935f494d173028b2300891b47a0084bf8a8ab3bc))
* **docs:** add basic instructions ([d78b31d](https://gitlab.com:dropfort/dropfort_build/commit/d78b31d539126ac76449d6a3596a3586a18bd8dd))
* **docs:** add info about managed files ([0db7641](https://gitlab.com:dropfort/dropfort_build/commit/0db764162d1b9e585fa4e49aa44b0761f53de2e8))
* **drupal:** add drupal_check ([03c3b67](https://gitlab.com:dropfort/dropfort_build/commit/03c3b6736921b9dc4caf0444e09fe78dd85fe050))
* **hoa:** add console ([bdf50fe](https://gitlab.com:dropfort/dropfort_build/commit/bdf50fe6d501e2fea4e8fcbce1889fa27734fe4c))
* **hooks:** add commit check hooks ([a370166](https://gitlab.com:dropfort/dropfort_build/commit/a370166fdbd19a475361a7a7ceef6867ba4e6dcd))
* **scaffold:** add scaffold files ([ba0d4dd](https://gitlab.com:dropfort/dropfort_build/commit/ba0d4ddaf16f4a1f1d36ec78d5846858fd5a74d1))


### Bug Fixes

* **env:** update path ([47a3c73](https://gitlab.com:dropfort/dropfort_build/commit/47a3c734080b3d0933c17d14377fc65b891527f3))
* **packagist:** update tags ([fd8ca09](https://gitlab.com:dropfort/dropfort_build/commit/fd8ca098f0b3cc83ebb46731c7d543495428275c))


### Code Refactoring

* **build:** remove devel_php  ([82deb41](https://gitlab.com:dropfort/dropfort_build/commit/82deb410e3b9cf1ae6e7b761c07649d3dc85aeaa))
* **codeception:** use different base project ([287ec15](https://gitlab.com:dropfort/dropfort_build/commit/287ec15adbadcbb87b3dfbfae66915ca7ca77f83))


### Build System

* **composer:** add drupal/devel_php ([57e879a](https://gitlab.com:dropfort/dropfort_build/commit/57e879acf8901c058c218042537cd4eb2d2d7713))
