## Description

The dropfort build project scaffolds several files to improve your development expierance.
This project contains a [devcontainer](https://code.visualstudio.com/docs/remote/containers) which you can open in vscode.
Since this project uses multiple containers docker-compose is used to make development more simple.
You can see the definition for the containers [here](https://gitlab.com/dropfort/dropfort_build/-/tree/5.x/assets/development/docker/docker-compose.yml).
In order for the project to work correctly there are some required environment variables. There is an example [env](https://gitlab.com/dropfort/dropfort_build/-/tree/5.x/assets/development/docker/env.example) file provided to get you started.

::: warning
The value for `COMPOSE_PROJECT_NAME` in your **docker/.env** file must be the same as in the project root **.env** file.
:::

## Config

### Enabling features
The default docker compose file will enable the following services by default in all instance.

Default Services:
- Apache/PHP Webserver (default container)
- MariaDB database
- Traefik reverse proxy
- MailHog email inspection tool.

Additional Services:
- Migration Source database
- Solr Cloud
- Memcache
- Selenium Hub with Browsers
- RequestBaskets HTTP Request capture tool
- XHGui for enhanced performance profiling (beta)


You can enable additional services by adding a comma delimited list to the **COMPOSE_PROFILES** environment variable in `docker/.env`

E.g.
```ini
COMPOSE_PROFILES=solr,memcache
```

Will enable all the default services but also turn on a Memcache server and a Solr Cloud instance.

You can find the list of available profiles by checking the **profiles** keywords in the `docker/docker-compose.yml` file in your project.

Additional options in your `docker/.env` file include controlling the version of PHP, NPM/NodeJS, Solr, Mariadb, etc. See the `docker/.env.example` file or documentation page for a list of options available.

### Docker Compose

The [docker-compose.yml](https://gitlab.com/dropfort/dropfort_build/-/tree/5.x/assets/development/docker/docker-compose.yml) takes care of setting up your local environment.

#### Docker Containers

Service Name | Description | Container Image
-- | -- | --
mariadb | <ul><li>Container for database.</li></ul> | [MariaDB](https://hub.docker.com/_/mariadb)
drupal | <ul><li> Container for drupal site.</li></ul> | [WebDevOps PHP Apache Dev](https://hub.docker.com/r/webdevops/php-apache-dev)
mailhog | <ul><li> Container for mailhog. (Managing emails)</li></ul> | [MailHog](https://hub.docker.com/r/mailhog/mailhog/)
traefik | <ul><li> Proxy to connect docker network to local network.</li></ul> | [Traefik](https://hub.docker.com/_/traefik)
memcache | <ul><li> Memcache caching service.</li></ul> | [Memcache](https://hub.docker.com/_/memcached)
solr | <ul><li> Solr service to provide search functionality.</li></ul> | [Solr](https://hub.docker.com/_/solr)
zookeeper | <ul><li> Zookeeper container for managing distiributed services in PHP.</li></ul> | [Zookeeper](https://hub.docker.com/_/zookeeper)
selenium | <ul><li> Selenium container for running browser tests.</li></ul> | [Selenium](https://hub.docker.com/r/selenium/hub)
chrome | <ul><li> Chrome container for selenium.</li></ul> | [Selenium Chrome](https://hub.docker.com/r/selenium/node-chrome)
firefox | <ul><li> Chrome container for selenium.</li></ul> | [Selenium Firefox](https://hub.docker.com/r/selenium/node-firefox)
edge | <ul><li> Edge container for selenium.</li></ul> | [Selenium Edge](https://hub.docker.com/r/selenium/node-edge)
xhgui | <ul><li> Xhgui container for profiling PHP.</li></ul> | [XHGui](https://hub.docker.com/r/xhgui/xhgui)
xhgui_mongodb | <ul><li> MongoDB for saving xhgui data.</li></ul> | [MongoDB (XHGui Storage)](https://hub.docker.com/r/percona/percona-server-mongodb-operator)
requestbaskets | <ul><li> Requestbaskets container for API integration testing.</li></ul> | [RequestBaskets](https://hub.docker.com/r/darklynx/request-baskets)
jmeter | <ul><li>JMeter instance for writing performance tests.</li></ul> | Custom to Dropfort Build
