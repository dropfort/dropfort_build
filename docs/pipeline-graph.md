```plantuml
@startuml
start
:<b>TARGET_PIPELINE|
split
  :build_and_deploy|
  #CCDDFF:<b>Task;
  #CCDDFF:<b>Code Test;
  if (Result?) then (Fail)
    #CCDDFF:<b>Code Test Failure;
    detach
  else (Pass)
    #CCDDFF:<b>Build;
    if (Build type?) then (Dev)
      split
        partition Feat {
          #CCDDFF:<b>Test;
          #CCDDFF:<b>Deploy;
          #CCDDFF:<b>Update data;
          #CCDDFF:<b>Validate;
          #CCDDFF:<b>Cleanup;
          detach
        }
      split again
        partition Dev {
          #CCDDFF:<b>Test;
          #CCDDFF:<b>Deploy;
          #CCDDFF:<b>Update data;
          #CCDDFF:<b>Validate;
          #CCDDFF:<b>Cleanup;
          detach
        }
      endsplit

    else (Release)
      split
        partition Qa {
          #CCDDFF:<b>Test;
          #CCDDFF:<b>Deploy;
          #CCDDFF:<b>Update data;
          #CCDDFF:<b>Validate;
          #CCDDFF:<b>Cleanup;
          detach
      }
      split again
        partition Prod {
          #CCDDFF:<b>Test;
          #CCDDFF:<b>Deploy;
          #CCDDFF:<b>Update data;
          #CCDDFF:<b>Validate;
          #CCDDFF:<b>Cleanup;
          detach
        }
      endsplit
    endif
  endif
  detach
split again
  :validation|
  #CCDDFF:<b>Validate;
  detach
split again
  :outdated|
  #CCDDFF:<b>Outdated;
  detach
split again
  :security_check|
  #CCDDFF:<b>Security;
  detach
endsplit
@enduml
```
