---
title: Getting Started
---

# Dropfort Build Tools

Drupal development tools, CI/CD checks and validation scripts.

Generates development and CI/CD configuration files within your Drupal project to allow it to work with the Dropfort release processes.

All configurations and settings defined in this project are based on the Drupal best practices and Composer / PHP recommended development practices.

## Quick Installation

If you just want the default setup for a new site use the following in your terminal:

```shell
# Set your desired project name.
export DF_PROJECT_NAME=myproject

# Run the rest.
composer create-project drupal/recommended-project $DF_PROJECT_NAME;
cd $DF_PROJECT_NAME;
composer config prefer-stable true
composer config minimum-stability 'dev'
composer config --merge extra.drupal-scaffold.allowed-packages --json '["dropfort/dropfort_build"]'
composer config --json extra.drupal-scaffold.gitignore false
composer require dropfort/dropfort_build:^6.0 --dev
composer scaffold
echo COMPOSE_PROJECT_NAME=$DF_PROJECT_NAME >> container/dev/.env
echo COMPOSE_PROJECT_NAME=$DF_PROJECT_NAME >> .env
code .
```

Then reopen in container when prompted. Once loaded in the VSCode terminual run:

```shell
composer config --merge extra.drupal-scaffold.locations --json '{"config_sync": "config/sync"}'
composer config --merge extra.drupal-scaffold.locations --json '{"config_local": "config/local"}'
composer config --merge extra.drupal-scaffold.locations --json '{"config_dev": "config/dev"}'
composer config --merge extra.drupal-scaffold.locations --json '{"config_qa": "config/qa"}'
composer config --merge extra.drupal-scaffold.locations --json '{"config_prod": "config/prod"}'
composer require drush/drush
drush si minimal -y
drush en config config_split environment_indicator -y
drush cim --partial --source="../vendor/dropfort/dropfort_build/assets/scaffold/config/sync" -y
drush cim --partial --source="../vendor/dropfort/dropfort_build/assets/scaffold/config/local" -y
drush cex -y
```

Commit your files and you're all set.

## Installation

### Create a new project

Create your Drupal project with the Drupal Core Recommended project.

See [Starting a site using Drupal Composer Project Templates](https://www.drupal.org/docs/develop/using-composer/starting-a-site-using-drupal-composer-project-templates) for the most up to date information. But generally speaking you would run:

```shell
composer create-project drupal/recommended-project my-project
```

Depending on the version of Dropfort build you may need to adjust your stability settings:

```json
{
  ...
  "minimum-stability": "dev",
  "prefer-stable": true,
  ...
```

Once your project is ready, from within your `my-project` directory run `composer require dropfort/dropfort_build --dev` within your new project.

### Existing projects

::: warning Note
This project assumes you already have a functional Drupal 9 or 10 site using [drupal/core-composer-scaffold](https://github.com/drupal/core-composer-scaffold). If not, you'll need to add it as a normal dependency.
:::

```shell
composer require drupal/core-composer-scaffold
composer require dropfort/dropfort_build --dev
```

### Configuring your composer.json file
After running `composer require dropfort/dropfort_build --dev` in your existing project Dropfort build requires a few changes to your composer.json file to allow it to create files and reach environment configuration. These changes should have no impact on the actual functionality within your application.

#### Using Composer CLI

```shell
# Allow dropfort build to scaffold files.
composer config --merge extra.drupal-scaffold.allowed-packages --json '["dropfort/dropfort_build"]'
# Tell the scaffold not to exclude scaffolded files from source control
composer config --json extra.drupal-scaffold.gitignore false
# Optionally setup your config split locations.
composer config --merge extra.drupal-scaffold.locations --json '{"config_sync": "config/sync"}'
composer config --merge extra.drupal-scaffold.locations --json '{"config_local": "config/local"}'
composer config --merge extra.drupal-scaffold.locations --json '{"config_dev": "config/dev"}'
composer config --merge extra.drupal-scaffold.locations --json '{"config_qa": "config/qa"}'
composer config --merge extra.drupal-scaffold.locations --json '{"config_prod": "config/prod"}'
# Run the scaffold.
composer scaffold
```

#### Manually update composer.json

Add the following to your composer.json file:

```json{7-9}
// Inside your project's composer.json.
{
  "name": "my/drupal-project",
  ...
  "extra": {
    "drupal-scaffold": {
      "gitignore": false,
      "allowed-packages": [
        "dropfort/dropfort_build"
      ],
      ...
    }
  },
}
```

Then run:

```bash
composer scaffold
```

### Environment Variables

Dropfort build comes with the [Symfony DotEnv variable loader](https://github.com/symfony/dotenv) by default. If you already have an autoload for .env environment value loadingy you don't need to use our load.environment.php file.

If not, add the following to your composer.json file

```json
  ...
  "autoload": {
    "files": [
      "load.environment.php"
    ]
  }
  ...
```

### Setup without local copy of PHP

If your local env does not have PHP or Composer available, you can bootstrap your project using the Composer docker container. Simply replace the `composer` command above with `docker run --rm --interactive --tty --volume $PWD:/app composer` to execute the commands in a container. Once your project is scaffolded, you can then reopen in container and run your composer and PHP commands from there.

### NodeJS and Package.json configuration
Next you can update your new `package.json` file your own project name, URLs, etc. This file contains all scripts for build commands, test tools and commit message tools you'll need to maintain your project.

### Finishing Up
Once you're all done, commit all the changes. You've now succesfully scaffolded your Drupal project and can begin using Dropfort Build tools.

For more information and advanced usage see the [Project Scaffolding](./scaffolding.md) section.

## Starting a development session

Open your new project in VSCode (either with the UI or use `code path/to/project` on the terminal to open the current forlder in VSCode).

### First time setup
If this is your first time launching your project in VSCode it will ask you to install the recommended extensions (which you'll click **install** to allow).

Next you'll have to update your .env file at the root of your project an set a unique `COMPOSE_PROJECT_NAME=myproject` env variable. You can set it to nearly any value but usually something short or the current folder name would suffice.

Finally, you'll have to copy that `COMPOSE_PROJECT_NAME` value into your `container/dev/.env` file as well. Both files need the same value (due to how VSCode and Docker Compose read env files).

### Continuing a development session

Once you've opened your project in VSCode, click on the **Reopen in Container** dialog which appeared (or click on the bottom left green button and select the reopen in container option). This will relaunch your VSCode instance and start up all your docker containers.

If you're launching for the first time or your container images have been updated recently it may take up to a minute for the environment build to finish.

All your enabled services are available in your browser by default at ***.dflocal.net:8000** (e.g. [drupal.dflocal.net:8000](http://drupal.dflocal.net:8000), [mailhog.dflocal.net:8000](mailhog.dflocal.net:8000)). You can check on those services through the Treafik interface port at [drupal.dflocal.net:8080](http://drupal.dflocal.net:8080).
