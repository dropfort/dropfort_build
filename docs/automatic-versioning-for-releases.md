---
title: Automatic versioning
tags:
  - versioning
  - version
  - semver
  - commit standards
  - standard-version
  - release
  - prerelease
---

# Automatic versioning and releases

Creating new releases and version bumps is done automatically through a combination of [standard-version](https://www.npmjs.com/package/standard-version) and the project's [commit standard](./commit-and-code-standards.md#commit-and-code-standards).

Once standard-version is run, a new version tag will be created using the [semver](https://semver.org/) version standard and a changelog will be written.

::: warning
Any commit message that does _not_ follow the commit standard will not be taken into account when calculating the version number, nor will it appear in the changelog.
:::

## Creating a release

To create a release, run the pre-defined script provided in the project's `package.json` file.

```bash
npm run release
```

Once your new release is created, use git to push the tag to your remote repository.

```bash
git push --follow-tags
```

## Creating a pre-release

To create a prerelease, you will need to pass the `--prerelease` option to standard-version.

```bash
npm run release -- --prerelease rc
```

For more information specific to running prereleases, please see standard-version's [official documentation](https://github.com/conventional-changelog/standard-version#release-as-a-pre-release).

## Troubleshooting with a dry-run

To troubleshoot or preview what will happen when running any kind of release, you can pass the `--dry-run` option to standard-version.

```bash
npm run release -- --dry-run
```

This will go through all of the processes for checking and bumping the version number of the project without altering any files or creating new tags. The changelog will be output to the terminal.
