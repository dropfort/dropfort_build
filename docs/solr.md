---
title: Solr
tags:
  - solr
---

# Solr

If you've enabled your local Solr Cloud instance by setting the **solr** flag in your Docker Compose Profiles environment variable, you'll have a local Solr Cloud instance setup automatically.

## Local Development

### Default Solr Cloud Credentials
You can access the Solr web ui at [http://solr.dflocal.net:8000](http://solr.dflocal.net:8000) with **solr** and **SolrRocks** as the user/pass respectively.

::: warning Do not use these credentials in production
The credentials and certificates in Dropfort Build are from the Apache Solr documentation examples and should not be used in production
:::

### Versions of Solr
You can control the version of solr installed by setting the `SOLR_VERSION` variable in your `docker/.env` file.

For example:

```ini
SOLR_VERSION=9
```

Will download the latest Solr 9.x container.

## Deployed Releases

Dropfort can automatically manage the Solr host, username and password for any Search API Solr 4.x backend automatically. You can set the `DF_DRUPAL_SEARCH_API_SOLR_MANAGED_BACKENDS` with a comma separated list of config backend machine names and Dropfort will set the host to `DF_DRUPAL_SOLR_HOST`, username to `DF_DRUPAL_SOLR_USER` and password to `DF_DRUPAL_SOLR_PASSWORD` for each backend.

E.g.

```env
DF_DRUPAL_SEARCH_API_SOLR_MANAGED_BACKENDS='default_solr_server,other_solr_server'
```

Automatically sets

```php
$managed_solr_backends = getenv('DF_DRUPAL_SEARCH_API_SOLR_MANAGED_BACKENDS') ?: '';
$managed_solr_backends = trim($managed_solr_backends);
if (!empty($managed_solr_backends)) {
  $managed_solr_host = getenv('DF_DRUPAL_SOLR_HOST');
  $managed_solr_user = getenv('DF_DRUPAL_SOLR_USER');
  $managed_solr_password = getenv('DF_DRUPAL_SOLR_PASSWORD');
  $managed_solr_servers = explode(',', $managed_solr_backends);
  foreach($managed_solr_servers as $managed_server) {
    $config['search_api.server.' . $managed_server]['backend_config']['connector'] = 'solr_cloud_basic_auth';
    $config['search_api.server.' . $managed_server]['backend_config']['connector_config']['host'] = $managed_solr_host;
    $config['search_api.server.' . $managed_server]['backend_config']['connector_config']['username'] = $managed_solr_user;
    $config['search_api.server.' . $managed_server]['backend_config']['connector_config']['password'] = $managed_solr_password;
  }
}
```

in your settings.auto.php file or settings.php in containerized deployments.

### Kubernetes Deployments
For Kubernetes releases the host, username and password env variables are automatically added. You only need to provide the `DF_DRUPAL_SEARCH_API_SOLR_MANAGED_BACKENDS` environment variable in your deployment configuration. You can do so through https://api.dropfort.com or in your `k8s/<env>/<site>.yml` file in the source repo under the `extraEnvVars` section.