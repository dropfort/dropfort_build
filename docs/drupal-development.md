---
title: Drupal development
tags:
  - drupal
  - composer
  - drush
  - config_split
  - xhprof
  - xhgui
  - xdebug
  - mailhog
  - email
  - debug
---

# Drupal Development

Once you've opened your project in VSCode and opened in a remote container Dropfort Build will take care to create your **settings.php** file and public/private files directories automatically. You'll also have a working database connection as well.

The default URL for your local site is http://drupal.dflocal.net:8000

## SSH and Drush
Dropfort Build also takes care to forward your SSH keys from the host OS into your containers. This allows you to use Drush commands on remote aliases to sync data, files, or any other command you'd like to run.

It is recommended on the host OS to set the following (or similar) in your ~/.ssh/config file to ensure you use the right username when triggering SSH sessions remotely.

```ini
Host *
  User myusername
```

In the container you're logged in as the **root** user which will end up being the default username used by SSH unless told otherwise. Optionall your could set a username in your Drush alias files or have host specific entries for your username in your ~/.ssh/config file to ensure you use the right credentials when performing remote actions.

```
Host mydev.site
  User myusername

Host otherdev.site
  User otherusername
```

## Initial setup
When you first launch your environment you'll need to run `composer install` to build your Drupal instance. You can then use Drush to SQL sync a database from another instance or use the site install command to start fresh.

::: tip Files in volumes
Your docker volume data (e.g. /var/www/html/web, /var/www/html/vendor, /var/www/html/node_modules) are not synced back to the host OS disk. They only exist in the volume for performance reasons. You won't see your code changes outside of the container for that reason
:::

E.g.
Copy an existing database, clear the cache, run database updates and import config.
```shell
drush sql:sync @mysite.dev @self
drush deploy
```

Install an existing site from a config import
```shell
drush si --existing-config
```

Install a fresh site
```shell
drush si minimal
```

Once your site is synced or installed you can run `drush uli` to get a login link to your local env.

::: tip Access denied
If you get an 'Access denied' from your login link, it's likely the super admin account is blocked. You can unblock them with `drush user:unblock admin`
:::

## Services Configuration

Your default services.yml file in `web/sites/default/services.yml` is scaffolded by Drupal's core composer scaffold. This file however is not versioned by git since it sites in your `web/` directory. Dropfort Build provides a `services/` directory for storing environment/config split specific services files (e.g. `dev.services.yml`, `prod.services.yml`) as well as an `override.services.yml` file in that directory. The override file is to overwrite values in the stock `services.yml` file provided by Drupal which are common to all environments. The other files are then loaded based on your active config split environment.

Files in the `services/default` directory are for your default site. For multisite configurations, name the directories within the services directory the same as your multisite directories (e.g. sites/anothersite => services/anothersite).

## Stock Drupal Configuration

Dropfort Build ships with stock Drupal configurations for a local, dev, qa and prod environment. These include settings for config split and environment indicator.

Note that you'll need to ensure all the required modules are enabled to import the config. For instance `drush en environment_indicator config_split -y` if you haven't done so already.

You can import the stock config:

```shell
drush config:import --partial --source="../vendor/dropfort/dropfort_build/assets/scaffold/config/sync" -y
```

You can import the config for each predefined split depending on your active split:

For example, if your active split is set to "prod" you can import the stock prod config:
```shell
drush config:import --partial --source="../vendor/dropfort/dropfort_build/assets/scaffold/config/prod" -y
```

Once you're satisfied with your configuration changes, don't forget to export the configs to your config directory with `drush config:export`

## Environment Indicator

Similar to the config_split setup, we also ship with stock Environment Indicator config. You can import

## Multi-site Setups

If your Drupal site has a multisite configuration, Dropfort Build will map your sites/* directories to *.drupal.dflocal.net automatically. It will also set a settings.php file, public files and private files for each multi-site. It also has a sites.php file which will automaticlaly route requests from those subdomains to the appropriate multi-site.

It will also automatically set your trusted host patterns properly so you don't get any warnings in your status report pages.

If you want to use memcache with your multisites, each settings.php file sets an appropriate prefix to prevent collisions in the memcache storage.

### Additional Database Creation
Your default Drupal database is created automatically. Your multi-sites however will require a manual step to create their databases the first time you setup your environment.

Once you have your main site running (or at the very least have Drush installed), you can use Drush to create the additional databases. From within your multi-site's site directory (e.g. /var/www/html/web/sites/mysite) you can run the following:

```
drush --uri=drupal.multisite.mysite sql-create --db-su=root --db-su-pw=$DB_ROOT_PASSWORD
```

This will use the preloaded env variable DB_ROOT_PASSWORD and the automatically generated database connection settings to create your database. Then you can run your site installer or import your existing site into your database.

# Migrations
You can use your Dropfort Build environment to write migrations to your new Drupal site. There's a preconfigured migration source database defined in the settings.php file for your site which can connect to a local database of your migration source site (e.g. Drupal 7 database).

## Enable Migration Database
To enable your Migration database, edit the `container/dev/.env` file and add the `migrate` flag to the comma separated list of items in the COMPOSE_PROFILES environment variable.

e.g.

```shell
COMPOSE_PROFILES=solr,migrate
```

Rebuild your container and the new docker services should be available. You can test the connection with Drush by using the following command:

```shell
drush sql:cli --database=migrate
```

You should get a CLI interface on your migration database instance.
## Loading Migration Data

To work form your local migration database you'll need to use `drush sql:query --database=migrate --file=/var/www/html/source.sql.gz` or similar command to load the SQL data in to your migration database. The simplest way to get your database into your container is to put a copy of the sql or gzipped sql into your project's folder on your host machine. The dev container will see the file and allow you to import the data into your database container.

::: warning SQL Query Parameters
Note that it is very important to specify the `--database=migrate` flag otherwise the command will crush your local Drupal site's data.
:::

# Running Multiple Instances of Dropfort Build

If you want to have more than one environment up at the same time, you only need to define unique ports on the host machine to forward to your containers.

If the `container/dev/.env` file set the following three values to any other port you like. For example:

```shell
HOST_PORT=8001
HOST_TLS_PORT=8543
MANAGE_PORT=8081
```

The `HOST_PORT` and `HOST_TLS_PORT` values are the HTTP/HTTPS ports to access your sites and local services with in the browser. The `MANAGE_PORT` is the port to use to view the current Traefik status for that Dropfort Build environment.

For example, based on the ports above your new URLs would be

- http://drupal.dflocal.net:8001
- https://drupal.dflocal.net:8543
- http://drupal.dflocal.net:8081

So long as your keep your ports from colliding, you can have as many instances of Dropfort Build up at a time as you like.

# Debug Tools

Dropfort build comes with XDebug preconfigured. You can trigger a debug session in VSCode with the debug tool (usually CTRL/CMD+F5) and set breakpoints in your IDE where you like.

## Email debug

All emails sent by your Drupal site are captured automatically as long as you're using the default mailer which uses the local postfix service. So as long as you've sending from modules such as SMTP or Postmark you can see your mail messages in the local Mailhog instance which is accessible at http://mailhog.dflocal.net:8000

## Search API Solr debug

We recommend enabling the **Search API Solr Admin** module in your local environment to interface with the local Solr Cloud instance (if enabled).

For local development, set the solr node to 'solr'. Set **solr** and **SolrRocks** as the user/pass respectively in the solr server configuration in Drupal. Remember to set a collection name prior to doing the next step.

Once the Solr server has been set up in Drupal, click on the name of the server. Click 'Upload Configset' to update the config on the Solr server.

For more information on using Solr, see the Solr section.

## Debugging PHP with XDebug

PHP and VSCode are preconfigured with XDebug v3 enabled by default. You can start a debug session using the "Run and Debug" command pallet and select "Listen for XDebug (v3)". Typically **F5** is a shortcut to start the debugger.

Then add a breakpoint to any PHP file and VSCode will pause the execution at that point.

::: v-pre
This also works with the [Twig XDebug](https://drupal.org/project/twig_xdebug) module as well. Add your `{{ breakpoint() }}` to a twig file and start your debugger in VSCode. The page rendering will pause at that point.
:::

*N.B. The Twig XDebug may require a `drush cr` to force a new compile of a twig file depending on your services.yml config and twig cache settings.*

## Performance profiling with XHProf and XHGui

XHProf dependencies are automatically added to your default Drupal container. You can run a debug session using the [XHProf](https://drupal.org/project/xhprof) module directly. However Dropfort Build also comes with XHGui as a means of storing your performance runs and an improved user experience while sifting through performance data.

To enable XHGui, edit your `container/dev/.env` file and append the `profiling` or `debug` option to the comma separated list of `COMPOSE_PROFILES` values.

E.g

```
COMPOSE_PROFILES=profiling
```

Then reopen your project in VSCode so Docker and VSCode can start the additional containers.

This will enable xhgui.dflocal.net:8000 URL to work.

Next, you can download and install the (XHGui PHP Profiler)[https://drupal.org/project/php_profiler] module as a dev dependency to your project (e.g. `composer require --dev drupal/php_profiler`). Enable the module along with its dependencies.

```
drush en php_profiler -y
```

::: warning Disable profiling by default
Be sure to disable XHProf / PHP Profiler by default in your site config or use Config Split to disable php_profiler from other envs other than local. We also recommend leaving the profiler off by default to avoid performance issues while in active development.
:::

Then go to the **admin/config/development/xhprof** page in your Drupal site. Enable the debugger and select "XHGui Upload" as the profile storage option. The default values for the XHGui URL "http://xhgui" and a blank XHGui token should suffice.

Then trigger a page load and you'll see your XHProf data in the XHGui interface. This run data is saved to a volume so the runs can persist between debug sessions.

## Performance testing with JMeter

The performance profile includes an instance of JMeter which you can use to write out your test scenarios locally. The `testing/jmeter` directory in your drupal project is automatically synced to the `/tests` directory in your jmeter instance.

You can access jmeter.dflocal.net:8000 and connect to the noVNC desktop to view the JMeter UI and load you tests from there.
