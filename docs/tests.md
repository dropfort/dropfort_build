<!-- Copyright (c) 2019 Coldfront Labs Inc.

This file is part of DropfortCI Scripts.

DropfortCI Scripts is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DropfortCI Scripts is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DropfortCI Scripts.  If not, see <https://www.gnu.org/licenses/>. -->

# Built-in Tests

A few generic tests are included by default. These mostly include compatibility tests for future versions of PHP, Drupal and VueJS. These tests are allowed to fail and will not stop a deployment by default. These tests are meant to be informational to developers and managers to keep an active view on pending changes and potential issues in future releases if steps aren't taken to resolve the deprecations.

Teams are encouraged to add additional tests to this stage as required. These tests are designed to run without an active build of the application. Therefore they lend themselves well to tests which are performing static code analysis.

Note that all the tests listed below are the default behavior. Testing tools and arguments can be modified by editing your **.gitlab-ci.yml** file, your **package.json** and/or your **composer.json** files within your project.

## PHP Compatibility Test
Runs a scan on custom code directories for any syntax issues with PHP 7 or higher. You can optionally increase the minimum PHP version to test if you're already running later versions of PHP.

## Drupal Deprecations Test
Runs the `drupal-check` command (see https://github.com/mglaman/drupal-check) on custom modules and themes directories by default. Reports only on deprecations.

## NPM Outdated Test
Runs `npm outdated` to check for available updates for npm dependencies.

## Composer Outdated Test
Runs the `composer outdated` check on direct dependencies of your project.

## Broken Link Check
**Manual Trigger** Performs a broken link check on the deployed Drupal site. By default it will check 3 levels deep and report any broken paths, images or links. It will produce an HTML and CSV report for review. Check the "validate" stage of jobs to run the link checker.
