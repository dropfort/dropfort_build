import { defineConfig } from "vitepress";
import markdownItPlantUML from "markdown-it-plantuml";

export default defineConfig({
  lang: "en-US",
  title: "Dropfort Build Tools",
  description: "Drupal development tools, CI/CD checks and validation scripts.",
  base: "/dropfort_build/",
  outDir: "../public",
  markdown: {
    config: (md) => {
      md.use(markdownItPlantUML, {
        openMarker: "```plantuml",
        closeMarker: "```",
      });
    },
  },
  themeConfig: {
    sidebar: [
      {
        text: "Getting Started",
        link: "/",
      },
      {
        text: "Project scaffolding",
        link: "/scaffolding",
      },
      {
        text: "Local development environment",
        link: "/local-environment",
      },
      {
        text: "Drupal Development",
        link: "/drupal-development",
      },
      {
        text: "Using Apache Solr",
        link: "/solr",
      },
      {
        text: "Commit and code standards",
        link: "/commit-and-code-standards",
      },
      {
        text: "Automatic versioning for releases",
        link: "/automatic-versioning-for-releases",
      },
      {
        text: "Release Workflow Gitlab CI",
        link: "/stages-and-jobs",
      },
      {
        text: "Automatic site updates",
        link: "/automatic-dependency-updates",
      },
      {
        text: "Environment variables",
        link: "/environment-variables",
      },
      {
        text: "Override Default Configuration",
        link: "/override-default-configuration",
      },
      {
        text: "Automated Testing",
        link: "/tests",
      },
      {
        text: "Troubleshooting",
        link: "/troubleshooting",
      },
    ],

    socialLinks: [
      { icon: "github", link: "https://gitlab.com/dropfort/dropfort_build" },
    ],

    search: {
      provider: "local",
    },
  },
});
