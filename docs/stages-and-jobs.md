---
title: CI/CD Stages and Jobs
tags:
  - gitlab
  - ci
  - cd
  - ci/cd
  - task
  - build
  - test
  - predeploy
  - pre_deploy
  - deploy
  - update
  - update_data
  - post_update
  - prevalidate
  - pre_validate
  - validate
  - security
  - cleanup
---

# CI/CD Stages and Jobs

The automatic deployment is set up in your [`.gitlab-ci.yml`](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/scaffold/gitlab-ci.yml) file.

This file references your [profile.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/profile.drupal.yml) file which contains the definition for the stages in the build process.

[[toc]]

## Task

This stage contains manual jobs available for copying data and files between environments.

The following jobs are available in this stage:

### Backcopy Data

Performs a `drush sql:sync` from the production environment to the selected target environment, after which the database will be sanitized, cache cleared, data sanitized, updatedb and a config import.

If `DF_TASK_BACKCOPY_SKIP_DROP_TARGET_DATABASE` is set to a non-empty value then the `drush sql:drop` will be skipped on the target (a.k.a destination). Default action is to skip the sql drop.

If `DF_TASK_BACKCOPY_SKIP_SANITIZE` is set to a non-empty value then the `drush sql:sanitize` will be skipped on the target (a.k.a destination). Default action is to sanitize the data.

If `DF_TASK_BACKCOPY_SKIP_DEPLOY` is set to a non-empty value (e.g. 'true', 1, 'yes') then the `drush deploy` command will be skipped after back copying data.

**REMOVED** ~~If `BACKCOPY_CIM` is set to `"true"` a `drush config:import` will be run after the database is backcopied to ensure proper environment config is applied.~~

If backcopying from Production to Dev, user 1 will be unblocked to allow for easier testing and development work.

The default jobs provide you with the ability to backcopy data _from_ **Production** _to_ **QA** or **Dev**.

::: tip
This job can only be triggered by clicking the **play** button in the GitLab CI UI from a production release pipeline.
:::

**Source:** [task.backcopy-data.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/task.backcopy-data.drupal.yml)

### Backcopy Files

Performs a `drush rsync` from the production environment's files directory to the selected target environment's files directory.

The default jobs provide you with the ability to backcopy files _from_ **Production** _to_ **QA** or **Dev**.

::: tip
This job can only be triggered by clicking the **play** button in the GitLab CI UI from a production release pipeline.
:::

**Source:** [task.backcopy-files.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/task.backcopy-files.drupal.yml)

### Get Config

Performs a `drush config:export` on the target environment and saves it as an artifact to download.

The default jobs provide you with the ability to download config from **Production**, **QA**, or **Dev**.

::: tip
This job can only be triggered by clicking the **play** button in the GitLab CI UI.
:::

**Source:** [task.get-config.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/task.get-config.drupal.yml)

### Get Dev Database

Runs `drush sql:dump` on the development environment and saves the resulting file as an artifact to download. Data is sanitized before dumping.

::: tip
This job can only be triggered by clicking the **play** button in the GitLab CI UI.
:::

**Source:** [task.get-db.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/task.get-db.drupal.yml)

### Kunbernetes - Restart/Cache Clear Varnish

Runs `kubectl` rolling update to your kubernetes cluster to clear the varnish cache in the cluster.

::: tip
This job can only be triggered by clicking the **play** button in the GitLab CI UI.
:::

**Source:** [task.k8s-restart-varnish.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/task.k8s-restart-varnish.yml)

### Kunbernetes - Update Secrets

Runs `kubectl` to update secrets stored in environment variables in your application container for any secrets set in https://api.dropfort.com

::: tip
This job can only be triggered by clicking the **play** button in the GitLab CI UI.
:::

**Source:** [task.k8s-update-secrets.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/task.k8s-update-secrets.yml)

### Kunbernetes - Update Varnish Configuration/VCL

Runs `helm upgrade` to update the running configuration for Varnish in the cluster. Applies latest configuration from https://api.dropfort.com

::: tip
This job can only be triggered by clicking the **play** button in the GitLab CI UI.
:::

**Source:** [task.k8s-update-varnish.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/task.k8s-update-varnish.yml)


## Build

The build stage runs package manager installers such as **composer** and **npm** to download required dependencies and then compiles and minifies the code.
Once the code is compiled and ready a build artifacts is saved for use in subsequent stages and jobs.

The following jobs are available in this stage:

### Drupal Build

Downloads and compiles all the code required to create a build artifact for the site using `composer install` and `npm install`. This job can also compile a theme using npm scripts.

While release build artifacts will not expire, dev build artifacts will be removed after 24 hours.

::: warning
These jobs assume you have npm scripts for `theme:build` and `theme:dev`. These jobs are used to compile the theme.

If you do not have either of these in your package.json file, the builds will fail.
:::

The default jobs provide you with the ability to build the Drupal site for either **releases** or **development**.

**Source:** [build.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/build.drupal.yml)

## Test

This stage is used to run tests against the compiled build and to perform reference checks prior to deployment.

This stage is reserved for tests which require the code base to be available but don't require a deployed copy of the web application. For example unit test can be run during this stage.

### Drupal Coding Standards

Runs PHP Code Sniffer on the build artifact to ensure all custom PHP code passes the coding standards. For more information see [PHP coding standards](./commit-and-code-standards.md#php-coding-standards).

The default jobs provide you with the ability to build the drupal site _from_ **release** _or_ **dev**.

**Source:** [code-test.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/code-test.drupal.yml)

### Scripts Coding Standards

Runs ESLint on the code base to ensure all custom JavaScript code passes the coding standards. For more information see [JavaScript coding standards](./commit-and-code-standards.md#javascript-coding-standards).

**Source:** [code-test.scripts.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/code-test.scripts.yml)

## Pre Deploy

This is an **optional stage** which is **triggered when required for feature branches**, and **for the dev branch** if the **DF_INITIAL_SETUP** environment variable is set to _true_.

This stage is used to set up the web application for the first time. It will deploy the code, set the database credentials and perform a fresh install.

This stage will be **triggered automatically (if required) for feature and development branches**. Set the following variable to trigger this stage for the dev branch:
- `DF_INITIAL_SETUP=true`

::: warning
This stage destroys any data on the remote target.
:::

The following jobs are part of this stage:

### Initial Deploy Feature

This job is run to set up a feature environment based on a feature branch. This
job is only triggered if `CI_INITIAL_SETUP` is set to `true` and the deployment pipeline was triggered by a change to a branch other than the _8.x-1.x_ branch.

**Source:** [deploy.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/deploy.drupal.yml)

### Deploy Feature Stop

Clean up feature environment.

**Source:** [deploy.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/deploy.drupal.yml)

### Initial Deploy Dev

This job is run to set up a development environment. This job is only triggered if `CI_INITIAL_SETUP` is set to ‘true’ and the deployment pipeline was triggered by a change to the _8.x-1.x_ branch.

::: Warning
This stage will destroy any data on the remote target.
:::

**Source:** [deploy.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/deploy.drupal.yml)

## Deploy

The deploy stage downloads artifacts generated during the [build stage](#build) and copies the files to a target server.

By default, Dropfort Build provides four different deploy jobs: **feature**, **dev**, **qa**, and **prod**.

### Feature Environment Deployment

Feature branches are copies of the development enviornment used to implement new features without worrying about your work being pushed upstream to other environments.

This job runs automatically every time new commits are pushed to a branch that is not the development branch.

A link to the feature environment is provided in the [update stage](#update-data).

**Source:** [deploy.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/deploy.drupal.yml)

#### Kubernetes Feature Environment Configuration

For Kubernetes environments, a `k8s/feat/sitename.yml` file is required with the following information defined beyond the usual k8s deployment configuration:

1. sourceName: Name of the deployment to copy data/files from
1. sourceSecret: Name of the secret to allow access to the source deployment data (typically `<sourceName>-envsecrets`).
1. syncFiles: Boolean true/false whether to copy the files (e.g. images, pdfs, etc) from the source site to the feature env.
1. copyDatabase: Boolean true/false whether to copy the database from the original site. If a database already exists for the feature env the database copy is skipped.

```yaml
nameOverride: coo-feat-shenkman

image:
  repository: git.dropfort.com:4567/city-of-ottawa/hosting/shenkman_profile
  tag: main
  pullPolicy: Always

imagePullSecrets:
  - name: coo-sec-shenkman

storageClass: coo-feat-lbnfs
storageSize: "10Gi"

extraEnvVars:
  - name: MARIADB_AUTO_UPGRADE
    value: "true"

sourceName: "coo-dev-shenkman"
sourceSecret: "coo-dev-shenkman-envsecrets"
syncFiles: true
copyDatabase: true
```

**Source:** [deploy.k8s.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/deploy.k8s.yml)

### Dev Deployment

The development environment is automatically deployed every time new commits are pushed to the development branch.

Valid development branch names can be found in the [environments](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/environments.drupal.yml) file.

**Source:** [deploy.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/deploy.drupal.yml)

### QA Deployment

The QA environment is automatically deployed whenever a pre-release tag is pushed to the repository.

Valid pre-release tags can be found in the [environments](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/environments.drupal.yml) file.

**Source:** [deploy.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/deploy.drupal.yml)

### Prod Deployment

The Production environment is set up to deploy whenever a release tag is pushed to the repository.

Valid release tags can be found in the [environments](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/environments.drupal.yml) file.

::: tip
This job can only be triggered by clicking the **play** button in the GitLab CI UI.
:::

**Source:** [deploy.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/deploy.drupal.yml)

### Kubernetes Deployment

Helm creates a deployment to a Kubernetes cluster as defined by your kubectl config. Helm will run a rolling release and rollback on any container error. This job has a 10 minute timeout by default.

**Source:** [deploy.k8s.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/deploy.k8s.yml)

## Update Data

This stage is used for tasks such as database updates, migrations or other tasks related to data changes which require the new code to already be in place.

The following jobs are defined for Update Data stage: **update feature**, **update dev**, **update qa**, **update prod**.

**Source:** [update.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/update.drupal.yml)

## Post Update

This stage is used for running migrations.

The following jobs are defined for Update Data stage: **update feature**, **update dev**, **update qa**, **update prod**.

::: tip
These jobs are allowed to fail.
:::

**Source:** [post_update.migrate.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/post_update.migrate.yml)

## Pre Validate

This stage is used for tasks which must occur after new configuration has been applied but may be required by validation tests. For example updating search indexes or running data migrations.

The following jobs are defined for Pre Validate stage: **update search feature**, **update search dev**, **update search qa**, **update search prod**.

**Source:** [pre_validate.search.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/pre_validate.search.yml)

## Validate

The validation stage is used to check the quality and consistency of the deployment. During this stage, automated tests are run to verify the functionality and the _look and feel_ of the updated site to confirm it was deployed properly. This stage is different from the **Test** phase as these tests are performed on a live remote site.

The following jobs are available in this stage:

### Validate Acceptance

Acceptance tests are run using Codeception to ensure that the site is functioning as expected.

The following jobs are defined for Validate acceptance stage: **validate feature acceptance**, **validate dev acceptance**, **validate qa acceptance**, **validate prod acceptance**.

**Source:** [validate.acceptance.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/validate.acceptance.drupal.yml)

### Validate Accessibility

Accessibility tests are run using Axe rules to ensure that the site meets accessibility standards.

The following jobs are defined for Validate acceptance stage: **validate feature accessibility**, **validate dev accessibility**, **validate qa accessibility**, **validate prod accessibility**.

**Source:** [validate.accessibility.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/validate.accessibility.yml)

### Validate CSS

Visual regression tests are run using BackstopJS to compare pages on the site against reference images. These tests are run to ensure that no unintended visual changes have occurred on the site.

The following jobs are defined for Validate acceptance stage: **validate feature CSS**, **validate dev CSS**, **validate qa CSS**, **validate prod CSS**.

**Source:** [validate.css.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/validate.css.yml)

### Validate Core Requirements

Validate core requirements with drush.

The following jobs are defined for the validate core requirements jobs: **validate feature core requirements**, **validate dev core requirements**, **validate qa core requirements**, **validate prod core requirements**.

**Source:** [validate.core-requirements.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/validate.core-requirements.drupal.yml)

### Validate Drupal Deprecations

Check drupal deprecations.

The following jobs are defined for the validate core requirements jobs: **test drupal deprecation dev**, **test drupal deprecation release**.

### Validate Links

Validate links in the given environment.

The following jobs are defined for the validate core requirements jobs: **validate feature links**, **validate dev links**, **validate qa links**, **validate prod links**.

**Source:** [validate.links.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/validate.links.yml)

### Validate Outdated Composer

Validate outdated composer packages.

The following jobs are defined for the validate core requirements jobs: **test outdated composer dev**, **test outdated composer release**.

**Source:** [validate.outdated.composer.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/validate.outdated.composer.yml)

### Validate Outdated NPM

Validate outdated npm packages.

The following jobs are defined for the validate core requirements jobs: **test outdated npm dev**, **test outdated npm release**.

**Source:** [validate.outdated.npm.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/validate.outdated.npm.yml)

### Validate PHP Compatability

Test PHP compatability.

The following jobs are defined for the validate core requirements jobs: **test PHP compatability dev**, **test PHP compatability release**.

**Source:** [validate.phpcompatibility.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/validate.phpcompatibility.drupal.yml)

### Validate URI

Validates that the site URI is set properly in the site's configuration.

The following jobs are defined for the validate URI jobs: **validate feat URI**, **validate dev URI**, **validate qa URI**, **validate prod URI**.

**Source:** [validate.uri.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/validate.uri.drupal.yml)

::: warning
Failures during the validation phase do not rollback the deployment.
:::

::: tip
Rolling back changes after this point requires a new deployment or manual intervention.
:::

::: tip
If you would like to run a pipeline with only the validate jobs, include [validate only] in your commit message.
:::

::: tip
If you would like to run a pipeline without the validate jobs, include `[skip validate]` in your **commit message** or run the pipeline with the `SKIP_VALIDATE` environment variable set to _true_.
:::

### Validate URI Pantheon

Validates that the site URI is set properly for pantheon in the site's configuration.

The following jobs are defined for the validate pantheon URI jobs: **validate dev URI pantheon**, **validate qa URI pantheon**, **validate prod URI pantheon**.

**Source:** [validate.uri.drupal.pantheon.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/validate.uri.drupal.pantheon.yml)

## Security

The security stage checks for security updates and outdated packages from Composer, NPM, and Drupal.

The following jobs are available in this stage:

### Validate Security Drupal

This job checks for security vulnerabilities for drupal.

The following jobs are defined for the validate security drupal jobs: **validate security drupal feature**, **validate security drupal dev**, **validate security drupal qa**, **validate security drupal prod**.

**Source:** [security.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/security.drupal.yml)

### Validate Security NPM

This job checks for security vulnerabilities for npm.

The following jobs are defined for the validate security drupal jobs: **validate security npm feature**, **validate security npm dev**, **validate security npm qa**, **validate security npm prod**.

**Source:** [security.npm.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/security.npm.yml)

### Validate Security Composer

This job checks for security vulnerabilities for composer.

The following jobs are defined for the validate security drupal jobs: **validate security composer feature**, **validate security composer dev**, **validate security composer qa**, **validate security composer prod**.

**Source:** [security.composer.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/security.composer.yml)

## Post Security

The post security stage is mainly for acting upon the results of the security stage. The following jobs are available:

### Apply Updates

This job applies security and general updates for composer and npm.

**Source:** [post_security.apply-updates.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/post_security.apply-updates.yml)

## Cleanup

The last step in the pipeline is to clean up temporary files and older backups, and perform any final reporting on the deployment.

The following jobs are available in this stage:

### Clean Up

This job cleans up the given environment by removing site files and backing up the site into a single archive.

The following jobs are defined for the clean up jobs: **clean up  feature**, **clean up dev**, **clean up qa**, **clean up prod**.

**Source:** [cleanup.drupal.yml](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/assets/ci/templates/cleanup.drupal.yml)
