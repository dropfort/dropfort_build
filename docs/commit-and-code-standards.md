---
title: Commit and code standards
tags:
  - commit
  - commit message
  - code standards
  - lint
  - linting
  - standards
  - code check
---

Dropfort Build comes with multiple tools to ensure both commit message standards and code standards for multiple languages are met.

## Commit message standards

Commit messages are checked for standard violations locally using a combination of [husky](https://typicode.github.io/husky) git hooks and [commitlint](https://commitlint.js.org). If you commit message does not match the standard, it will be denied.

By default, commit messages use the conventional commit standard, which means your commits should follow a basic template of:

> \<type>[optional scope]: \<description>
>
> [optional body]
>
> [optional footer(s)]

For more detailed information about available types, scopes, breaking changes, etc. please see the [official documentation](https://www.conventionalcommits.org/en/v1.0.0/).

## PHP coding standards

PHP code is checked for standard violations both locally and remotely during the test stage of the CI/CD pipeline.

Locally a [husky](https://typicode.github.io/husky) git hook is set up to use [PHP Code Sniffer](https://github.com/squizlabs/PHP_CodeSniffer) through [lint-staged](https://www.npmjs.com/package/lint-staged) to check your staged PHP files. Any automatically fixable violations will be corrected and added to your commit. All other violations will prevent the commit until they are resolved.

During the test stage of the CI/CD pipeline PHP Code Sniffer is used to check _all_ custom PHP code within the project.

Both the local and remote tests use the official [Drupal coding standards](https://www.drupal.org/docs/develop/standards/coding-standards).

## JavaScript coding standards

JavaScript is checked for standard violations both locally and remotely during the test stage of the CI/CD pipeline.

Locally a [husky](https://typicode.github.io/husky) git hook is set up to use [ESLint](https://eslint.org/) through [lint-staged](https://www.npmjs.com/package/lint-staged) to check your staged JavaScript files. Any automatically fixable violations will be corrected and added to your commit. All other violations will prevent the commit until they are resolved.

During the test stage of the CI/CD pipeline ESLint is used to check _all_ custom JavaScript within the project.

Both the local and remove tests use the Drupal JavaScript standard provided by [eslint-plugin-drupal-contrib](https://www.npmjs.com/package/eslint-plugin-drupal-contrib).
