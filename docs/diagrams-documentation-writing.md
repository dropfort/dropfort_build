---
title: Documentation and Diagrams
tags:
  - drupal
  - uml
  - plantuml
  - markdown
---

# Documentation Writing

Dropfort Build provides VSCode extensions for building documentation in Markdown and UML.

## Markdown Docs
Dropfort Build includes the Markdown plugin in VSCode. You can edit markdown files and see a live preview of those docs by right-clicking the mardown file in question and selecting "Open preview". You can then move that preview to a new window to edit the source and see the updates live as you're working.

For more information about [writing markdown in VSCode please refer to the official documentation](https://code.visualstudio.com/docs/languages/markdown).

## UML Diagrams
If you use the Drupal Spec tool you can generate your Entity Relationship Diagram using that. Then your generated UML can be put into your `docs/diagrams/src` directory with any file extension supported by PlantUML (e.g. .plantuml) and it can be compiled into PNG files or other sharable formats.

UML is also supported in most GitLab instances with `\```plantuml` as a prefix in your Markdown.
