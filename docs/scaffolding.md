---
title: Project scaffolding
tags:
  - scaffold
  - composer
---

# Project scaffolding

Dropfort Build is based off of Drupal's [core-composer-scaffold](https://github.com/drupal/core-composer-scaffold) and follows all of the same processes for setting up and altering scaffolded filess.

## Updating scaffold files

::: danger Those who ran Dropfort Build prior to v2.2 must replace their package.json files with the updated one provided

There is a new [standard-version](https://github.com/conventional-changelog/standard-version) section in the package.json which is required to prevent composer validation errors on releases. See [default.package.json](https://gitlab.com/dropfort/dropfort_build/-/blob/5.x/assets/scaffold/default.package.json) for details.
:::

Most scaffolded files will be overwritten with every install/update, meaning all custom changes will be removed. If you'd like to perminently customize a file, see [Changing default behavior](#changing-default-behavior).

The following files will _not_ be overwritten by default and will instead only be scaffolded if they do not already exist:

- [package.json](https://gitlab.com/dropfort/dropfort_build/-/blob/5.x/assets/scaffold/default.package.json)

Periodically it is a good idea to manually delete these files and re-scaffold them to gain any updates which may have been added.

You are responsible for manually updating unscaffolded files as new versions are released. The easiest way to merge changes into an overridden file is to delete the file, run `composer scaffold` and then use your git diff tool of choice to merge your changes back in.

For a full list of scaffolded files, please see [Dropfort Build's composer.json](https://gitlab.com/dropfort/dropfort_build/-/blob/5.x/composer.json) file.

::: warning Commit these files!
All these scaffolded files should be versioned with updates committed to your Drupal project. This is different from most scaffold projects, but these files need to exist for the CI/CD pipelines to function properly and to share development environment configuration with others.
:::

### Changing default behavior

If you have customized a scaffolded file that is normally overwritten you can tell composer to disable the scaffolding.

See [Core Composer Scaffold - Excluding Scaffold Files](https://github.com/drupal/core-composer-scaffold#excluding-scaffold-files) for more details.

As an example, if you made changes to your `.eslintrc.js` file and customized the `docker-compose.yml file`, you would have to add the following:

#### Using the Composer CLI
```
composer config --merge extra.drupal-scaffold.file-mapping --json '{"[project-root]/.eslintignore" : false}'
composer config --merge extra.drupal-scaffold.file-mapping --json '{"[project-root]/docker/docker-compose.yml" : false}'
```

#### Manually in your composer.json file
```json{7-9}
// Inside your project's composer.json.
{
  "name": "my/drupal-project",
  ...
  "extra": {
    "drupal-scaffold": {
      "file-mapping": {
        "[project-root]/.eslintrc.js": false,
        "[project-root]/docker/docker-compose.yml": false
      }
    },
    ...
  }
}
```

### Modifying scaffolded files

If you'd like to add your own changes to scaffolded files, you can append or patch scaffolded files. See [Alerting Scaffolded files](https://www.drupal.org/docs/develop/using-composer/using-drupals-composer-scaffold#toc_4) for details.

E.g.

```json
  "[project-root]/.gitignore": {
    "append": "patches/my-gitignore-additions.txt"
  },
```
