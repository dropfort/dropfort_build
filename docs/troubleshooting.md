---
title: Troubleshooting
tags:
  - drupal
  - docker
  - drush
---

# Troubleshooting

Dropfort Build attempts to be a simple as possible to use. But sometime things get stuck or don't work as expected. Here are some general tips on resolving common issues.

If you can't find a resolution here, feel free to open and issue at https://gitlab.com/dropfort/dropfort_build/-/issues and we'll do our best to help solve your problem.

## I get a 404 when accessing drupal.dflocal.net subdomain

If you try to access drupal.dflocal.net:8000 and you get a 404, ensure the following has been done:

1. `composer install` was run in your container
1. `drush status` doesn't report any errors

Next check to see that the Traefik proxy sees the containers/services. To go drupal.dflocal.net:8080 (the manage port) and see the list of services. By default at least 6 services should be running including drupal, mariadb and mailhog. If any of those are missing, it's likely another container was using a port or some other failure occurred when generating the container.

Use VSCode to reopen the folder locally, go to the Docker extension in VSCode and ensure all your containers have stopped and clear them out.

Then reopen in container and your services should be back up and running.

## I get the Drupal site install page instead of my Drupal site

Run a `drush status` and `drush sql-connect` within your container. Ensure the database connection info is set and correct (by default the user, pass and db name are "drupal"). Next check that your database container started properly (use the Remote Explorer in VScode). You shoudl see a tiny play button next to the name of your database container (e.g. myproject_mariadb). If you don't see a play button, right click the container and select "Show container log". That may help you discover why your database container failed to start and what needs to be fixed.

If all else fails, shutdown your containers and delete the data volumes for your databases. **This will erase all database data from your local drupal site** but will allow your database container to start up again.

## I get an error when building the containers

Sometimes the apt commands in the web.dockerfile can fail. This is either a disk space issue or a network error. Typically this can be resolved by clearing your existing containers and triggering a rebuild.

From outside the container, you can run a rebuild manually. Go into your docker directory and run `docker compose pull` and `docker compose build --no-cache` to force a rebuild of your containers. Assuming there are no errors, you can then reopen in container to continue working.

## I get an error when running `composer install` similar to `PHP Warning:  require(/var/www/html/vendor/composer/../chi-teck/drupal-code-generator/src/bootstrap.php): Failed to open stream: No such file or directory in /var/www/html/vendor/composer/autoload_real.php`

Sometimes switching between versions of composer you can end up with a broken autoload. You can use the global install of composer in your container to recover the site.

Rerun the command with `/usr/local/bin/composer install` which exists outside the project. It can recreate the autoload for your project.

## I get an incomptibility with the platform reqs and the `composer install` or `composer update`

Try adding the `--ignore-platform-reqs` to get the install working. Then adjust your dropfort environment to have the requirements you need (e.g. swap from PHP7 to PHP8, add/disable php extensions).
