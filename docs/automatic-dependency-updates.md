---
title: Automatic site updates
tags:
  - versioning
  - version
  - semver
  - commit standards
  - standard-version
  - release
  - prerelease
---

# Automatic site updates

Apply security updates and general updates on composer and npm.
