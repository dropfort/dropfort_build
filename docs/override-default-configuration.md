---
title: Override Default Configuration
tags:
  - drupal
  - composer
  - docker
  - scaffold
  - apache
---

# Override Default Configuration

Dropfort Build scaffolds all of the files required to build, test and deploy Drupal sites. However there are times when you want to change the behavior of your containers, CI pipelines or local environment.

## Modifying Configuration Files
The containers and VSCode both use environment variables to control base functionality. If you can manipulate your setup with environment variables we'd recommend starting there. If you need to modify source files themselves (for example, adding additional containers to docker compose) you have three options to accomplish this.

1. Append to the configuration file in question
1. Patch the configuration file
1. Replace the file entirely

The first two allow you to continue to recieve updates from Dropfort Build releases as they come out and stay up to date. The last option puts the responsibility on you to bring in required changes as they are released.

### Drupal Composer Scaffold
The primary tool used by Dropfort Build to provision files is [drupal/core-composer-scaffold](https://www.drupal.org/docs/develop/using-composer/using-drupals-composer-scaffold) which provides the means for you to append, patch or replace scaffolded files.

You can see all the files that Dropfort Build scaffolds by [reviewing the `composer.json` file that ships with it](https://gitlab.com/dropfort/dropfort_build/-/blob/6.x/composer.json?ref_type=heads#L77). Any file mapping you see here you can override in your own project's `composer.json` file.

Two key values to remember are the `[project-root]` and `[web-root]` keywords. These refer to the actual project root directory and the web directory (a.k.a. where index.php is) within your project. You can see more about these keywords in the Drupal Composer Scaffold documentation linked above.

## Examples
The examples below require you to make changes to your `composer.json` file in the `extra.drupal-scaffold.file-mapping` section.

### Appending to your gitignore
A common use case for appending configuration is to add new entries to the `.gitingore` file in your project. To do this, you can add a file to the `/patches` directory at the root of your project and tell the scaffold to append the contents to your `.gitignore`.

Edit your `composer.json` file in the `extra.drupal-scaffold.file-mapping` section. Here you can add rules to override how files are scaffolded.
e.g.
```json
  "file-mapping": {
    "[project-root]/.gitignore": {
      "append": "patches/extra-gitignore.patch"
    }
  }
```
### Patching your .htaccess file

Please refer to the [official Drupal Composer Scaffold documentation](https://www.drupal.org/docs/develop/using-composer/using-drupals-composer-scaffold#toc_4) for a working example.

### Replacing MariaDB with MySQL as the database
Some users prefer to use the official MySQL server as their backend over MariaDB. In this scenarios we would explicitly `replace` the scaffolded file with your own.

e.g.
```json
  "file-mapping": {
    "[project-root]/container/dev/docker-compose.yml": false
  }
```

This will tell the scaffold to never touch that file again and leave your changes intact from here on out. But any upstream changes to the docker-compose.yml file will not be added to your file.

### Modifying GitLab CI
The CI pipeline can usually be manipulated directly by editing your .gitlab-ci.yml file at the root of your project. This file is initially scaffolded but is then left alone since most projects require changes to this file.

If your changes are too extensive, or you just find it easier to organize your CI by using the `ci/templates` files directly, you can tell the scaffold to skip or append to a file as needed.

### Adding configuration to the Apache instance in the container
Both your dev and release containers use the `webdevops/php-apache-dev` and `webdevops/php-apache` respectively to run your web server. If you want to add configuration to the global Apache configuration or to your vhost configuration here's a working example.

Dropfort Build scaffolds the `container/dev/config/apache` and `container/release/config/apache` directories. Each also contain a `vhost` directory. These folders can be used to store custom configuration for apache. For example, if we wanted to add an alias in apache for SimpleSamlPHP and set the required environment variables we can do the following.

1. Create your configuration files
You can commit new files into the `config/apache` or `config/apache/vhost` directly without worry about the scaffold touching them. In our case lets create a `saml.conf` file in the `config/apache/vhost` directory since this is a vhost specific configuration (* **n.b.** the location of the configuration files within your project doesn't really matter, this is just recommended to stay organized*).

Next we'll tell the `container/dev/build/web.dockerfile` to copy that config into the container image. To do so we need to append to the `web.dockerfile` to add a `COPY` command. We can store the text for that command by creating a `patches/web.dockerfile.saml.txt` file.

```dockerfile
# Add Saml vhost configuration.
COPY config/apache/vhost/saml.conf /opt/docker/etc/httpd/vhost.common.d/saml.conf
```

*Refer to the webdevops/php-apache documentation on the file locations for customizing configuration*

Edit your composer.json file
e.g.
```json
  "file-mapping": {
    "[project-root]/container/dev/build/web.dockerfile": {
      "append": "patches/web.dockerfile.saml.txt"
    }
  }
```

Rebuild your container and your should see Docker add your configuration to the container.

This will allow Dropfort Build to continue to scaffold the web.dockerfile and then your additional configuration options can be included as well.
