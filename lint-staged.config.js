const config = {
  "*.{js,mjs,cjs}": ["eslint --fix", "prettier --write"],
  "*.{php,module,inc,install,test,profile,theme}": "npm run lint:php -- ",
};

module.exports = config;
