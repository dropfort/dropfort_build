# This file is managed by dropfort/dropfort_build.
# Modifications to this file will be overwritten by default.

# Copyright (c) 2022 Coldfront Labs Inc.

# This file is part of DropfortCI Scripts.

# DropfortCI Scripts is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# DropfortCI Scripts is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with DropfortCI Scripts.  If not, see <https://www.gnu.org/licenses/>.


########################################
# Backcopy Drupal database.
########################################


########################################
# Templates.
########################################

## Backcopy database template.
.backcopy_data_template:
  stage: task
  variables:
    DRUSH_BACKCOPY_SOURCE: prod
    DF_TASK_BACKCOPY_SKIP_DROP_TARGET_DATABASE: 'true'
  when: manual
  allow_failure: true
  script:
    - composer install --optimize-autoloader
    - |
      if [ -z "$DF_TASK_BACKCOPY_SKIP_DROP_TARGET_DATABASE" ]; then
        echo 'Dropping the existing target database before syncing.'
        drush $DRUSH_SITE_ALIAS_OPTIONS --ssh-options="$DRUSH_SSH_OPTIONS" @$DRUSH_ALIAS_GROUP.$DRUSH_ALIAS_TYPE sql:drop --yes
      fi
    - drush $DRUSH_SITE_ALIAS_OPTIONS --ssh-options="$DRUSH_SSH_OPTIONS" @$DRUSH_ALIAS_GROUP.$DRUSH_BACKCOPY_SOURCE sql-sync @self @$DRUSH_ALIAS_GROUP.$DRUSH_ALIAS_TYPE --runner=source --structure-tables-key=common --yes
    - |
      if [ -z DF_TASK_BACKCOPY_SKIP_SANITIZE ]; then
        drush $DRUSH_SITE_ALIAS_OPTIONS --ssh-options="$DRUSH_SSH_OPTIONS" @$DRUSH_ALIAS_GROUP.$DRUSH_ALIAS_TYPE sql:sanitize -y
      fi
    - drush $DRUSH_SITE_ALIAS_OPTIONS --ssh-options="$DRUSH_SSH_OPTIONS" @$DRUSH_ALIAS_GROUP.$DRUSH_ALIAS_TYPE cr
    # There is a bug with watchdog where a config import will fail due to the watchdog table already existing.
    # The current solution to this is to run the config import twice.
    - |
      if [ -z "$DF_TASK_BACKCOPY_SKIP_DEPLOY" ]; then
        # Mirror CI job by trying twice in case of an initial config import failure.
        drush $DRUSH_SITE_ALIAS_OPTIONS --ssh-options="$DRUSH_SSH_OPTIONS" @$DRUSH_ALIAS_GROUP.$DRUSH_ALIAS_TYPE deploy --yes \
        || drush $DRUSH_SITE_ALIAS_OPTIONS --ssh-options="$DRUSH_SSH_OPTIONS" @$DRUSH_ALIAS_GROUP.$DRUSH_ALIAS_TYPE deploy --yes
      fi
    - drush $DRUSH_SITE_ALIAS_OPTIONS --ssh-options="$DRUSH_SSH_OPTIONS" @$DRUSH_ALIAS_GROUP.$DRUSH_ALIAS_TYPE ssh "sudo service httpd graceful && sudo service memcached restart" || true # @todo probably deprecate this somehow

## Backcopy database exclude rules.
.backcopy_data_exclude_rules:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$TARGET_PLATFORM == "pantheon"'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ /\[validate only\]/'
      when: never

## Backcopy database include rules.
.backcopy_data_include_rules:
  rules:
    - if: '$TARGET_PIPELINE == "all"'
    - if: '$TARGET_PIPELINE == "build_and_deploy"'

########################################
# Jobs.
########################################

## Backcopy database from Prod to Dev.
backcopy_data_prod_to_dev:
  extends:
    - .prod_detect_template
    - .backcopy_data_template
  variables:
    DRUSH_ALIAS_TYPE: dev
    DRUSH_BACKCOPY_SOURCE: prod
  after_script:
    - eval $(ssh-agent -s)
    - test ! -z "$RUNNER_DEPLOY_KEY" && ssh-add <(echo "$RUNNER_DEPLOY_KEY" | base64 --decode)
    - test ! -z "$RUNNER_DEPLOY_KEY_DEV" && ssh-add <(echo "$RUNNER_DEPLOY_KEY_DEV" | base64 --decode)
    - ssh-add -l
    - export PATH="$(pwd)/bin:$(pwd)/vendor/bin:$PATH"
    - export DF_ADMIN_USER=`drush $DRUSH_SITE_ALIAS_OPTIONS --ssh-options="$DRUSH_SSH_OPTIONS" @$DRUSH_ALIAS_GROUP.$DRUSH_ALIAS_TYPE ssh "drush uinf --uid=1 --format=json" | jq .[].name -r`
    - drush $DRUSH_SITE_ALIAS_OPTIONS --ssh-options="$DRUSH_SSH_OPTIONS" @$DRUSH_ALIAS_GROUP.$DRUSH_ALIAS_TYPE uublk $DF_ADMIN_USER
  rules:
    - if: '$SKIP_JOBS =~ /backcopy_data_prod_to_dev/'
      when: never
    - !reference [.backcopy_data_exclude_rules, rules]
    - !reference [.backcopy_data_include_rules, rules]
    - !reference [.prod_detect_include_rules, rules]

## Backcopy database from Prod to QA.
backcopy_data_prod_to_qa:
  extends:
    - .prod_detect_template
    - .backcopy_data_template
  variables:
    DRUSH_ALIAS_TYPE: qa
    DRUSH_BACKCOPY_SOURCE: prod
  rules:
    - if: '$SKIP_JOBS =~ /backcopy_data_prod_to_qa/'
      when: never
    - !reference [.backcopy_data_exclude_rules, rules]
    - !reference [.backcopy_data_include_rules, rules]
    - !reference [.qa_detect_no_env_exclude_rules, rules]
    - !reference [.prod_detect_include_rules, rules]
